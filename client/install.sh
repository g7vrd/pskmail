#! /bin/sh

INSTALLDIR="/usr/local/share"

MYDIR=$PWD

if [ -d $INSTALLDIR/pskmail ]; then
	echo "Directory $INSTALLDIR/pskmail exists"
else
	echo "Making $INSTALLDIR/pskmail directory"
	cd $INSTALLDIR
	mkdir pskmail
fi

cd $MYDIR

echo "Copying pskmail to /usr/bin"

cp -f pskmail /usr/bin
cp -f paq8l /usr/bin
cp -f fldigi /usr/bin

echo "Copying scripts to $INSTALLDIR/pskmail"

cp -f psk_arq.pl $INSTALLDIR/pskmail
cp -f rflinkclient.pl $INSTALLDIR/pskmail
cp -f arq.pm $INSTALLDIR/pskmail
cp -f PSKmail.glade $INSTALLDIR/pskmail
cp -f paq8l $INSTALLDIR/pskmail
cp -f paq864 $INSTALLDIR/pskmail
cp -f unpaq864 $INSTALLDIR/pskmail

if [ -d $HOME/.pskmail ]; then
	exit 1
fi

echo "Making ~/.pskmail directory"

cd $HOME

mkdir .pskmail

cd .pskmail

mkdir downloads
mkdir pskuploads
mkdir Outbox


chown -R $SUDO_USER:$SUDO_USER $HOME/.pskmail

exit 1
