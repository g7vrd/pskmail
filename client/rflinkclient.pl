#! /usr/bin/perl -w

# ARQ module rflinkclient.pl by PA0R. This module is part of the PSK_ARQ suite of
# programs. Rflinkclient contains the client protocol engine which adds an arq layer 
# to keyboard oriented protocols like PSK31, PSK63, MFSK, MT63 etc.

# Rflinkclient.pl includes the arq primitives for the client.

# This program is published under the GPL license.
#   Copyright (C) 2005, 2006, 2007, 2008
#       Rein Couperus PA0R (rein@couperus.com)
# 
# *    rflinkclient.pl is free software; you can redistribute it and/or modify
# *    it under the terms of the GNU General Public License as published by
# *    the Free Software Foundation; either version 2 of the License, or
# *    (at your option) any later version.
# *
# *    rflinkclient.pl is distributed in the hope that it will be useful,
# *    but WITHOUT ANY WARRANTY; without even the implied warranty of
# *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# *    GNU General Public License for more details.
# *
# *    You should have received a copy of the GNU General Public License
# *    along with this program; if not, write to the Free Software
# *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# last update 100908



use lib "/usr/local/share/pskmail";
use arq;
use Fcntl qw(:flock);
use IO::Handle;

my ($reader, $writer);
pipe($reader, $writer);
$writer->autoflush(1);
STDIN->blocking(0);

my ($ServerCall, $Inputfile,  $Max_retries, $Maxidle, $positmessage, $offset, $posit_second);
$Inputfile = "";
$Max_retries = 20;
$posit_second = 5;
$Maxidle = 10;
$positmessage = "";
$offset = 0;					
my $queryposit;

# get config data...
	my @configdata;
	if (-e "$ENV{HOME}/.pskmail/.pskmailconf") {
		open (CONFIG, "$ENV{HOME}/.pskmail/.pskmailconf")or die "Could not open config file\n";
			$configdata = <CONFIG>;
		close (CONFIG);
		@confrecord = split ",", $configdata;
		
		$Inputfile = "$ENV{HOME}/" . $confrecord[3];
		$Max_retries = $confrecord[6];
		$Maxidle = $confrecord[7];
#		$Txdelay = $confrecord[8];
		$positmessage = $confrecord[9] . $confrecord[10];
		$offset = $confrecord[11];
		$posit_second = $confrecord[12];
	} else {
		logprint ("CONFIG FILE NOT THERE\n");
	}


#open ($getfh, ">", "$ENV{HOME}/PSKmailserver");
#close ($getfh);

my $TextFromFile = "";
my $Status = "Listen";
my $ConnectStatus = "Listening";
my $RxStatus = "";
$DisconnectFlag = 0;
$Maxidle = 9;
my $Maxconnectidle = 2;
my $Retries = 0;
my $RxReady = 0;
my $lastbeacon = time;
my $beaconsecs = 600;
my $shortbeacon = 0;
my $latstore = 0.0;
my $lonstore = 0.0;
my @options = ();

my $outputstring = "";
my $statusmessage = "";
my $harvest = 0;
my $advice = "";
my $master = 0;
my $rxpolls = 0;

$SIG{PIPE} = 'IGNORE';

initialize();
		if (-s "$ENV{HOME}/.pskmail/.PSKoptions") {
			open (OPTIONS, "$ENV{HOME}/.pskmail/.PSKoptions");
			@options = <OPTIONS>;		
			close (OPTIONS);
			
			chomp $options[0];
			chomp $options[1];
			chomp $options[2];
			chomp $options[3];
			chomp $options[4];
			chomp $options[5];
		}
		
my $lat = $options[3];
my $lon = $options[4];
my $cog = "";
my $spd = "";
my @gps = ();
$offset = $options[5];	

					

if ($pid = fork) {
	close $writer;

	while ($DisconnectFlag == 0) {
		my $readline = <$reader>;
		if ($readline) {
			chomp($readline);
			open CLIENTOUT, ">>clientout";
			print CLIENTOUT $readline, "\n";
			close CLIENTOUT;
		} 
	}
	close $reader;
	waitpid($pid, 0);
} else {
	die "Cannot fork: $!" unless defined $pid;
	close $reader;
		
	while (1) {
		if (pskclient() eq "Disconnect") {;
			$DisconnectFlag = 1;
			close $writer;
			exit;	
		}
	}
}

exit (1);

########################################################
sub pskclient {		#main, client
########################################################
my $session = "none";
my $Iamslave = 0;
my $Connectnr = 0;

while (1) {

	if ($ConnectStatus eq "Listening") {
	## send connect	

		while ($Retries < $Max_retries && get_rxstatus() ne "Connect_ack" && $ConnectStatus !~ /Connected/) {
		
			open SESSIONDATA, "PSKmailsession";
			$session = <SESSIONDATA>;
			chomp $session;
			close SESSIONDATA;
			if (-e ".scanning" ) {
				if ($session =~ /Connected/ && $session ne "TTYConnected" ) {
					if ((time/60) % 5 == $offset) {
								$Iamslave = 0;
								if ($Retries) { 
									sleep $Retries;
								} else {
									sleep 5;
								}
								set_txstatus("TXConnect");
								printstuff ("sending connect\n");
								send_frame();
								$Retries++;
								sleep (10);
					} 
				}
			} else {
				if ($session =~ /Connected/ && $session ne "TTYConnected" ) {
							$Iamslave = 0;
#							if ($Retries) { 
#								sleep $Retries * 2;
#							} 
							set_txstatus("TXConnect");
							printstuff ("sending connect\n");
							send_frame();
							$Retries++;
							sleep 4;
							my $whatmodem = mygetspeed();	
							if ($whatmodem eq "THOR22") {
								sleep 10;
							}			
				} elsif ($session =~ /TTYConnected/) {
					$Iamslave = 0;
#					if ($Retries) { sleep (2 * $Retries) + 5;}
					if ($Retries > $Max_retries + 1) {
						$ConnectStatus = "Listening";
						print $writer "\n==Disconnected...\n"; # tell the GUI we're done
						$DisconnectFlag = 1;
						system ("killall rflinkclient.pl");
						exit;	
					}
					set_txstatus("TTYConnect");
					printstuff ("sending connect\n");
					send_frame();
					$Retries++;	
					sleep (5);	
				}
			}

	## wait for connect_ack
			until (get_rxstatus() eq "Connect_ack" || $ConnectStatus eq "Connected") {

						listening();
				
				
				if (get_rxstatus() eq "Connect_req") {
					set_txstatus("TXConnect_ack");
					printstuff ("sending connect ack\n");
					send_frame();
					$session = "Connected";
					$ConnectStatus = "Connected";
					$Retries = 0;
					$Maxidle = 5;
					open SESSIONDATA, ">PSKmailsession";
					print SESSIONDATA "Connected";
					close SESSIONDATA;

				} elsif (get_rxstatus() eq "TTY_req"){
					set_txstatus("TTYConnect_ack");
					printstuff ("sending TTY ack\n");
					send_frame();
					$session = "Connected";
					$ConnectStatus = "Connected";
					$Retries = 0;
					$Maxidle = 5;
					$Iamslave = 1;
					open SESSIONDATA, ">PSKmailsession";
					print SESSIONDATA "TTYConnected";
					close SESSIONDATA;
				
				}

				if ($session eq "none") {
					my $mystring = get_rxqueue();
					
					if ($mystring) {
						$harvest += length $mystring;
						print $writer $mystring;
						reset_rxqueue();
					}
	# do posit beacon ?

						$myminute = (time /60) % 60; 	#/
						$mysecond = (time % 60);
						
					
						
					if ($mysecond == $posit_second && -e ".doposit" && 
						($myminute == 5 + $offset ||
						$myminute == 15 + $offset ||
						$myminute == 25 + $offset ||
						$myminute == 35 + $offset ||
						$myminute == 45 + $offset ||
						$myminute == 55 + $offset) ) {
											 						
							open (OPTIONS, "$ENV{HOME}/.pskmail/.PSKoptions");
							@options = <OPTIONS>;		
							close (OPTIONS);
							
							chomp $options[0];
							chomp $options[1];
							chomp $options[2];
							chomp $options[3];
							chomp $options[4];
							chomp $options[5];
							$offset = $options[5];
						
						
						if (-e "$ENV{HOME}/.pskmail/.gpsval") {	# see if gps running
							my $gpsstuff = `cat $ENV{HOME}/.pskmail/.gpsval`;
							@gps = split ",", $gpsstuff;
							$lat = $gps[1];
							$lon = $gps[2];
							$cog = sprintf ("%s", int($gps[4]));
							$spd = sprintf ("%3.4s", $gps[5]) ;
						} else {
							$lat = "Nofix";
						}
							
						if ($lat eq "Nofix")	{
							$lat = $options[3];
							$lon = $options[4];	
						}
						
						if (-e ".shortbeacon" && $lat == $latstore && $lon == $lonstore) {
							my $posit = "&&";
							
							send_aprsmessage($posit);
							
							$lastbeacon = time;
						
						} else {
							
							$latstore = $lat;
							$lonstore = $lon;			
						
							my $lat_sign;
							my $lon_sign;
							if ($lat < 0) { 
								$lat = abs $lat;
								$lat_sign = "S";
							} else {
								$lat_sign = "N";
							}
							if ($lon < 0) {
								$lon = abs $lon;
								$lon_sign = "W";
							} else {
								$lon_sign = "E";
							}
							my ($aprs_sign,$aprs_status) = get_positmsg();

							$aprs_lat = (($lat - int($lat)) * 60) + int($lat)* 100;

							if (int($lat) != 0 && abs($lat) < 10.0) {
								$aprs_lat = "0" . $aprs_lat;
							} elsif (int($lat) == 0) {
								$aprs_lat= "00" . $aprs_lat;
							} else {
								$aprs_lat = $aprs_lat;
							}
							
							if ($aprs_lat =~ /(\d\d\d\d\.\d\d)/) {
								$aprs_lat = $1;
							} elsif ($aprs_lat =~ /(\d\d\d\d\.\d)/) {
								$aprs_lat = $1 . "0";
							} elsif ($aprs_lat =~ /(\d\d\d\d)/) {
								$aprs_lat = $1 . ".00";
							} else {
								$aprs_lat = $1 . "0000.00";
							}

							
							$aprs_lon = (($lon - int($lon)) * 60) + int($lon)* 100;
							if (int($lon) == 0) {
								$aprs_lon = "000" . $aprs_lon;
							} elsif (int($lon) < 10) {
								$aprs_lon = "00" . $aprs_lon;
							} elsif (int($lon) < 100) {
								$aprs_lon = "0" . $aprs_lon;
							} else {
								$aprs_lon =  $aprs_lon;
							}

							if ($aprs_lon =~ /(\d\d\d\d\d\.\d\d)/){
								$aprs_lon = $1;
							} elsif ($aprs_lon =~ /(\d\d\d\d\d\.\d)/){
								$aprs_lon = $1 . "0";
							} elsif ($aprs_lon =~ /(\d\d\d\d\d)/) {
								$aprs_lon = $1 . ".00";
							} else {
								$aprs_lon = "00000.00";
							}
							
							
							if(-e "$ENV{HOME}/.pskmail/.doposit") {
								 $queryposit = `cat $ENV{HOME}/.pskmail/.doposit`;
							 }
							
							if ($queryposit && $queryposit =~ /(\d*)WX/) {
								my $wxcontent = `cat $ENV{HOME}/.pskmail/.wx`;
								$aprs_status .= $wxcontent;
							}
														
							if ($spd) {
								$posit = "!" . $aprs_lat . $lat_sign . "/" . $aprs_lon . $lon_sign . $aprs_sign . $cog . "/" . $spd . "/" . $aprs_status;
							} else {
								$posit = "!" . $aprs_lat . $lat_sign . "/" . $aprs_lon . $lon_sign . $aprs_sign . $aprs_status;
							}
							
							if ($queryposit =~ /(\d+).*/) {							
								if (int(rand($1)/10) == 0) {
									send_aprsmessage($posit);
								}	
							} 					
							
							
							$lastbeacon = time;
						}
					}			
					
								
					
				} else {
					inc_idle();

# VK2ETA Start

					$Myspeed1 = getspeed();			
					my $speedvalue;
					if ($Myspeed1 eq "PSK250") {
						$speedvalue = 2;
					} elsif ($Myspeed1 eq "PSK125"){
						$speedvalue = 4;
					} elsif ($Myspeed1 eq "PSK63"){
						$speedvalue = 8;
					} else {
						$speedvalue = 10;
					}
# VK2ETA end
					if (get_idle() > $speedvalue) { last; } # timeout, try again
				}

			}
			
		}
=head1		
		if ($Retries > $Max_retries) {
			$statusmessage = $ConnectStatus . "," . "Exiting...";				
			update_status ($statusmessage);
			wait;
			exit 1; 
		} # nobody home...
=cut
	
		if (get_rxstatus() eq "Connect_ack" || $ConnectStatus =~ /Connected/) {
			$ConnectStatus = "Connected";
			$Retries = 0;
			
			my $Caller = `cat $ENV{HOME}/.pskmail/.mastercall`;
			chomp $Caller;

			open SESSIONDATA, "PSKmailsession";
			$session = <SESSIONDATA>;
			chomp $session;
			close SESSIONDATA;
						
			if ($Iamslave && $session eq "TTYConnected") {
				print $writer "==Connect from $Caller\n"; #tell the GUI
			} else { 
				print $writer "==Connected\n"; #tell the GUI
			}
			

			$RxReady = 1;
	## we are now connected
			if ($RxReady == 1) {	# send first frame
				$RxReady = 0;
				if ($session eq "TTYConnected"){
					if ($Iamslave) {
						$Maxidle = 15;
					} else {
						$Maxidle = 10;
					} 
				} else  {
					$Maxidle = 10;				
				}
				$outputstring = gettxinput();
				set_txstatus("TXTraffic");
				send_frame($outputstring);
				$outputstring = "";
			}

			while ($ConnectStatus =~ /Connected/) {
	## receive loop						
				until (get_idle() > $Maxidle) {
#					sleep(1);
					inc_idle();	# update idle counter
					listening();
					if (check_lastblock()) { 
						last; 
					}
				}
				$RxStatus = get_rxstatus();
				
				my @queuestatus = ();
				@queuestatus = get_rxqueue_status();
				if ($queuestatus[1] == $queuestatus[2]) {
					$queuestatus[3] = "";
				}
				
				
#				$statusmessage = sprintf ("   E=%d", length ($queuestatus[3]));
			
				my $statdiff = $queuestatus[1] - $queuestatus[2];
				if ($statdiff < 0) { $statdiff += 64; }
				if ($statdiff == 9) { 
					$queuestatus[1] = $queuestatus[2]; 
					$queuestatus[3] = "";
				}

				$statusmessage = 
					$queuestatus[0] .
					"[" .
					$queuestatus[2] . "-" .
					$queuestatus[1] . "]" .
#					$harvest ;
					$queuestatus[3];
				if ($Retries) {
					$statusmessage .= "R:$Retries"
				} 
				if ($rxpolls) {
					$statusmessage .= "P:$rxpolls";
				}

				update_status ($statusmessage);


	## if timeout send poll frame			
				if ($Iamslave) {
					if (get_idle() > $Maxidle) {
						if ($Retries < $Max_retries) {
							if ($Retries) {
								my $mysecond = (time % 10);
								my $waiting = 5 - $mysecond;
								if ($waiting < 0) { 
									$waiting += 10; 
								}
								sleep ($waiting);
							}
							set_txstatus("TXPoll");
							send_frame("");
							$Retries++;
							$RxStatus = "";
						}else {
							$ConnectStatus = "Listening";
							print $writer "\n==Disconnected...\n"; # tell the GUI we're done
							$DisconnectFlag = 1;
							system ("killall rflinkclient.pl");
						
							printstuff ("sending abort...\n");
							set_txstatus("TXAbort");
							send_frame();
							$ConnectStatus = "Listen";
							$Retries = 0;
						}
					}
				
				} else {
				
					if (get_idle() > $Maxidle) {
						if ($Retries < $Max_retries) {
							if ($Retries ) {
								my $mysecond = (time % 10);
								my $waiting = 10 - $mysecond;
								if ($waiting < 0) { 
									$waiting += 10; 
								}
								sleep ($waiting);
							}
							set_txstatus("TXPoll");
							send_frame("");
							$Retries++;
							$RxStatus = "";
						} else {
					$ConnectStatus = "Listening";
					print $writer "\n==Disconnected...\n"; # tell the GUI we're done
					$DisconnectFlag = 1;
					system ("killall rflinkclient.pl");
							printstuff ("sending abort...\n");
							set_txstatus("TXAbort");
							send_frame();
							$ConnectStatus = "Listen";
							$Retries = 0;
						}
					}
				}				
				if ($RxStatus eq "Abort") {
					$ConnectStatus = "Listening";
					last;
				} elsif ($RxStatus eq "TTY_req") {
					if ($Connectnr) {
						$ConnectStatus = "Listening";
						print $writer "\n==Disconnected...\n"; # tell the GUI we're done
						$DisconnectFlag = 1;
						sleep 5;
						system ("killall rflinkclient.pl");
						exit;
					} else {
						$Connectnr++;
					}
				} elsif ($RxStatus eq "Status_rx"){
					$Retries = 0;
					$RxReady = 1;
					$rxpolls = 0;
				} elsif ($RxStatus eq "Poll_rx"){
					$RxReady = 1;
					$rxpolls++;
				} elsif ($RxStatus eq "Disconnect_req"){
					$ConnectStatus = "Listening";
					logprint ("Disconnected...\n");
					print $writer "==Disconnected...\n"; # tell the GUI
					$DisconnectFlag = 1;
					system ("killall rflinkclient.pl");
					return "Disconnect";
				} elsif ($Retries >= $Max_retries){
					$ConnectStatus = "Listening";
					print $writer "\n==Disconnected...\n"; # tell the GUI we're done
					$DisconnectFlag = 1;
					sleep 5;
					system ("killall rflinkclient.pl");
					exit;
				} elsif ($rxpolls >= $Max_retries){
					$ConnectStatus = "Listening";
					print $writer "\n==Disconnected...\n"; # tell the GUI we're done
					$DisconnectFlag = 1;
					sleep 5;
					system ("killall rflinkclient.pl");
					exit;
				} else {
					$ConnectStatus = "Connected";
				}

				my $mystring = get_rxqueue();
				
				if ($mystring) {
					$harvest += length $mystring;
					
					if ($mystring =~ /\~QUIT/) {
						open SESSIONDATA, ">PSKmailsession";
						print SESSIONDATA "none";
						close SESSIONDATA;
						set_txstatus("TXDisconnect");
						send_frame();
						$mystring = "";
						$session = "none";
						`killall rflinkclient.pl`;
					} else {
						print $writer $mystring;
						reset_rxqueue();
						if ($session eq "TTYConnected") {
							$Maxidle = get_maxidle() * 1.5;
						} else {
							$Maxidle = get_maxidle();						
						}
					}
				}			

				if ($RxReady == 1) {	# send next frame
					$RxReady = 0;
					$outputstring = gettxinput();

					if (get_sendqueue() || $outputstring) {

						set_txstatus("TXTraffic");
						send_frame($outputstring);
						$outputstring = "";
					} else {
						set_txstatus("TXStat");
						send_frame("");
					}
				}

				
				if ($Retries > 5) {
					if (get_idle() > ($Retries * $Retries * $Maxidle) && $session ne "none") {	# send poll frame
						set_txstatus("TXPoll");
						send_frame();
						$Retries++;
					}
				} elsif (get_idle() > $Maxidle && $session ne "none") {	# send poll frame
					set_txstatus("TXPoll");
					send_frame();
					$Retries++;
				}
				
				if ($Retries >= $Max_retries) {
					set_txstatus("TXAbort");
					send_frame();
					$ConnectStatus = "Listening";
					$Retries = 0;
				}
				if ($session eq "none") {
					$ConnectStatus = "Listening";
					last;
				}
			}
## connect ended
		}
		$ConnectStatus = "Listening";
		update_status("");
	}
#exit;
}

}


############################# end pskclient ################################

#############################################
sub update_status {
#############################################

	my $status_string = shift @_;
	open (STATUS, ">", ".pskmailstatus") or die "none\n";
	flock (STATUS, LOCK_EX);
	print STATUS $status_string, "\n";
	close (STATUS);
	select undef, undef, undef, 0.001;

} # end 
#############################################

#############################################
sub printstuff {
#############################################
my $stuff = shift @_;
open PRINTOUT, ">>client.log";
if ($stuff =~ m/==TIME!/) {
	;
} else {
	print PRINTOUT $stuff, "\n";
}
close (PRINTOUT);
} 


#############################################
sub mygetspeed {
#############################################
      open (SPD, "$ENV{HOME}/.pskmail/.modem");
       my $localspeed = <SPD>;
      chomp $localspeed;
       close (SPD);
       return $localspeed;
}
