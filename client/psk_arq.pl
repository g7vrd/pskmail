#! /usr/bin/perl -w 

# ARQ module psk_arq.pl by PA0R. 
# This module is part of the PSK_ARQ suite of programs. 
# psk_arq.pl contains the GUI and the mailbox functions (the application
# layer).

# This program is published under the GPL license.
#   Copyright (C) 2005, 2006, 2007, 2008
#       Rein Couperus PA0R (rein@couperus.com)
# 
# *    psk_arq.pl is free software; you can redistribute it and/or modify
# *    it under the terms of the GNU General Public License as published by
# *    the Free Software Foundation; either version 2 of the License, or
# *    (at your option) any later version.
# *
# *    psk_arq.pl is distributed in the hope that it will be useful,
# *    but WITHOUT ANY WARRANTY; without even the implied warranty of
# *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# *    GNU General Public License for more details.
# *
# *    You should have received a copy of the GNU General Public License
# *    along with this program; if not, write to the Free Software
# *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# Last change: 101108

use warnings;
use Gtk2 -init;
use Gtk2::GladeXML;
use Glib;
use IO::Handle;
use IO::Select;
use IO::Socket;
use MIME::Base64;
use Digest::MD5 qw(md5_base64);
use utf8;
require "utf8_heavy.pl";
require "Encode/Unicode.pm";


if (-d "$ENV{HOME}/.pskmail/") {
	# o.k.
} else {
	mkdir "$ENV{HOME}/.pskmail" ;
	chdir "$ENV{HOME}/.pskmail" or die "Cannot cd to .pskmail:$!\n";
	mkdir "downloads" or die "Cannot make downloads:$!\n";
	mkdir "Outbox" or die "Cannot make Outbox:$!\n";
	mkdir "pskupload" or die "Cannot make pskupload:$!\n";
}

if (! -e "/usr/bin/paq8l") {
	print "Decompression programs are missing...\n";
	exit;	
}

my @conf = ();

if (-e "$ENV{HOME}/.pskmail/.scanning") { unlink "$ENV{HOME}/.pskmail/.scanning";}	# clean all semaphores
if (-e "$ENV{HOME}/.pskmail/.shortbeacon") { unlink "$ENV{HOME}/.pskmail/.shortbeacon";}
if (-e "$ENV{HOME}/.pskmail/.doposit") { unlink "$ENV{HOME}/.pskmail/.doposit";}
if (-e "$ENV{HOME}/.pskmail/.wx") { unlink "$ENV{HOME}/.pskmail/.wx";}
if (-e "$ENV{HOME}/.fldigi/fldigi.log") { `echo "" > $ENV{HOME}/.fldigi/fldigi.log`;}
if (-e "$ENV{HOME}/.pskmail/.gpsval") {unlink "$ENV{HOME}/.pskmail/.gpsval"};

#########################################################################################

if ($modpid = fork) {
	#continued below...
	print "Main app...\n";
} else {
	die "cannot fork: $! " unless defined $modpid;
		
	my $remote_modem = "127.0.0.1";
	my $modem_port = 3122;

	my $socket = IO::Socket::INET->new(PeerAddr => $remote_modem,
									PeerPort => $modem_port,
									Proto    => "tcp",
									Type     => SOCK_STREAM)
		or die "Couldn't connect to $remote_modem:$modem_port : $@\n";

	$socket->blocking(0);
	STDOUT->autoflush();

	$pskmailon = sprintf ("%cMULTIPSK-OFF%c\n", 26, 27);

	print "starting PSKMAIL mode\n";

	print $socket $pskmailon;

	sleep 1;

	print "Setting sweet spot\n";

	print $socket "<cmd>server</cmd>";

	sleep 1;

	print "Starting RX\n";

	################## Modem Main loop ####################################

	################### constants ##################
	my $Busy_time = 3;		# delay time frequency busy
	my $MaxTXTime = 300;	# reset timer for $TX flag
	
	###################run variables################
	my $Modembuffer = "";	# Receive buffer
	my $Lastchar = "";		# Last character received by the modem
	my $TX = 0;				# Modem is sending
	my $TXTimer = 0;		# $TX reset timer
	my $Modem = "";			# Modem type
	my $Busy = 0;			# Busy flag
	my @BlockQueue = ();	# Modem out queue
	 
	################### scratch pad memry ##########
	my $Lastmodem = "";		# Modem type last checked.
	my $LastBlock = "";		# Last block received
	my $Sectimer = 0;		# Second timer
	my $tm_timer = 0;		# 30 minutes timer
	my $event_modem = 0;	# Check modem file
	my $event_beacon = 0;	# send beacon


	while (1) {
	## TIMERS ################
		
		## 1 second ##
		my $Timenow = time();
		if ($Timenow - $Sectimer > 0) {
			$Sectimer = $Timenow;		
			$event_modem = 1;
			$event_send = 1;
			$event_counter = 1;
		}
		## 30 minutes ##
		if (($Timenow - $tm_timer) > (30 * 60)) {
			$tm_timer = $Timenow;		
			$event_beacon = 1;
		}
		## down counter (seconds)##
		if ($event_counter) {
			$event_counter = 0;
			if ($Busy) { 
				$Busy--;
			} elsif ( -e "$ENV{HOME}/.pskmail/squelch.lk") {
				`rm $ENV{HOME}/.pskmail/squelch.lk`;
			}
			if ($TXTimer) { 
				$TXTimer--;
			}
		}
		
	## INPUT #################
		
		# Modem input?
			$Lastchar = modemrecv ();
			
			if ($Lastchar) {
				if ($Lastchar eq "<0>") {
					$Modembuffer .= "0";	
				} elsif ($Lastchar eq "<10>") {
					$Modembuffer .= "\n";	
				} elsif ($Lastchar eq "<13>") {
					$Modembuffer = sprintf ("%s%s", $Modembuffer, "\015");	
				} else {
					$Modembuffer .= $Lastchar;
				}
				if ($Busy < 1) {
					$Busy = $Busy_time;	# set the busy flag 
					`touch $ENV{HOME}/.pskmail/squelch.lk`;
				}
			}

		# PSKmail block?
			if ($Lastchar) {
				$Lastchar = "";			
				my ($NewBlock, $NewModembuffer) = get_next_block ($Modembuffer);
				$LastBlock = $NewBlock;
				$Modembuffer = $NewModembuffer;
				unshift @BlockQueue, $LastBlock;
				$LastBlock = "";
			}
		# Send block to app	
				if (scalar (@BlockQueue)) {
					if ( -e "$ENV{HOME}/.pskmail/.modlock") {
						# too early
					} else {
						
						my $LastBlock = pop @BlockQueue;
						
						if (length ($LastBlock) > 7 ) {
							
							`touch $ENV{HOME}/.pskmail/.modlock`;
							
							my $debugstring = "::" . $LastBlock;
							
	#						`echo '$debugstring' >> ~/.pskmail/test.txt`;
																
							open (MOUT, ">" , "$ENV{HOME}/.pskmail/.input");
							print MOUT $LastBlock, "\n";
							close MOUT;
						}
					}
				
				}

			
	## EVENT WATCHERS ########
		
		## Modem type wanted?
				if ($event_modem && $TX == 0 && -e "$ENV{HOME}/.pskmail/.modem" ) {
					$event_modem = 0;
					$Modem = `cat $ENV{HOME}/.pskmail/.modem`;
					chomp $Modem;
					if ($Modem ne $Lastmodem) {
						changemodem ($Modem);
						$Lastmodem = $Modem;
						$Modem = "";
						sleep 1;
					}
				} 
				if ($TXTimer == 1) {
					$TXTimer = 0;
					$TX = 0;
					if (-e "$ENV{HOME}/.pskmail/.tx.lck") {
						`rm $ENV{HOME}/.pskmail/.tx.lck`;
					}
				}	

		
	## OUTPUT ################

		## Terminal output		
				if ($LastBlock) { 
					print $LastBlock, "\n";
					$LastBlock = "";	
				}
		
		## Text output to fldigi 		
				if ($event_send && $Busy == 0 && $TX == 0 && -e "$ENV{HOME}/.pskmail/snd") {
					$event_send = 0;
					my $txt = `cat $ENV{HOME}/.pskmail/snd`;
					`rm $ENV{HOME}/.pskmail/snd`;
										
					chomp $txt;
					sendblock ($txt);
				}
				

		select undef, undef, undef, 0.01;
	} # END ######################

	############# receive input from modem ##########################
	# reveives one character from the modem and adds it to the buffer
	# returns the character received and sets the global
	# TX flag.
	#
	sub modemrecv {
	#################################################################	
		my $buf = "";
		my $chr = 0;

		read ($socket, $buf, 1) ;

		if (length $buf) {
				my $chr = ord($buf);
				if ($chr < 128) {
					if ($chr == 10) { 
						$buf = "<10>";
					} elsif ($chr == 13) {
						$buf = "<13>";
					} elsif ($chr == 48) {
						$buf = "<0>";
					} elsif ($chr == 4) {
						$buf = "<EOT>";
						if ( -e "$ENV{HOME}/.pskmail/squelch.lk") {
							`rm $ENV{HOME}/.pskmail/squelch.lk`;
						}
						$Busy = 0;
					} elsif ($chr == 1) {
						$buf = "<SOH>";
					} elsif ($chr == 6) {
						$buf = "";
						$TX = 0;
						if (-e "$ENV{HOME}/.pskmail/.tx.lck") {
							`rm $ENV{HOME}/.pskmail/.tx.lck`;
						}
						return ("");
					} elsif ($chr == 29) {
						$buf = "";
						return (undef);
					} elsif ($chr == 31) {
						$buf = "";
						$TX = 0;
						if (-e "$ENV{HOME}/.pskmail/.tx.lck") {
							`rm $ENV{HOME}/.pskmail/.tx.lck`;
						}
						return ("");
					} elsif ($chr == 30) {
						$buf = "";
						$TX = 1;
						`touch $ENV{HOME}/.pskmail/.tx.lck`;
						$TXTimer = $MaxTXTime;
						return (undef);
					} elsif  ($chr < 32) {
						$buf = sprintf ("<%d>", ord($buf));
					}

					if ($TX ) {
						return ("");
					} 

					return ($buf);
				}
			}
				
				
	} # end ############################################################

	################################################################
	# Isolates a pskmail protocol block. Returns an array containing 
	# the new block and what is left in the buffer.
	sub get_next_block {
	################################################################
		my $mystring = shift @_;
		
		if (length $mystring < 17)
		{
			return ("", $mystring);
		}
		
		
		if ($mystring =~ /(.*)(<SOH>.*<SOH>)(.*)/s)
		{
			return ($2, "<SOH>" . $3);
		}
		elsif ($mystring =~ /(.*)(<SOH>.*<EOT>)(.*)/s)
		{
			return ($2, $3);
		} elsif ($mystring =~ /.*(<SOH>.*)/s) {
			return ("", $1);
		} else {
				return ("", $mystring);
		}
		
	} # end

	########################################################
	#
	sub sendblock {
	########################################################
	my $sendtext = shift @_;
	$sendtext .= "\000";
	
	$TX = 1;
=head
#	my $prototext = $text ;
	my $prototext = $text . "\000";

	my @sendtxt = split ('', $prototext);

	$sendtext .= chr(26);
	$sendtext .= "TX";
	$sendtext .= chr(27);

	foreach my $letter (@sendtxt) {
#		$sendtext .= chr(25);
		$sendtext .= $letter;
	}

	$sendtext .= chr(26);
	$sendtext .= "RX_AFTER_TX";
	$sendtext .= chr(27);
=cut
	print $socket $sendtext;
	
	$sendtext = "";
	} # end

	#########################################################
	sub changemodem {
	#########################################################
	my $Modem = shift @_;
		
		print $socket "<cmd><mode>" . $Modem . "</mode></cmd>";
	
	} # end
	
	print $socket "<cmd>normal</cmd>";
	

exit;
}

#########################################################################################

if (-e "$ENV{HOME}/.pskmail/id_defined") { 
	unlink "$ENV{HOME}/.pskmail/id_defined";
}

use lib "/usr/local/share/pskmail";
use arq;

my $debug = 0;

# global definitions...
my $Version = "0.9.10";
my $Outputfile = "$ENV{HOME}/.pskmail/TxInputfile";
my $message = "message";
my @options = ();
my $context_id = 1;
my $context_id2 = 0;
my $pid = 0;
my $MSGNR = 0;
my $lastmessage = "";
my $mode = "MAIL";
my $indicator = 0;
my ($positsign_field, $positmsg_field) = get_positmsg();
my $bulletin_active = 0;
my $activemessage = "";
my $bin_active = 0;
my $binlength = 0;
my $Pfile_length = 0;
my $Frate = 0.0;
my $showstatus = 0;
my $bintable = "";
my $msg_active = 0;
my %msgseen = ();
my $headers_active = 0;
my $file_active = 0;
my $www_active = 0;
my $rcvfilename = "";
my @headers = ();
my $LastHeader = 0;
my $Scanningmode = 0;
my $Modem = "PSK250";
my $wassquelch = 0;
my $squelchtime = 0;
my $spd = 0.0;
my $cog = 0;
my @gps = ();
my $logoutstring = "";

my $monitorstart = 45;
my $colr = "black";
my $partial = "";
my $firstconfig = 0;

my $gpssocket = "";
my $gpspid;
my $gpssecs = 0;

`echo "30" > $ENV{HOME}/.pskmail/.doposit`;
`echo "30" > $ENV{HOME}/.pskmail/.dopositcopy`;
`touch $ENV{HOME}/.pskmail/headerlist`;
`echo "" > $ENV{HOME}/.pskmail/clientout`;
`touch $ENV{HOME}/.pskmail/.mastercall`;

	open (CMD, ">$ENV{HOME}/.pskmail/.modem");
	print CMD "PSK250";
	close (CMD);
	$Modem = "PSK250";


$togglepositbeacon = 1;


$SIG{PIPE} = 'IGNORE';


if ( -e "$ENV{HOME}/.pskmail/debug") { 
	$debug = 1;
} else {
	$debug = 0;
}

if (!$debug){
open (*STDOUT, "+<", "/dev/null") || die "cannot reopen to /dev/null: $!";
open (*STDERR, "+<", "/dev/null") || die "cannot reopen to /dev/null: $!";
}

# run variables
my $connected = 0;

# get the GUI and the widgets...

my $g = Gtk2::GladeXML->new("/usr/local/share/pskmail/PSKmail.glade");
my $window = $g->get_widget("mainwindow");
my $textview = $g->get_widget("maintext");
my $monitorview = $g->get_widget("textview2");
my $fileview = $g->get_widget("fileview"); 
my $headerview = $g->get_widget("Headers");
my $Main_entry = $g->get_widget("mainentry");
my $connect_button = $g->get_widget("connectbutton1");
my $connect_label = $g->get_widget("connectlabel");
my $statusline1 = $g->get_widget("statusbar1");
my $statusline2 = $g->get_widget("statusbar2");
my $timewindow = $g->get_widget("time");
my $minutewindow = $g->get_widget("minute");
my $exit_button = $g->get_widget("exitbutton1");
my $qtc_button = $g->get_widget("qtcbutton1");
my $download_button = $g->get_widget("downloadbutton1");
my $read_button = $g->get_widget("readbutton1");
my $r_window = $g->get_widget("readwindow");
my $read_entry = $g->get_widget("readentry");
my $delete_button = $g->get_widget("deletebutton1");
my $d_window = $g->get_widget("deletewindow");
my $delete_entry = $g->get_widget("deleteentry");
my $send_button = $g->get_widget("sendbutton1");
my $abort_button = $g->get_widget("abortbutton1");
my $new_button = $g->get_widget("newbutton1");
my $Positionbutton=$g->get_widget("positionbutton2");
my $n_window = $g->get_widget("newmessagewindow");
my $new_cancel = $g->get_widget("newcancelbutton");
my $new_ok = $g->get_widget ("newokbutton");
my $To_entry = $g->get_widget("toentry");
my $Subject_entry = $g->get_widget("subjectentry");
my $Body = $g->get_widget("bodyentry");
my $options_button = $g->get_widget("optionsbutton1");
my $options_window = $g->get_widget("Optionswindow");
my $server_entry_spin = $g->get_widget("comboboxentry1");
my $server_entry = $g->get_widget("comboboxentry-entry1");
my $mode_label = $g->get_widget("modelabel");
my $Progress = $g->get_widget("progressbar1");
my $Statuslabel = $g->get_widget("Statuslabel");
my $fileupload = $g->get_widget("filechooserdialog1");
my $fileok = $g->get_widget("fileokbutton");
my $filecancel = $g->get_widget("filecancelbutton");
my $speedbox = $g->get_widget("speedentry");

my $Notebook = $g->get_widget("notebook1");

my $http_window=$g->get_widget("httpwindow");
my $http_entry1=$g->get_widget("httpentry1");
my $http_entry2=$g->get_widget("httpentry2");
my $http_entry3=$g->get_widget("httpentry3");
my $http_entry4=$g->get_widget("httpentry4");
my $http_entry5=$g->get_widget("httpentry5");
my $http_entry6=$g->get_widget("httpentry6");
my $http_button1=$g->get_widget("httpbutton1");
my $http_button2=$g->get_widget("httpbutton2");
my $http_button3=$g->get_widget("httpbutton3");
my $http_button4=$g->get_widget("httpbutton4");
my $http_button5=$g->get_widget("httpbutton5");
my $http_button6=$g->get_widget("httpbutton6");
my $http_cancelbutton=$g->get_widget("httpcancelbutton");
my $http_storebutton=$g->get_widget("httpstorebutton");

my $http_window1=$g->get_widget("httpwindow1");
my $http_button7=$g->get_widget("httpbutton7");
my $http_button8=$g->get_widget("httpbutton8");
my $http_button9=$g->get_widget("httpbutton9");
my $http_button10=$g->get_widget("httpbutton10");
my $http_button11=$g->get_widget("httpbutton11");
my $http_cancelbutton1=$g->get_widget("httpcancelbutton1");

my $about_window=$g->get_widget("aboutdialog");
my $about_ok=$g->get_widget("aboutokbutton");
my $Call_entry = $g->get_widget("Mycallentry");
my $Server_entry = $g->get_widget("Servercallentry");
my $Blocklen_entry = $g->get_widget("Blocklenentry");
my $Lat_entry = $g->get_widget("Latentry");
my $Lon_entry = $g->get_widget("Lonentry");
my $QRG_entry = $g->get_widget("Qrgentry");
my $Options_cancel = $g->get_widget("Optionscancel");
my $Options_ok = $g->get_widget("Optionsok");
my $Position_button=$g->get_widget("positionbutton1");

## configuration dialog

my $systemconf = $g->get_widget("confwindow");
my $debugbutton = $g->get_widget("checkbutton1");
my $monitorbutton = $g->get_widget("checkbutton2");
my $rawbutton = $g->get_widget("checkbutton3");
my $infile_field = $g->get_widget("Infile");
my $outfile_field = $g->get_widget("Outfile");
my $logfile_field = $g->get_widget("Logfile");
my $max_retries_field= $g->get_widget("spinbutton1");
my $idle_field = $g->get_widget("spinbutton2");
my $txd_field = $g->get_widget("spinbutton3");
   $positsign_field = $g->get_widget("Positsign");
   $positmsg_field = $g->get_widget("Positmsg");
my $offset_minute_field = $g->get_widget("spinbutton4");
my $beaconsecond_field = $g->get_widget("spinbutton5");
my $confsave_button = $g->get_widget("confsave");
my $confcancel_button = $g->get_widget("confcancel");
my $minuteinput = $g->get_widget("spinbutton6");

## Mail options dialog

my $Mailoptionwindow=$g->get_widget("maildialog");
my $Mailoptionokbutton=$g->get_widget("mailoptionok");
my $Mailoptioncancelbutton=$g->get_widget("mailoptioncancel");
my $Mailoptionhost=$g->get_widget("hostentry");
my $Mailoptionuser=$g->get_widget("userentry");
my $Mailoptionpass=$g->get_widget("passentry");
my $Mailoptionreply=$g->get_widget("replyentry");
my $Mailoptionfindu=$g->get_widget("finduentry");

## GPS info line

my $GPSLatentry=$g->get_widget("entry3");
my $GPSLonentry=$g->get_widget("entry4");
my $GPSCoglabel=$g->get_widget("label144");
my $GPSSpdlabel=$g->get_widget("label145");
my $GPSCommententry=$g->get_widget("entry2");
my $GPSIconentry=$g->get_widget("comboboxentry3");
my $APRSIcon=$g->get_widget("comboboxentryicon");
my $Speedprogress2=$g->get_widget("progressbar3");
my $Compasswindow=$g->get_widget("textview3");
my $APRSbeaconON=$g->get_widget("checkbutton5");
my $APRSperiod=$g->get_widget("comboboxentry2");
my $Includewx=$g->get_widget("checkbutton4");
my $Emergencydialog=$g->get_widget("dialog1");
my $Emergencycancel = $g->get_widget("emergencycancel");
my $Sendemergency = $g->get_widget("emergencyok");
my $Emcbutton = $g->get_widget("EMCbutton");
my $Emergencyentry = $g->get_widget("emergencyentry");
my $SOSmessage = "";
my $Emergencystatus = 0;

my $Compassbuffer=$Compasswindow->get_buffer;

my $fontdesc = Gtk2::Pango::FontDescription->from_string ('monospace'); 
$Compasswindow->modify_font ($fontdesc); 
$textview->modify_font ($fontdesc);
$headerview->modify_font ($fontdesc);
$fileview->modify_font ($fontdesc);


my $Compass = " NNW           N            NNE           NE          ENE            E            ESE           SE           SSE           S            SSW";
my $Compass1= "| 340 | 350 |  0  |  10 |  20 |  30 |  40 |  50 |  60 |  70 |  80 |  90 | 100 | 110 | 120 | 130 | 140 | 150 | 160 | 170 | 180 | 190 | 200 | 210 ";
my $Compass2= "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||";
my $Compass3= "               S            SSW           SW          WSW            W            WNW           NW           NNW           N            NNE";
my $Compass4= "| 160 | 170 | 180 | 190 | 200 | 210 | 220 | 230 | 240 | 250 | 260 | 270 | 280 | 290 | 300 | 310 | 320 | 330 | 340 | 350 |  0  |  10 |  20 |  30 ";
my $Compass5= "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||";

my $WXdialog=$g->get_widget("Wxdialog");
my $Windspeed=$g->get_widget("Windspeed");
my $Sea=$g->get_widget("Sea");
my $Hpspin=$g->get_widget("Hpspin");
my $Tempspin=$g->get_widget("Tempspin");
my $Wdircombo=$g->get_widget("Wdircombo");
my $Seadircombo=$g->get_widget("Seadircombo");
my $Wxstat=$g->get_widget("statcombobox");
my $Savewx=$g->get_widget("Savewx");
my $Cancelwx=$g->get_widget("Cancelwx");
my $Wxset=$g->get_widget("Wxset");
my $Wxentry=$g->get_widget("Wxentry");
my $Pingbutton=$g->get_widget("pingbutton");
my $Linkbutton=$g->get_widget("linkbutton");
my $Getmsgbutton=$g->get_widget("getmessages");

my $Beaconperiod = "30";

$APRSperiod->set_active(1);
`echo "30" > $ENV{HOME}/.pskmail/.doposit`;

$Wdircombo->set_active(0);
$Seadircombo->set_active(0);
$Wxstat->set_active(0);

my $msgtextview=$g->get_widget("textview1");

my $Filedownloadbutton=$g->get_widget("Filedownload");
my $Filesbutton=$g->get_widget("Files");
my $Fileupdate=$g->get_widget("Fileupdate");
my $Fileconnectbutton=$g->get_widget("Fileconnectbutton");
my $Fileabortbutton=$g->get_widget("Fileabortbutton");
my $Filelistbutton=$g->get_widget("Listfiles");
my $Filequitbutton=$g->get_widget("Filequitbutton");
my $Fileconnectlabel=$g->get_widget("fileconnectlabel");

## End GUI config

if ($Frate > 1) {$Frate = 1;}
if ($binlength < 0) {$binlength = 0}; 
$trate = sprintf ("%d", 0);
$Progress->set_fraction($Frate);
$Progress->set_text($trate);

my $messagebuffer = $Body->get_buffer();
my $buffer = $textview->get_buffer;

my $monitorbuffer = $monitorview->get_buffer;
my $fileviewbuffer = $fileview->get_buffer;
my $msgtextbuffer = $msgtextview->get_buffer;

#$redtag = $monitorbuffer->create_tag ("red_foreground", foreground => "red");

my $end_mark = $buffer->create_mark ('end', $buffer->get_end_iter, FALSE);
my $monitor_end_mark = $monitorbuffer->create_mark ('end', $monitorbuffer->get_end_iter, FALSE);
my $msg_end_mark = $msgtextbuffer->create_mark ('end', $msgtextbuffer->get_end_iter, FALSE);
my $file_end_mark = $fileviewbuffer->create_mark ('end', $fileviewbuffer->get_end_iter, FALSE);

my $headerbuffer = $headerview->get_buffer;
my $headerinit = `cat $ENV{HOME}/.pskmail/headerlist`;

$headerbuffer->set_text($headerinit);

$statusline1->pop($context_id);
$statusline1->push($context_id, "");

my $ttymode_menu_item = $g->get_widget("tty1");

my $filenm = "";	# for uploads

`echo "" > $ENV{HOME}/.pskmail/modem.log`;
my $oldlength = 0;

$speedbox->set_text($Modem);

# set the parameters from file

if (-s "$ENV{HOME}/.pskmail/.PSKoptions") {
	open (OPTIONS, "$ENV{HOME}/.pskmail/.PSKoptions");
	@options = <OPTIONS>;		
	close (OPTIONS);
	
	chomp $options[0];
	chomp $options[1];
	chomp $options[2];
	chomp $options[3];
	chomp $options[4];
	chomp $options[5];
	
	$Call_entry->set_text($options[0]);
	$Server_entry->set_text($options[1]);
	$Blocklen_entry->set_text($options[2]);
	$Lat_entry->set_text($options[3]);
	$GPSLatentry->set_text($options[3]);
	$Lon_entry->set_text($options[4]);
	$GPSLonentry->set_text($options[4]);	
	$QRG_entry->set_text($options[5]);
	$offset = $options[5];
	my $callentry = $Call_entry->get_text();
	`touch $ENV{HOME}/.pskmail/calls.txt`;
#	`echo "$callentry" >> $ENV{HOME}/.pskmail/calls.txt`;
	`cat $ENV{HOME}/.pskmail/calls.txt | sort | uniq > $ENV{HOME}/.pskmail/tmpcalls`;
	`mv $ENV{HOME}/.pskmail/tmpcalls $ENV{HOME}/.pskmail/calls.txt`;
} else {
	`echo "N0CALL" >> $ENV{HOME}/.pskmail/.PSKoptions`;
	`echo "N0CALL" >> $ENV{HOME}/.pskmail/.PSKoptions`;
	`echo "4" >> $ENV{HOME}/.pskmail/.PSKoptions`;
	`echo "0.00000" >> $ENV{HOME}/.pskmail/.PSKoptions`;
	`echo "0.00000" >> $ENV{HOME}/.pskmail/.PSKoptions`;
	`echo "4" >> $ENV{HOME}/.pskmail/.PSKoptions`;
	$firstconfig = 1;
	$options_window->show(); # or from the options window
}

my @servers = ();
if (-e "$ENV{HOME}/.pskmail/.servers") {
	open (SERVERS, "$ENV{HOME}/.pskmail/.servers");
	@servers = <SERVERS>;
	close (SERVERS);
	foreach my $server (@servers) {
		chomp $server;
      	$server_entry_spin->append_text ($server);
	}	
} else {
}

#$server_entry_spin->remove_text (0);
$server_entry_spin->set_active(1);
$minuteinput->set_value($options[5] || 0);

$window->show();

# Actions from the menu...
$g->signal_autoconnect_all(
		
		on_quit1_activate => 
		sub {
			if (-e "$ENV{HOME}/.pskmail/headerlist") {
				send_to_mbox() ;
			}
			
			sendmodemcommand ("normal");
			sleep 1;
			
			`cp $ENV{HOME}/.pskmail/.cpclientout $ENV{HOME}/.pskmail/clientout`;
			if (-e "$ENV{HOME}/.pskmail/id_defined") { 
				unlink "$ENV{HOME}/.pskmail/id_defined";
			}
			untie %memreceive;
			kill('KILL', $pid);
			wait();
			Gtk2->main_quit;
		},
		on_about1_activate => 
		sub {
			$about_window->show();
		},
		on_options1_activate => 
		sub {
			if (-s "$ENV{HOME}/.pskmail/.PSKoptions") {
				open (OPTIONS, "$ENV{HOME}/.pskmail/.PSKoptions");
				@options = <OPTIONS>;		
				close (OPTIONS);
				
				chomp $options[0];
				chomp $options[1];
				chomp $options[2];
				chomp $options[3];
				chomp $options[4];
				chomp $options[5];
				
				$Call_entry->set_text($options[0]);
				$Server_entry->set_text($options[1]);
				$Blocklen_entry->set_text($options[2]);
				$Lat_entry->set_text($options[3]);
				$Lon_entry->set_text($options[4]);
				$QRG_entry->set_text($options[5]);
				my $serverentry = $Server_entry->get_text();
				my $callentry = $Call_entry->get_text();
				`echo "$serverentry" >> $ENV{HOME}/.pskmail/calls.txt`;
				`echo "$callentry" >> $ENV{HOME}/.pskmail/calls.txt`;
				`cat $ENV{HOME}/.pskmail/calls.txt | sort | uniq > $ENV{HOME}/.pskmail/tmpcalls`;
				`mv $ENV{HOME}/.pskmail/tmpcalls $ENV{HOME}/.pskmail/calls.txt`;
			}						

			$options_window->show();
		},
		on_mail_options1_activate => 
		sub {
			if (-s "$ENV{HOME}/.pskmail/.record") {
				open (OPTIONS, "$ENV{HOME}/.pskmail/.record");
				my @roptions = <OPTIONS>;		
				close (OPTIONS);
				
				chomp $roptions[0];
				my @options = split /,/ , $roptions[0];
				
				$Mailoptionhost->set_text($options[0]);
				$Mailoptionuser->set_text($options[1]);
				$Mailoptionpass->set_text($options[2]);
				$Mailoptionreply->set_text($options[3]);
				$Mailoptionfindu->set_text($options[4]);
			}						

			$Mailoptionwindow->show();
		},
		on_update_server1_activate => 
		sub {
			my $rec = `cat $ENV{HOME}/.pskmail/.record`;
			my $recbase64 = encode_base64($rec, "");
			$rec_up = "~RECx" . $recbase64;
			`echo "$rec_up" >> $ENV{HOME}/.pskmail/TxInputfile`;
			$statusline1->push($context_id, "Uploading record to server...");			
		},
		on_configuration1_activate => 
		sub {
			getconfig();
			
			$infile_field->set_text($conf[3]);
			$outfile_field->set_text($conf[4]);
			$logfile_field->set_text($conf[5]);
			$max_retries_field->set_value($conf[6]);
			$idle_field->set_value($conf[7]);
			$txd_field->set_value($conf[8]);
			$positsign_field->set_text($conf[9]);
			$positmsg_field->set_text($conf[10]);
			$offset_minute_field->set_value($conf[11]);
			$beaconsecond_field->set_value($conf[12]);

			$systemconf->show();
		},
		on_send_posit_beacon1_activate => 
		sub {

		},
		on_checkbutton5_activate => 
		sub {
			$statusline2->pop($context_id2);
			if ($togglepositbeacon) {
				$statusline2->push($context_id2, "Posit Beacon Off...");			
				`cp $ENV{HOME}/.pskmail/.doposit $ENV{HOME}/.pskmail/.dopositcopy`;
				`unlink $ENV{HOME}/.pskmail/.doposit`;
				$togglepositbeacon = 0;
			} else {
				$statusline2->push($context_id2, "Posit Beacon On...");			
				`cp $ENV{HOME}/.pskmail/.dopositcopy $ENV{HOME}/.pskmail/.doposit`;
				$togglepositbeacon = 1;
			}
		},
		
		on_new1_activate => 
		sub {
			$To_entry->set_text("");
			$Subject_entry->set_text("");
			$messagebuffer->set_text("");
			$n_window->show();	
		},
		on_save_draft1_activate => 
		sub {
			`cp $ENV{HOME}/.pskmail/message $ENV{HOME}/.pskmail/message.sav`;
			$statusline2->push($context_id2, "Drafts saved...");
		},
		on_open_draft1_activate => 
		sub {
			`cp message.sav message`;
			$statusline2->push($context_id2, "Opened send file...");
		},

		on_clear_sendfile1_activate => 
		sub {
			if (-e "./Outbox") {`rm ./Outbox/*`;}
			if (-e "message") {`rm message`;}
			$activemessage = "";
			$statusline2->push($context_id2, "Outgoing mailqueue deleted...");
		},

		on_clear_mailfile1_activate => 
		sub {
			`cat $ENV{HOME}/.pskmail/clientout >> $ENV{HOME}/.pskmail/mailarchive`;
			`echo "" > $ENV{HOME}/.pskmail/clientout`;
			`echo "" > $ENV{HOME}/.pskmail/.cpclientout`;
		},
		on_clear_headers1_activate => 
		sub {
			`echo "" > $ENV{HOME}/.pskmail/headerlist`;
			$headerbuffer->set_text("");
			$LastHeader = 0;
			$statusline2->push($context_id2, "Header list deleted...");
		},
		on_clearbulletins_activate => 
		sub {
			`echo "" > $ENV{HOME}/.pskmail/downloads/bulletins`;
			$statusline2->push($context_id2, "Bulletins deleted...");
		},
		on_mailmode_activate => 
		sub {
			$mode = "MAIL";
			$Scanningmode = 0;
			if (-e "$ENV{HOME}/.pskmail/.scanning") { unlink "$ENV{HOME}/.pskmail/.scanning";}
			$statusline1->push($context_id, "Setting $mode mode");
			$mode_label->set_text("MAIL/APRS");
			`echo "MAIL" > $ENV{HOME}/.pskmail/.pskmailmode`;
			`cp $ENV{HOME}/.pskmail/.dopositcopy $ENV{HOME}/.pskmail/.doposit`;

		},
		on_mail_scanning1_activate => 
		sub {
			$mode = "MAIL";
			$Scanningmode = 1;
			`touch "$ENV{HOME}/.pskmail/.scanning"`;
			$statusline1->push($context_id, "Setting scanning mode");
			$mode_label->set_text("MAIL/SCAN");
			`echo "MAIL" > $ENV{HOME}/.pskmail/.pskmailmode`;
			`cp $ENV{HOME}/.pskmail/.dopositcopy $ENV{HOME}/.pskmail/.doposit`;

		},
		on_tty1_activate => 
		sub {
			$mode = "TTY";
			$Scanningmode = 0;
			if (-e "$ENV{HOME}/.pskmail/.scanning") { unlink "$ENV{HOME}/.pskmail/.scanning";}

				open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
				print SESSIONDATA "none";
				close SESSIONDATA;
			`killall rflinkclient.pl`;
			$connected = 0;
			`cp $ENV{HOME}/.pskmail/.doposit $ENV{HOME}/.pskmail/.dopositcopy`;
			`unlink $ENV{HOME}/.pskmail/.doposit`;
			`echo "TTY" > $ENV{HOME}/.pskmail/.pskmailmode`;

			$statusline1->push($context_id, "Setting $mode mode");
			$mode_label->set_text("CHAT");

		},
		on_psk63_activate => 
		sub {
			open SESSION, "$ENV{HOME}/.pskmail/PSKmailsession";
			my $session = <SESSION>;
			chomp $session;
			close SESSION;
			
			if ($session eq "Connected" && $Modem ne "PSK63") {	
				`echo "~SPEED63!" > $ENV{HOME}/.pskmail/TxInputfile`;
				$Modem = "PSK63";
				$statusline1->push($context_id, "Request PSK63");
#				sleep 10;				
			} else {	
				open (CMD, ">$ENV{HOME}/.pskmail/.modem");
				print CMD "PSK63";
				close (CMD);
				$Modem = "PSK63";
				sendmode($Modem);
				$statusline1->push($context_id, "Setting $Modem mode");
				$speedbox->set_text("PSK63");
			}
		},
		on_psk125_activate => 
		sub {
			open SESSION, "$ENV{HOME}/.pskmail/PSKmailsession";
			my $session = <SESSION>;
			chomp $session;
			close SESSION;
			
			if ($session eq "Connected" && $Modem ne "PSK125") {	
				`echo "~SPEED125!" > $ENV{HOME}/.pskmail/TxInputfile`;
				$Modem = "PSK125";
				$statusline1->push($context_id, "Request PSK125");
#				sleep 10;				
			} else {	
				open (CMD, ">$ENV{HOME}/.pskmail/.modem");
				print CMD "PSK125";
				close (CMD);
				$Modem = "PSK125";
				sendmode($Modem);
				$statusline1->push($context_id, "Setting $Modem mode");
				$speedbox->set_text("PSK125");
			}
			
		},
		on_PSK250_activate => 
		sub {
			open SESSION, "$ENV{HOME}/.pskmail/PSKmailsession";
			my $session = <SESSION>;
			chomp $session;
			close SESSION;
			
			if ($session eq "Connected" && $Modem ne "PSK250") {	
				`echo "~SPEED250!" > $ENV{HOME}/.pskmail/TxInputfile`;
				$Modem = "PSK250";
				$statusline1->push($context_id, "Request PSK250");
#				sleep 10;				
			} else {	
				open (CMD, ">$ENV{HOME}/.pskmail/.modem");
				print CMD "PSK250";
				close (CMD);
				$Modem = "PSK250";
				sendmode($Modem);
				$statusline1->push($context_id, "Setting $Modem mode");
				$speedbox->set_text("PSK250");
			}
		},
		on_dominoex8_activate => 
		sub {
			open (CMD, ">$ENV{HOME}/.pskmail/.modem");
			print CMD "DOMINOEX8";
			close (CMD);
			$Modem = "DOMINOEX8";
			sendmode($Modem);
			$statusline1->push($context_id, "Setting $Modem mode");
		},
		on_dominoex11_activate => 
		sub {
			open (CMD, ">$ENV{HOME}/.pskmail/.modem");
			print CMD "DOMINOEX11";
			close (CMD);
			$Modem = "DOMINOEX11";
			sendmode($Modem);
			$statusline1->push($context_id, "Setting $Modem mode");
		},
		on_mfsk64_activate => 
		sub {
			open SESSION, "$ENV{HOME}/.pskmail/PSKmailsession";
			my $session = <SESSION>;
			chomp $session;
			close SESSION;
			
			if ($session eq "Connected" && $Modem ne "MFSK64") {	
				`echo "~SPEEDMFSK64!" > $ENV{HOME}/.pskmail/TxInputfile`;
				$Modem = "MFSK64";
				$statusline1->push($context_id, "Request MFSK64");
#				sleep 10;				
			} else {	
				open (CMD, ">$ENV{HOME}/.pskmail/.modem");
				print CMD "MFSK64";
				close (CMD);
				$Modem = "MFSK64";
				sendmode($Modem);
				$statusline1->push($context_id, "Setting $Modem mode");
				$speedbox->set_text("MFSK64");
			}
		},
		on_thor22_activate => 
		sub {
			open SESSION, "$ENV{HOME}/.pskmail/PSKmailsession";
			my $session = <SESSION>;
			chomp $session;
			close SESSION;
			
			if ($session eq "Connected" && $Modem ne "THOR22") {	
				`echo "~SPEEDTHOR22!" > $ENV{HOME}/.pskmail/TxInputfile`;
				$Modem = "THOR22";
				$statusline1->push($context_id, "Request THOR22");
#				sleep 10;				
			} else {	
				open (CMD, ">$ENV{HOME}/.pskmail/.modem");
				print CMD "THOR22";
				close (CMD);
				$Modem = "THOR22";
				sendmode($Modem);
				$statusline1->push($context_id, "Setting $Modem mode");
				$speedbox->set_text("THOR22");
			}
		},
		on_QSY_activate => 
		sub {
			open SESSION, "$ENV{HOME}/.pskmail/PSKmailsession";
			my $session = <SESSION>;
			chomp $session;
			close SESSION;
			
			if ($session eq "Connected") {			
				`echo "~QSY!" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$statusline1->push($context_id, "QSY to traffic channel");
			} else {
				$statusline1->push($context_id, "You are not connected!");			
			}
		},
		on_help1_activate => 
		sub {
			$statusline2->push($context_id2, "Viewing readme file...");
			open (HELPFILE, "PSKmail.README");
			my @helparray = <HELPFILE>;
			close (HELPFILE);
			$helptext = join "", @helparray;
			$buffer->set_text($helptext);			
		},
		on_Tidestations_activate => 
		sub {
				`echo "~GETTIDESTN" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$statusline1->push($context_id, "get tide stn list");
		},
		on_Tide_activate => 
		sub {
				my $number = $Main_entry->get_text;
				`echo "~GETTIDE $number" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$statusline1->push($context_id, "get tide for $number");
		},
		on_getnear_activate => 
		sub {
				`echo "~GETNEAR" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$statusline1->push($context_id, "get stns near me");
		},
		on_Getcamper_activate => 
		sub {
			my $lat = $GPSLatentry->get_text;
			my $lon = $GPSLonentry->get_text;
				`echo "~GETCAMP $lat $lon" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$statusline1->push($context_id, "get camper parkings");
		},
		on_getservers_activate => 
		sub {
				`echo "~GETSERVERS" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$statusline1->push($context_id, "get server freqs");
		},
		on_getnews_activate => 
		sub {
				`echo "~GETNEWS" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$statusline1->push($context_id, "get pskmail news");
		},
		on_get_page1_activate => 
		sub {
			$http_window->show();
		},
		on_ping1_activate => 
		sub {
			send_ping();
		},
		on_message1_activate => 
		sub {
			my $string = $Main_entry->get_text;
			if ($string){
				if ($string =~ m/\w+\.*\w+\@\S* /) {
					send_uimessage($string);
#					$Main_entry->set_text("");
				} else {
					send_aprsmessage($string);
					# add message queue for stn-to-stn messages (end to end ack)
				}
			} else {
				$statusline2->push($context_id2, "No message...");
			}
		},
		on_link_to_gate1_activate => 
		sub {
			send_linkreq();
				system ("killall rflinkclient.pl");
				sleep 1;
				system ("/usr/local/share/pskmail/rflinkclient.pl &> /dev/null & ");
			
								
		},
		on_link_test1_activate => 
		sub {
			
			`echo "~TEST" >> $ENV{HOME}/.pskmail/TxInputfile`;
			$statusline1->pop($context_id);
			$statusline1->push($context_id, "Starting link test...");
								
		},
		on_beacons_hrd1_activate => 
		sub {
			
			`echo "~GETBEACONS" >> $ENV{HOME}/.pskmail/TxInputfile`;
			$statusline1->pop($context_id);
			$statusline1->push($context_id, "Get beacons...");
								
		},
		on_server_status1_activate => 
		sub {
			
			`echo "~STATUS" >> $ENV{HOME}/.pskmail/TxInputfile`;
			$statusline1->pop($context_id);
			$statusline1->push($context_id, "Get server status...");
								
		},
		on_list2_activate => 
		sub {
			
			`echo "~LISTLOCAL" >> $ENV{HOME}/.pskmail/TxInputfile`;
			$statusline1->pop($context_id);
			$statusline1->push($context_id, "List local mail...");
								
		},
		on_read1_activate => 
		sub {
			my $string = $Main_entry->get_text;
			if ($string =~ /\d+/) {
				$statusline1->push($context_id, "Reading local " . "$string");
				`echo "~READLOCAL $string" >> $Outputfile`; 
			} else {
				$statusline1->push($context_id, "Reading local " . "1");
				`echo "~READLOCAL $string" >> $Outputfile`; 							
			}
			$Main_entry->set_text ("");			
								
		},
		on_delete1_activate => 
		sub {
			my $string = $Main_entry->get_text;
			if ($string =~ /\d+/) {
				$statusline1->push($context_id, "Deleting local" . "$string");
				`echo "~DELETELOCAL $string" >> $Outputfile`; 
			} else {
				$statusline1->push($context_id, "Deleting local " . "1");
				`echo "~DELETELOCAL $string" >> $Outputfile`; 							
			}
			$Main_entry->set_text ("");			
								
		},
		on_listfiles1_activate => 
		sub {
			
			`echo "~LISTFILES" >> $ENV{HOME}/.pskmail/TxInputfile`;
			$statusline1->pop($context_id);
			$statusline1->push($context_id, "List files...");
								
		},
		on_get_txt_file1_activate => 
		sub {
			my $string = $Main_entry->get_text;
				$statusline1->push($context_id, "Reading file " . "$string");
				`echo "~GETFILE $string" >> $Outputfile`; 
			$Main_entry->set_text ("");			
								
		},
		on_get_bin_file1_activate => 
		sub {
			my $string = $Main_entry->get_text;
				$statusline1->push($context_id, "Get file " . "$string");
				`echo "~GETBIN $string" >> $Outputfile`; 
			$Main_entry->set_text ("");			
								
		},
		on_view_file_active =>
		sub {
			$fileupload->set_action("open");
			$fileupload->set_current_folder ("$ENV{HOME}/.pskmail/downloads");
			my $stat = $fileupload->run();
			if ($stat eq 'ok') {
				my $myfile = $fileupload->get_filename;
				my $content = `cat $myfile`;
				$fileviewbuffer->set_text($content);
			}
			
			$fileupload->hide();
			
		},
		on_upload_file1_activate => 
		sub {
			$fileupload->set_current_folder ("$ENV{HOME}/.pskmail/pskuploads");
			my $stat = $fileupload->run();
			if ($stat eq 'ok') {
				$filenm = $fileupload->get_filename;
				my $grb = "";
				if (-e "$filenm") {
					if (substr ($filenm, -2) ne "gz" && substr ($filenm, -3) ne "bz2") {
						`/usr/local/share/pskmail/paq864 $filenm`;
						$filenm .= ".864";
						$grb = `cat $filenm`;
						`/usr/local/share/pskmail/unpaq864 $filenm`;	
						`rm $filenm`;			
					} else {
						$grb = `cat $filenm`;
						$grb = encode_base64 ($grb);
					}
					$lqrb = length ($grb);
					$filenm =~ s{/\S+/}{};
					`echo "Your file:$filenm $lqrb" >> $ENV{HOME}/.pskmail/TxInputfile`;
					`echo "$grb" >> $ENV{HOME}/.pskmail/TxInputfile`;				
					`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
					unlink "$ENV{HOME}/.pskmail/$filenm.864";
					$statusline1->pop($context_id);	
					$statusline1->push($context_id, "Sending file $filenm");
				} else {
					$statusline1->pop($context_id);	
					$statusline1->push($context_id, "File " . "$filenm" . " not found!");
				}
			} else {
				$filenm = "";
			}

			$Main_entry->set_text ("");
			$fileupload->hide();			
								
		},
		on_scrolledwindow5_show =>
		sub {
			$fileupload->set_action("open");
			$fileupload->set_current_folder ("$ENV{HOME}/.pskmail/downloads");
			my $stat = $fileupload->run();
			if ($stat eq 'ok') {
				my $myfile = $fileupload->get_filename;
				my $content = `cat $myfile`;
				$fileviewbuffer->set_text($content);
			}
			
			$fileupload->hide();
			
		},
		on_mailfile1_activate => 
		sub {
			$statusline2->push($context_id2, "Viewing mail file...");

			open (TEXTIN, "$ENV{HOME}/.pskmail/clientout");
			my @textin = <TEXTIN>;
			close (TEXTIN);
			for (my $yy = 0; $yy < @textin ; $yy++) {
				if ($textin[$yy] =~ m/===TIME!/) {
					$textin[$yy] = "";				
				}			
				if ($textin[$yy] =~ m/==Connected/) {
					$textin[$yy] = "";				
				}			
				if ($textin[$yy] =~ m/==Disconnected/) {
					$textin[$yy] = "";				
				}			
			}
			$end_mark = $buffer->create_mark ('end', $buffer->get_end_iter, FALSE);
			$buffer->set_text(join "", @textin);
			$end_mark = $buffer->create_mark ('end', $buffer->get_end_iter, FALSE);
			$textview->scroll_mark_onscreen ($end_mark);
		},
		on_headers1_activate => 
		sub {
			if (-e "$ENV{HOME}/.pskmail/headerlist") {
				$statusline2->push($context_id2, "Viewing headers file...");

				open (TEXTIN, "$ENV{HOME}/.pskmail/headerlist");
				my @textin = <TEXTIN>;
				close (TEXTIN);
				$header_end_mark = $headerbuffer->create_mark ('end', $headerbuffer->get_end_iter, FALSE);
				$headerbuffer->set_text(join "", @textin);
				$header_end_mark = $headerbuffer->create_mark ('end', $headerbuffer->get_end_iter, FALSE);
				$headerview->scroll_mark_onscreen ($end_mark);
			} else {
				$statusline2->push($context_id2, "Headers file empty...");
			}
		},
		on_mail_archive1_activate => 
		sub {
			$statusline2->push($context_id2, "Viewing archive...");

			open (TEXTIN, "mailarchive");
			my @textin = <TEXTIN>;
			close (TEXTIN);
			$end_mark = $buffer->create_mark ('end', $buffer->get_end_iter, FALSE);
			$buffer->set_text(join "", @textin);
			$end_mark = $buffer->create_mark ('end', $buffer->get_end_iter, FALSE);
			$textview->scroll_mark_onscreen ($end_mark);
		},

		on_readme1_activate => 
		sub {
			$statusline2->push($context_id2, "Viewing readme file...");
			open (HELPFILE, "PSKmail.README");
			my @helparray = <HELPFILE>;
			close (HELPFILE);
			$helptext = join "", @helparray;
			$buffer->set_text($helptext);			
		},
		on_get_position1_activate => 
		sub {
			`echo "~GETPOS\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
			$statusline1->pop($context_id);
			$statusline1->push($context_id, "Checking position...");
		},
		on_get_messages1_activate => 
		sub {
			`echo "~GETMSG\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
			$statusline1->pop($context_id);
			$statusline1->push($context_id, "Checking messages...");
		},
		on_rpt_beacons1_activate  => 
		sub {
			if ($shortbeacon) {
				if (-e "$ENV{HOME}/.pskmail/.shortbeacon") { unlink "$ENV{HOME}/.pskmail/.shortbeacon"};
				$shortbeacon = 0;
			} else {
				`touch $ENV{HOME}/.pskmail/.shortbeacon`;
				$shortbeacon = 1;
			}
			$statusline1->pop($context_id);
			$statusline1->push($context_id, "Using Rpt bcn...");
		},
		on_spinbutton6_change_value	 => 
		sub {
			my $newminute = $minuteinput->get_value;
			$QRG_entry->set_text($newminute);
			open (OPTIONS, ">$ENV{HOME}/.pskmail/.PSKoptions");
			print OPTIONS $Call_entry->get_text(), "\n";
			print OPTIONS $Server_entry->get_text(), "\n";
			print OPTIONS $Blocklen_entry->get_text(), "\n";
			print OPTIONS $Lat_entry->get_text(), "\n";
			print OPTIONS $Lon_entry->get_text(), "\n";
			print OPTIONS $QRG_entry->get_text(), "\n";
			close (OPTIONS);
		},	
		on_comboboxentry1_changed =>
		sub {
			my $newserver = $server_entry_spin->get_active_text;
			$newserver = uc($newserver);
			$Server_entry->set_text($newserver);
			open (OPTIONS, ">$ENV{HOME}/.pskmail/.PSKoptions");
			print OPTIONS $Call_entry->get_text(), "\n";
			print OPTIONS $Server_entry->get_text(), "\n";
			print OPTIONS $Blocklen_entry->get_text(), "\n";
			print OPTIONS $Lat_entry->get_text(), "\n";
			print OPTIONS $Lon_entry->get_text(), "\n";
			print OPTIONS $QRG_entry->get_text(), "\n";
			close (OPTIONS);
				my $serverentry = $Server_entry->get_text();
				my $callentry = $Call_entry->get_text();
				`echo "$serverentry" >> $ENV{HOME}/.pskmail/calls.txt`;
				`echo "$callentry" >> $ENV{HOME}/.pskmail/calls.txt`;
				`cat $ENV{HOME}/.pskmail/calls.txt | sort | uniq > $ENV{HOME}/.pskmail/tmpcalls`;
				`mv $ENV{HOME}/.pskmail/tmpcalls $ENV{HOME}/.pskmail/calls.txt`;
		},
		on_maintext_insert_at_cursor =>
		sub {
			$end_mark = $buffer->create_mark ('end', $buffer->get_end_iter, FALSE);
			$textview->scroll_mark_onscreen ($end_mark);

		},
		on_textview1_insert_at_cursor =>
		sub {
			$end_mark = $msgtextbuffer->create_mark ('end', $msgtextbuffer->get_end_iter, FALSE);
			$msgtextview->scroll_mark_onscreen ($end_mark);

		},

		on_comboboxentryicon_changed =>
		sub {
			my $statustxt = $GPSIconentry->get_active_text;
			my @conf;
			$conf[0] = 0;
			$conf[1] = 0;
			$conf[2] = 0;
			$conf[3] = $infile_field->get_text;
			$conf[4] = $outfile_field->get_text;
			$conf[5] = $logfile_field->get_text;
			$conf[6] = $max_retries_field->get_value;
			$conf[7] = $idle_field->get_value;
			$conf[8] = $txd_field->get_value;
#			$conf[9] = $GPSIconentry->get_active_text;
			$conf[9] = $APRSIcon->get_text;
			$conf[10] = $GPSCommententry->get_text;
			$conf[11] = $offset_minute_field->get_value;
			$conf[12] = $beaconsecond_field->get_value;
			
			$positsign_field->set_text($statustxt);
			
			open (SAVECONF, ">$ENV{HOME}/.pskmail/.pskmailconf");
			$myconf = join (",", @conf);
			print SAVECONF $myconf, "\n";
			close (SAVECONF);
				
		},

		on_entry2_changed =>
		sub {
			my $statustxt = $GPSCommententry->get_text();
			my @conf;
			$conf[0] = 0;
			$conf[1] = 0;
			$conf[2] = 0;
			$conf[3] = $infile_field->get_text;;
			$conf[4] = $outfile_field->get_text;
			$conf[5] = $logfile_field->get_text;
			$conf[6] = $max_retries_field->get_value;
			$conf[7] = $idle_field->get_value;
			$conf[8] = $txd_field->get_value;
			$conf[9] = $GPSIconentry->get_active_text;
			$conf[10] = $GPSCommententry->get_text;
			$conf[11] = $offset_minute_field->get_value;
			$conf[12] = $beaconsecond_field->get_value;
			
			$positmsg_field->set_text($statustxt);
		$statusline1->push($context_id, $statustxt);
			
			open (SAVECONF, ">$ENV{HOME}/.pskmail/.pskmailconf");
			$myconf = join (",", @conf);
			print SAVECONF $myconf, "\n";
			close (SAVECONF);
				
		},

		on_checkbutton4_toggled => 
		sub {
			$statusline1->push($context_id, "WX beacon...");
			my $wxactive = `cat $ENV{HOME}/.pskmail/.doposit`;
			
			if ($wxactive =~ /WX/) {	
				$statusline1->push($context_id, "No WX beacon...");
				my $wxperiod = $APRSperiod->get_active_text;
				
				`echo $wxperiod > $ENV{HOME}/.pskmail/.doposit`;
				`echo $wxperiod > $ENV{HOME}/.pskmail/.dopositcopy`;

			} else {
				$statusline1->push($context_id, "Start WX beacon...");
				my $wxperiod = $APRSperiod->get_active_text;
				my $wxflag = $wxperiod . "WX";
				`echo $wxflag > $ENV{HOME}/.pskmail/.doposit`;
				`echo $wxflag > $ENV{HOME}/.pskmail/.dopositcopy`;
			}
			1;
		},
		
		on_comboboxentryperiod_changed =>
		sub {
			my $periodvalue = $APRSperiod->get_active_text;
			$statusline1->push($context_id, "Changing period...");
			if (-e "$ENV{HOME}/.pskmail/.wx") {
				$checkwx = `cat $ENV{HOME}/.pskmail/.wx`;
			} else {
				$checkwx = "";
			}
			if ($checkwx =~ /Wind/) {
					$periodvalue .= "WX";
			}
			$periodvalue = $APRSperiod->get_active_text;			
			`echo $periodvalue > $ENV{HOME}/.pskmail/.doposit`;
			`echo $periodvalue > $ENV{HOME}/.pskmail/.dopositcopy`;
		}


);




$window->signal_connect (destroy => sub { 			
			if (-e "$ENV{HOME}/.pskmail/headerlist") {
				send_to_mbox() ;
			}

			sendmodemcommand ("normal");			

			`cp $ENV{HOME}/.pskmail/.cpclientout $ENV{HOME}/.pskmail/clientout`;
			if (-e "$ENV{HOME}/.pskmail/id_defined") { 
				unlink "$ENV{HOME}/.pskmail/id_defined";
			}
			untie %memreceive;
			kill('KILL', $pid);
			wait();
			Gtk2->main_quit;
 });

$buffer->signal_connect (insert_text => sub {
			$end_mark = $buffer->create_mark ('end', $buffer->get_end_iter, FALSE);
			$textview->scroll_mark_onscreen ($end_mark);
});

$monitorbuffer->signal_connect (insert_text => sub {
			$monitor_end_mark = $monitorbuffer->create_mark ('end', $monitorbuffer->get_end_iter, FALSE);
			$monitorview->scroll_mark_onscreen ($monitor_end_mark);
});

$fileviewbuffer->signal_connect (insert_text => sub {
			$fileview_end_mark = $fileviewbuffer->create_mark ('end', $fileviewbuffer->get_end_iter, FALSE);
			$fileview->scroll_mark_onscreen ($fileview_end_mark);
});

$headerbuffer->signal_connect (insert_text => sub {
			$header_end_mark = $headerbuffer->create_mark ('end', $headerbuffer->get_end_iter, FALSE);
			$headerview->scroll_mark_onscreen ($header_end_mark);
});
####################################################
#actions from the toolbar
####################################################

$Main_entry->signal_connect(activate =>
		sub {
			open SESSION, "$ENV{HOME}/.pskmail/PSKmailsession";
			my $session = <SESSION>;
			chomp $session;
			close SESSION;
			if ($session =~ /Connected/) {
				my $string = $Main_entry->get_text;
				
				if ($string =~ /~GETCAMP/) {
					$string = $string . " " . $Lat_entry->get_text() . " " . $Lon_entry->get_text();
				}
				
				if ($mode eq "MAIL") {
					$string .= "\n";
				} else {
					$buffer->insert($buffer->get_end_iter, ">>" . $string . "\n");
				}
				`echo "$string" >> $Outputfile`;
				$Main_entry->set_text("");
				chomp $string;
				if ($mode eq "TTY") {
					$statusline1->push($context_id, "");	
				} else {
					$statusline1->push($context_id, "Sending $string");
				}
			} else {
				my $string = $Main_entry->get_text;
				if ($string){
					if ($string =~ m/\w+\.*\w+\@\S* /) {
						send_uimessage($string);
	#					$Main_entry->set_text("");
					} else {
						if ($string =~ /(\S*)(\s.*)/){
							my $call = uc($1);
							$string = $call . $2;
						}
						my $NR;
						if ($string ne $lastmessage) {
							$NR = get_MSGNR();
							$lastmessage = $string;
						} else {
							$NR = get_last_MSGNR();
						}
						if ($string =~ /^\d*\w*\d+\w*\-*\w*.*/) { 
							$string .= $NR;
						}
						$msgtextbuffer->insert($msgtextbuffer->get_end_iter, ">>" . $string .  "\n");

						send_aprsmessage($string);
				# add message queue for stn-to-stn messages (end to end ack)
						
					}
				} else {
					$statusline2->push($context_id2, "No message...");
				}
				
			}
		}
);


$options_button->signal_connect(clicked =>
		sub {
			if (-s "$ENV{HOME}/.pskmail/.PSKoptions") {
				open (OPTIONS, "$ENV{HOME}/.pskmail/.PSKoptions");
				@options = <OPTIONS>;		
				close (OPTIONS);
				
				chomp $options[0];
				chomp $options[1];
				chomp $options[2];
				chomp $options[3];
				chomp $options[4];
				chomp $options[5];
				
				$Call_entry->set_text($options[0]);
				$Server_entry->set_text($options[1]);
				$Blocklen_entry->set_text($options[2]);
				$Lat_entry->set_text($options[3]);
				$Lon_entry->set_text($options[4]);
				$QRG_entry->set_text($options[5]);					
				my $serverentry = $Server_entry->get_text();
				my $callentry = $Call_entry->get_text();
				`echo "$serverentry" >> $ENV{HOME}/.pskmail/calls.txt`;
				`echo "$callentry" >> $ENV{HOME}/.pskmail/calls.txt`;
				`cat $ENV{HOME}/.pskmail/calls.txt | sort | uniq > $ENV{HOME}/.pskmail/tmpcalls`;
				`mv $ENV{HOME}/.pskmail/tmpcalls $ENV{HOME}/.pskmail/calls.txt`;
			}
			$options_window->show();	


 		}
);

$Options_cancel->signal_connect(clicked =>
		sub {
			$options_window->hide();
		}
);

$Options_ok->signal_connect(clicked =>
		sub {
			if (! -e "$ENV{HOME}/.pskmail/.PSKoptions") {
				$restart = 1;
			}
			my $printcall = uc($Call_entry->get_text());
			my $printserver = uc($Server_entry->get_text());
			open (OPTIONS, ">$ENV{HOME}/.pskmail/.PSKoptions");
			print OPTIONS $printcall, "\n";
			print OPTIONS $printserver, "\n";
			print OPTIONS $Blocklen_entry->get_text(), "\n";
			print OPTIONS $Lat_entry->get_text(), "\n";
			print OPTIONS $Lon_entry->get_text(), "\n";
			print OPTIONS $QRG_entry->get_text(), "\n";
			$offset = $QRG_entry->get_text();
			close (OPTIONS);
				my $serverentry = uc($Server_entry->get_text());
				my $callentry = uc($Call_entry->get_text());
				`echo "$serverentry" >> $ENV{HOME}/.pskmail/calls.txt`;
				`echo "$callentry" >> $ENV{HOME}/.pskmail/calls.txt`;
				`cat $ENV{HOME}/.pskmail/calls.txt | sort | uniq > $ENV{HOME}/.pskmail/tmpcalls`;
				`mv $ENV{HOME}/.pskmail/tmpcalls $ENV{HOME}/.pskmail/calls.txt`;
				if ($firstconfig) {
					$firstconfig = 0;
					$statusline1->push($context_id, "STOP and Restart program...");
					
				}
				$options_window->hide();

				open (OPTIONS, "$ENV{HOME}/.pskmail/.PSKoptions");
				@options = <OPTIONS>;		
				close (OPTIONS);
				
				chomp $options[0];
				chomp $options[1];
				chomp $options[2];
				chomp $options[3];
				chomp $options[4];
				chomp $options[5];
				$Call_entry->set_text(uc($options[0]));
				$Server_entry->set_text(uc($options[1]));
				$Blocklen_entry->set_text($options[2]);
				$Lat_entry->set_text($options[3]);
				$Lon_entry->set_text($options[4]);
				$QRG_entry->set_text($options[5]);
				$offset = $options[5];	
				
				if ($restart) {
					exec ("./psk_arq.pl");
				}		
		}
);
$about_ok->signal_connect(clicked =>
		sub {
			$about_window->hide();
		}
);

$Wxset->signal_connect(clicked =>
		sub {

			$WXdialog->show();
		}
);
		
$Cancelwx->signal_connect(clicked =>
		sub {
			$WXdialog->hide();
		}
);

$Savewx->signal_connect(clicked =>
		sub {
			my $wind = $Windspeed->get_value;
			my $winddir = $Wdircombo->get_active_text;
			my $sea = $Sea->get_value;
			my $seadir = $Seadircombo->get_active_text;
			my $HP = $Hpspin->get_value;
			my $temp = $Tempspin->get_value;
			my $wxst = $Wxstat->get_active_text;
			my $wxstring = sprintf("/Wind:%d,%s/Sea:%d,%s/Hp:%d/T:%d%s",$wind, $winddir, $sea, $seadir, $HP, $temp, $wxst);

			$Wxentry->set_text($wxstring);
			`echo $wxstring > $ENV{HOME}/.pskmail/.wx`;
			$WXdialog->hide();
			$statusline1->push($context_id, "Wx set");
		}
);

$Positionbutton->signal_connect(clicked => \&sendposit);

$Position_button->signal_connect(clicked => \&sendposit);


$http_cancelbutton->signal_connect(clicked =>
		sub {
			$http_window->hide();
		}
);
$http_storebutton->signal_connect(clicked =>
		sub {
			open ($fh,">PSKmail_url_list" );
			print $fh $http_entry1->get_text . "," .
				$http_entry2->get_text . "," .
				$http_entry3->get_text . "," .
				$http_entry4->get_text . "," .
				$http_entry5->get_text . "," .
				$http_entry6->get_text . "\n";
			close $fh;
			
			$http_window->hide();			
		}
);
$http_button1->signal_connect(clicked =>
		sub {
			my $url = $http_entry1->get_text;
			`echo "~TGET $url" >> $ENV{HOME}/.pskmail/TxInputfile`;
			$http_window->hide();
			$statusline1->push($context_id, "Requesting $url...");
			resetprogress ();
		}
);
$http_button2->signal_connect(clicked =>
		sub {
			my $url = $http_entry2->get_text;
			`echo "~TGET $url" >> $ENV{HOME}/.pskmail/TxInputfile`;
			$http_window->hide();
			$statusline1->push($context_id, "Requesting $url...");
			resetprogress ();
		}
);
$http_button3->signal_connect(clicked =>
		sub {
			my $url = $http_entry3->get_text;
			`echo "~TGET $url" >> $ENV{HOME}/.pskmail/TxInputfile`;
			$http_window->hide();
			$statusline1->push($context_id, "Requesting $url...");
			resetprogress ();
		}
);
$http_button4->signal_connect(clicked =>
		sub {
			my $url = $http_entry4->get_text;
			`echo "~TGET $url" >> $ENV{HOME}/.pskmail/TxInputfile`;
			$http_window->hide();
			$statusline1->push($context_id, "Requesting $url...");
			resetprogress ();
		}
);
$http_button5->signal_connect(clicked => 
		sub {
			my $url = $http_entry5->get_text;
			`echo "~TGET $url" >> $ENV{HOME}/.pskmail/TxInputfile`;
			$http_window->hide();
			$statusline1->push($context_id, "Requesting $url...");
			resetprogress ();
		}
);
$http_button6->signal_connect(clicked =>
		sub {
			my $url = $http_entry6->get_text;
			`echo "~TGET $url" >> $ENV{HOME}/.pskmail/TxInputfile`;
			$http_window->hide();
			$statusline1->push($context_id, "Requesting $url...");
			resetprogress ();
		}
);
$connect_button->signal_connect(clicked => \&connect_pskmail);


$exit_button->signal_connect(clicked => 

		sub {
			if (-e "$ENV{HOME}/.pskmail/headerlist") {
				send_to_mbox() ;
			}
			sendmodemcommand ("normal");
			if (-e "$ENV{HOME}/.pskmail/id_defined") { 
				unlink "$ENV{HOME}/.pskmail/id_defined";
			}
			`cp $ENV{HOME}/.pskmail/.cpclientout $ENV{HOME}/.pskmail/clientout`;
			untie %memreceive;
			kill('KILL', $pid);
			wait();
			Gtk2->main_quit;
 		}
);


$qtc_button->signal_connect(clicked =>
		sub {
			resetprogress ();

			purge_headers();
			my $hdr = $LastHeader + 1;
			`echo "~QTC $hdr+" >> $Outputfile`; 
			$statusline1->push($context_id, "Requesting headers from $hdr...");
#			print "Requesting Headers: ~QTC $hdr+\n ";

 		}
);

$download_button->signal_connect(clicked =>
		sub {
			my $string = $Main_entry->get_text;
			
			resetprogress ();
			
			$Progress->set_fraction(0);
			$Progress->set_text("0");

			if ($string =~ /\d+/) {
				$statusline1->push($context_id, "Downloading " . "$string");
				`echo "~READPAQ $string" >> $Outputfile`; 
				$Main_entry->set_text ("");			
			} elsif ($string =~ /\D+/) {
				$statusline1->push($context_id, "Reading " . "$string");
				`echo "~GETBIN $string" >> $Outputfile`; 
				$Main_entry->set_text ("");
			} else {
				$statusline1->push($context_id, "Reading " . "1");
				`echo "~READPAQ 1" >> $Outputfile`; 
			}
		}
);


$send_button->signal_connect(clicked =>
		sub {
			$activemessage = "" unless $activemessage;
			if ($activemessage eq "") {
				my @messagelist = `ls $ENV{HOME}/.pskmail/Outbox`;
				my $nextmessage = shift @messagelist;
				if ($nextmessage) {
					$activemessage = $nextmessage; 
					$wholemessage = "$ENV{HOME}/.pskmail/Outbox/" . $nextmessage;
					my $message = `cat $wholemessage`;
					if ($message) {
						if ($mode eq "TTY") {
							my @msglines = ();
							my @outlines = ();
							my $Toflg;
							my $Subjflg;
							my $Datflg;
							my $Fromflg;
							my $Sndflg;
							my @body = ();
							@msglines = split "\n", $message;
							foreach my $line (@msglines) {
								if ($line =~ /To: /) {
									$Toflg = $line;
								} elsif ($line =~ /Subject:/) {
									$Subjflg = $line;
								} elsif ($line =~ /Date:/){
									$Datflg = $line;
								} elsif ($line =~ /From: /) {
									$Fromflg = $line;
								} elsif ($line =~ /~SEND/) {
									$Sndflg = $line;
								} else {
									push @body, $line;
								}
							}
							@msglines = ();
							my $date;
							my $sz = length($message);
							if ($Sndflg) {
								push @msglines, "Your msg: $sz";
							}
							if ($Fromflg) {
								push @msglines, $Fromflg;
							} else {
								$date = maildate();
								if (-s "$ENV{HOME}/.pskmail/.record") {
									open (OPTIONS, "$ENV{HOME}/.pskmail/.record");
									my @roptions = <OPTIONS>;		
									close (OPTIONS);
									
									chomp $roptions[0];
									my @options = split /,/ , $roptions[0];
									
									$Mailoptionhost->set_text($options[0]);
									$Mailoptionuser->set_text($options[1]);
									$Mailoptionpass->set_text($options[2]);
									$Mailoptionreply->set_text($options[3]);
									$Mailoptionfindu->set_text($options[4]);
								}						

								my $fromaddress = $Mailoptionreply->get_text;
								my $outline = "From " . $fromaddress . " " . $date;
								push @msglines, $outline;
								push @msglines, "From: $fromaddress";	
							}
							if ($Datflg) {
								push @msglines, $Datflg;
							} else {
								push @msglines, "Date: " . $date;
							}
							if ($Toflg) {
								push @msglines, $Toflg;
							} 
							if ($Subjflg) {
								push @msglines, $Subjflg;
								push @msglines, "\n";
							}
							if ($body[0]) {
								push @msglines, @body
							}
							push @msglines, "-end-\n";
							
							$message = join "\n", @msglines;
							
						}
`echo $Outputfile >> $ENV{HOME}/.pskmail/test`;
`echo $message >> $ENV{HOME}/.pskmail/test`;

						open ($MF, ">>$Outputfile");
						print $MF $message;
						close ($MF);
						`echo $message >> $ENV{HOME}/.pskmail/Sent`;
						$statusline1->push($context_id, "Sending mail file...");
						print "Sending Mail\n ";
					} else {
						$statusline1->push($context_id, "No mail in file...");
					}
				}
			} 
 		}
);

$abort_button->signal_connect(clicked => \&abort );


$new_button->signal_connect(clicked =>
		sub {
			$To_entry->set_text("");
			$Subject_entry->set_text("");
			$messagebuffer->set_text("");
			$n_window->show();	
		}
);
$new_cancel->signal_connect(clicked =>
		sub {
			$n_window->hide();
		}
);
$new_ok->signal_connect(clicked =>
		sub {
			my $to = $To_entry->get_text();
			my $subj = $Subject_entry->get_text();
			
			`echo "~SEND" > $ENV{HOME}/.pskmail/messagebody`;
			`echo "To: $to" >> $ENV{HOME}/.pskmail/messagebody`;
			`echo "Subject: $subj" >> $ENV{HOME}/.pskmail/messagebody`;
			
			my $mesgbody = $messagebuffer->get_text($messagebuffer->get_start_iter, $messagebuffer->get_end_iter, 0);

			open ($fd, ">>$ENV{HOME}/.pskmail/messagebody");
			print $fd $mesgbody;
			close ($fd);

			if ($mode eq "TTY") {
				`echo " \n." >>$ENV{HOME}/.pskmail/$message`;
			} else {
				`echo " \n.\n." >>$ENV{HOME}/.pskmail/messagebody`;
			}
			
			my $dat = `date`;
			my $mailname = md5_base64($dat);
			$mailname =~ tr/\/\+//;
			$mailname = "$ENV{HOME}/.pskmail/Outbox/" . substr($mailname, -12);					
						
			`cat $ENV{HOME}/.pskmail/messagebody > $mailname`;
			
			if (-e "$ENV{HOME}/.pskmail/messagebody") { unlink "$ENV{HOME}/.pskmail/messagebody"};
			
			$n_window->hide;
 		}
);

$read_button->signal_connect(clicked =>
		sub {
			resetprogress ();
			
			my $string = $Main_entry->get_text;
			if ($string =~ /\d+/) {
				$statusline1->push($context_id, "Reading " . "$string");
				`echo "~READ $string" >> $Outputfile`; 
			} elsif ($string =~ /\D+/) {
				$statusline1->push($context_id, "Reading file " . "$string");
				`echo "~GETFILE $string" >> $Outputfile`; 				
			} else {
				$statusline1->push($context_id, "Reading " . "1");
				`echo "~READ $string" >> $Outputfile`; 							
			}
			$Main_entry->set_text ("");
		}
);
$read_entry->signal_connect(activate =>
		sub {
			my $string = $read_entry->get_text;
			if ($string eq "") {$string = "1"};
			$statusline1->push($context_id, "Reading " . "$string");
			`echo "~READ $string" >> $Outputfile`; 
			$read_entry->set_text ("");
			$r_window->hide();
		}
);

$delete_button->signal_connect(clicked =>
		sub {
			$d_window->show();			
 		}
);
$delete_entry->signal_connect(activate =>
		sub {
			my $string = $delete_entry->get_text;
			if ($string) {
				$statusline1->push($context_id, "Deleting " . "$string");
				`echo "~DELETE $string" >> $Outputfile`; 
			}
			$delete_entry->set_text ("");
			$d_window->hide();

			
		}
);
$Mailoptionokbutton->signal_connect(clicked =>	
		sub {
			my $outrecord =
				$Mailoptionhost->get_text . "," .
				$Mailoptionuser->get_text . "," .
				$Mailoptionpass->get_text . "," .
				$Mailoptionreply->get_text . "," .
				$Mailoptionfindu->get_text;	
				
				`echo "$outrecord" > $ENV{HOME}/.pskmail/.record`;	
				$Mailoptionwindow->hide();
		}
);
$Mailoptioncancelbutton->signal_connect(clicked =>	
		sub {
			$Mailoptionwindow->hide();	
		}
);

$confcancel_button->signal_connect(clicked =>	
		sub {
			$systemconf->hide();	
		}
);
$confsave_button->signal_connect(clicked =>	
		sub {
			$conf[0] = 0;
			$conf[1] = 0;
			$conf[2] = 0;
			$conf[3] = $infile_field->get_text;;
			$conf[4] = $outfile_field->get_text;
			$conf[5] = $logfile_field->get_text;
			$conf[6] = $max_retries_field->get_value;
			$conf[7] = $idle_field->get_value;
			$conf[8] = $txd_field->get_value;
			$conf[9] = $positsign_field->get_text;
			$conf[10] = $positmsg_field->get_text;
			$conf[11] = $offset_minute_field->get_value;
			$conf[12] = $beaconsecond_field->get_value;
			
			open (SAVECONF, ">$ENV{HOME}/.pskmail/.pskmailconf");
			$myconf = join (",", @conf);
			print SAVECONF $myconf, "\n";
			close (SAVECONF);
			$statusline1->push($context_id, "Saving config data");
			
			$systemconf->hide();	
		}
);

$debugbutton->signal_connect(clicked =>	
		sub {
			if ($conf[0]) {
				$conf[0] = 0;	
			} else {
				$conf[0] = 1;	
			}
		}
);
$monitorbutton->signal_connect(clicked =>	
		sub {
			if ($conf[1]) {
				$conf[1] = 0;	
			} else {
				$conf[1] = 1;	
			}
		}
);
$rawbutton->signal_connect(clicked =>	
		sub {
			if ($conf[2]) {
				$conf[2] = 0;	
			} else {
				$conf[2] = 1;	
			}
		}
);

$Filedownloadbutton->signal_connect(clicked =>	
		sub {
			if ($connected) {

			
			
				my $string = $Main_entry->get_text;
			
				resetprogress ();
			
				$Progress->set_fraction(0);
				$Progress->set_text("0");
	
				if ($string =~ /\d+/) {
					$statusline1->push($context_id, "Downloading " . "$string");
					`echo "~READPAQ $string" >> $Outputfile`; 
					$Main_entry->set_text ("");			
				} elsif ($string =~ /\D+/) {
					$statusline1->push($context_id, "Loading " . "$string");
					`echo "~GETBIN $string" >> $Outputfile`; 
					$Main_entry->set_text ("");
				} else {
					$statusline1->push($context_id, "Reading " . "1");
					`echo "~READPAQ 1" >> $Outputfile`; 
				}

			} else {
					$statusline1->push($context_id, "Pse connect first!");
				
			}
		}
);

$Filesbutton->signal_connect(clicked =>
		sub {
			$fileupload->set_action("open");
			$fileupload->set_current_folder ("$ENV{HOME}/.pskmail/downloads");
			my $stat = $fileupload->run();
			if ($stat eq 'ok') {
				my $myfile = $fileupload->get_filename;
				if ($myfile =~ /\.grb/ && -e "$ENV{HOME}/.pskmail/run_zyGrib") {
					my $grbcmd = "~/.pskmail/run_zyGrib " . $myfile;
					system ($grbcmd);
				} else {
					my $content = `cat $myfile`;
					$fileviewbuffer->set_text($content);
				}
			}
			
			$fileupload->hide();
		}
);

$Fileupdate->signal_connect(clicked =>
		sub {
			if ($connected) {
				$fileupload->set_action("open");
				$fileupload->set_current_folder ("$ENV{HOME}/.pskmail/downloads");
				my $stat = $fileupload->run();
				if ($stat eq 'ok') {
					my $myfile = $fileupload->get_filename;
					$myfile =~ s{^.*/}{};
					`echo "~GETBIN $myfile" > $ENV{HOME}/.pskmail/TxInputfile`;
					$statusline1->push($context_id, "Updating $myfile");
				}
				
				$fileupload->hide();

			} else {
				$statusline1->push($context_id, "Pse connect first!");
			}
			
		}
);

$Pingbutton->signal_connect(clicked =>
		sub {
			send_ping();
		}
);

$Linkbutton->signal_connect(clicked =>
		sub {
			send_linkreq();
				system ("killall rflinkclient.pl");
				sleep 1;
				system ("/usr/local/share/pskmail/rflinkclient.pl &> /dev/null & ");							
		}
);

$http_button7->signal_connect(clicked =>
		sub {
			if ($connected) {
				`echo "~GETTIDESTN\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$statusline1->pop($context_id);
				$statusline1->push($context_id, "Checking nearest tide stn...");
			} else {
				connect_pskmail();
				`echo "~GETTIDESTN\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$statusline1->pop($context_id);
				$statusline1->push($context_id, "Checking nearest tide stn...");				
			}
			$http_window1->hide();
			$Notebook->set_current_page (0);
		}
);

$http_button8->signal_connect(clicked =>
		sub {
			if ($connected) {
				my $number = $Main_entry->get_text;
				`echo "~GETTIDE $number" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$statusline1->push($context_id, "Checking tide table...");
			} else {
				connect_pskmail();
				my $number = $Main_entry->get_text;
				`echo "~GETTIDE $number" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$statusline1->pop($context_id);
				$statusline1->push($context_id, "Checking tide table...");				
			}
			$http_window1->hide();
			$Notebook->set_current_page (0);
		}
);

$http_button9->signal_connect(clicked =>
		sub {
			if ($connected) {
				`echo "~GETNEAR\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$statusline1->pop($context_id);
				$statusline1->push($context_id, "Checking nearest APRS stn...");
			} else {
				connect_pskmail();
				`echo "~GETNEAR\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$statusline1->pop($context_id);
				$statusline1->push($context_id, "Checking nearest APRS stn...");				
			}
			$http_window1->hide();
			$Notebook->set_current_page (0);
		}
);

$http_button10->signal_connect(clicked =>
		sub {
			if ($connected) {
				`echo "~GETMSG\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$statusline1->pop($context_id);
				$statusline1->push($context_id, "Checking APRS messages...");
			} else {
				connect_pskmail();
				`echo "~GETMSG\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$statusline1->pop($context_id);
				$statusline1->push($context_id, "Checking APRS messages...");				
			}
			$http_window1->hide();
			$Notebook->set_current_page (0);
		}
);

$http_button11->signal_connect(clicked =>
		sub {
			if ($connected) {
				my $lat = $GPSLatentry->get_text;
				my $lon = $GPSLonentry->get_text;
				`echo "~GETCAMP $lat $lon" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$statusline1->pop($context_id);
				$statusline1->push($context_id, "Checking camp sites...");
			} else {
				connect_pskmail();
				my $lat = $GPSLatentry->get_text;
				my $lon = $GPSLonentry->get_text;
				`echo "~GETCAMP $lat $lon" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$statusline1->pop($context_id);
				$statusline1->push($context_id, "Checking camp sites...");				
			}
			$http_window1->hide();
			$Notebook->set_current_page (0);
		}
);


$Getmsgbutton->signal_connect(clicked =>
	sub {
		$http_window1->show();
	}
);

$http_cancelbutton1->signal_connect(clicked =>
	sub {
		$http_window1->hide();		
	}
);

$Filelistbutton->signal_connect(clicked =>
	sub {
		if ($connected) {
			`echo "~LISTFILES" > $ENV{HOME}/.pskmail/TxInputfile`;
			$statusline1->push($context_id, "Requesting files listing");		
		} else {
			$statusline1->push($context_id, "Pse connect first!");		
		}

	}
);

$Emcbutton->signal_connect(clicked =>
	sub{
		$Emergencydialog->show();	
	}
);

$Emergencycancel->signal_connect(clicked =>
	sub{
		$Emergencydialog->hide();
		$Emergencystatus = 0;	
	}
);

$Sendemergency->signal_connect(clicked =>
	sub{
		if (-e "$ENV{HOME}/.pskmail/.lastqsl") {
			`rm $ENV{HOME}/.pskmail/.lastqsl`;
		}
		$SOSmessage = $Emergencyentry->get_text();
		$Emergencystatus = 1;
		sendposit();
	}
);

$Fileconnectbutton->signal_connect(clicked => \&connect_pskmail);

$Fileabortbutton->signal_connect(clicked => \&abort);

$Filequitbutton->signal_connect(clicked =>		
		sub {
			if (-e "$ENV{HOME}/.pskmail/headerlist") {
				send_to_mbox() ;
			}
			sendmodemcommand ("normal");
			if (-e "$ENV{HOME}/.pskmail/id_defined") { 
				unlink "$ENV{HOME}/.pskmail/id_defined";
			}
			`cp $ENV{HOME}/.pskmail/.cpclientout $ENV{HOME}/.pskmail/clientout`;
			untie %memreceive;
			kill('KILL', $pid);
			wait();
			Gtk2->main_quit;
 		}
);


getconfig();	# get configuration file
$Outputfile = "$ENV{HOME}/.pskmail/TxInputfile";
$Inputfile = "$ENV{HOME}/$conf[3]";
#$commandfile = ">$ENV{HOME}/$conf[4]";

if (-s "PSKmail_url_list") {
	open ($fh, "PSKmail_url_list");
	my $urls = <$fh>;
	close ($fh);
	my @adds = split ",", $urls;
	chomp $adds[5];
	$http_entry1->set_text($adds[0]);
	$http_entry2->set_text($adds[1]);
	$http_entry3->set_text($adds[2]);
	$http_entry4->set_text($adds[3]);
	$http_entry5->set_text($adds[4]);
	$http_entry6->set_text($adds[5]);
}					


	open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
	print SESSIONDATA "none";
	close SESSIONDATA;

$statusline1->push($context_id, "Listening...");


# make a comm pipe for text input from gMFSK...
pipe READHANDLE, WRITEHANDLE or die "Cannot open pipe..";
WRITEHANDLE->autoflush(1);

# fork off a child to do the input stuff...
our $pid = fork();
die "failed to fork" unless defined $pid;

# off goes the first child... reading in info from the gMFSK logfile and
# sending it to the add_watch routine waiting for \*READHANDLE

if ($pid == 0) {
	close (READHANDLE);
	getstuff(\*WRITEHANDLE);
	exit;
}

# see if gpsd is running

my $gpsd_running = `ps aux | grep 'gpsd /dev' | grep -v grep`;

if ($debug) {print $gpsd_running, "\n";}

#if yes, open a socket to it

if ($gpsd_running) {
	if ($debug) { print "gpsd running\n";}

	if (-e "$ENV{HOME}/.pskmail/.gpsval") {
		unlink "$ENV{HOME}/.pskmail/.gpsval";
	}
	

	unless ($gpspid = fork) {
	
#		$remote_host = "gpsd.rellim.com";
		$remote_host = "127.0.0.1";
		$remote_port = "2947";

		$gpssocket = IO::Socket::INET->new(PeerAddr => $remote_host,
		                                PeerPort => $remote_port,
		                                Proto    => "tcp",
		                                Type     => SOCK_STREAM)
	
		    or die "Couldn't connect to $remote_host:$remote_port : $@\n";
		    
		 	while (1) {	## GPS loop
		 	
		## do gps stuff
				
				print $gpssocket "o\n";
		
				sleep 1;

				my $answer = <$gpssocket>;

#old:GPSD,O=GSV 1214313572.83 0.005 47.676997 9.513997          ? 12.00 11.20   0.0000    0.000          ?             ? 12.13        ? 3
					
#new:GPSD,O=RMC 1214313795.00 0.005 47.676972 9.514190          ?        ?        ? 260.9800    0.087          ?             ?        ?        ? 
#                               GPSD,0=RMC     tm.x      0.005            47.6769      9.5141
				if ($answer =~ /GPSD,O=\S+\s(\d+)\.\d*\s(\d+\.\d*)\s(\-*\d+\.\d*)\s(\-*\d+\.\d*)\s+(\S+)\s+\d+\.\d*\s+\d+\.\d*\s+(\d+\.\d*)\s+(\d+\.\d*)/ ||
					$answer =~ /GPSD,O=\S+\s(\d+)\.\d*\s(\d+\.\d*)\s(\-*\d+\.\d*)\s(\-*\d+\.\d*)\s+(\S+)\s+\S+\s+\S+\s+(\d+\.\d*)\s+(\d+\.\d*)/) {
=head
					if ($debug) { 
						print $1, "\n";	
						print $3, "\n";	
						print $4, "\n";	
						print $5, "\n";	
						print $6, "\n";	
						print $7, "\n";	
					}
=cut						
					my $gpsout = sprintf ("%s,%s,%s,%s,%s,%s\n", int($1), $3 , $4 ,  $5, $6 , $7 );
			
					`touch $ENV{HOME}/.pskmail/.gpslock`;
					open (SETGPS, ">", "$ENV{HOME}/.pskmail/.gpsval");
					print SETGPS $gpsout;
					close (SETGPS);
					`rm $ENV{HOME}/.pskmail/.gpslock`;			
				}	# end answer
			} # end while
		 	
		
	}
} else {
		$GPSLatentry->set_text($Lat_entry->get_text);
		$GPSLonentry->set_text($Lon_entry->get_text);		
}


unless ($childpid = fork) { # you can't have enough children...
	die "cannot fork: $!" unless defined $childpid;

				sleep 2;
	
	while (1) {				# spawn a fresh rflinkclient as soon as it dies...

			my $nr_programs = `ps x | grep -c ./rflinkclient `;	
			if ($nr_programs < 3 ) {
				`killall rflinkclient.pl`;
				`/usr/local/share/pskmail/rflinkclient.pl &> /dev/null & `; 	
			}
			
			sleep 2;
	}
}


Glib::IO->add_watch(
		fileno(\*READHANDLE), 'in', \&watch_callback
	);

## timer ##########################################
Glib::Timeout->add(500,  
############## timer ##############################
        sub { 
			my $rcvdstatus = get_status();
			my $idleflag = "";
			 
			if (-e "squelch.lk") {

 #               if (time() - $squelchtime > 60) {
  #                      $squelchtime = time();
   #                     unlink "$ENV{HOME}/.pskmail/squelch.lk";
#                        $idleflag = "-";
 #               } else {

                        $idleflag = "<";
  #              }

   #             $squelchtime = time();

			} else {
				$idleflag = "-";				
			}
			if (-e ".tx.lck") {
				$idleflag = ">";
			}
			
			if ($rcvdstatus){
				if ($showstatus) {
					chomp $rcvdstatus;
					$statusline2->pop($context_id2);
					$statusline2->push($context_id2, "$idleflag $rcvdstatus");
				} else {
					$statusline2->pop($context_id2);
					$statusline2->push($context_id2, "$idleflag");				
				}			

			} else {
				$statusline2->pop($context_id2);
				$statusline2->push($context_id2, "$idleflag");				
			}			
			
			$timewindow->set_text(timestring());
			$minutewindow->set_text(minutestring());

				my $logoutstring = `tail -c +$oldlength $ENV{HOME}/.fldigi/fldigi.log`;
				my $newtext = length ($logoutstring);
				if ($newtext) {
					$oldlength += $newtext;
				## add to monitor buffer
					$end_iter = $monitorbuffer->get_end_iter;
					$monitorbuffer->insert($end_iter, $logoutstring);
					$logoutstring = "";					
				}
				
					
			if ($gpsd_running ) {
					if ($gpssecs > 9 && -e "$ENV{HOME}/.pskmail/.gpsval" ) {
						$gpssecs = 0;
						
						my $gpsvals = `cat $ENV{HOME}/.pskmail/.gpsval`;
						my @gpsvalues = split "," , $gpsvals;
						
						$GPSLatentry->set_text($gpsvalues[1]);
						$GPSLonentry->set_text($gpsvalues[2]);
						if (abs($gpsvalues[5]) >= 0.01) {
							my $cogprint = sprintf ("%3d",$gpsvalues[4]);
							$GPSCoglabel->set_text($cogprint);
				
							
							if ($gpsvalues[4] < 180) {
								$compassvalue = 4 + int(int($gpsvalues[4]) * 109 / 180);
								my $compass1 = substr($Compass, $compassvalue, 23) . "\n";
								my $compass2 = substr($Compass1, $compassvalue, 23) . "\n";
								my $compass3 = "|||||||||||||||||||||||";
								$Compassbuffer->set_text($compass1 . $compass2 . $compass3);
							} else {
								$compassvalue = 4 + int(int($gpsvalues[4] - 180) * 109 / 180);
								my $compass1 = substr($Compass3, $compassvalue, 23) . "\n";
								my $compass2 = substr($Compass4, $compassvalue, 23) . "\n";
								my $compass3 = "|||||||||||||||||||||||";
								$Compassbuffer->set_text($compass1 . $compass2 . $compass3);			
							}
							
						} else {
							$GPSCoglabel->set_text("");	
							$compassvalue = 4 ;
							my $compass1 = substr($Compass, $compassvalue, 23) . "\n";
							my $compass2 = substr($Compass1, $compassvalue, 23) . "\n";
							my $compass3 = "|||||||||||||||||||||||";
							$Compassbuffer->set_text($compass1 . $compass2 . $compass3);
							
						}
						if (abs($gpsvalues[5]) >= 0.01) {
							my $spdprint = sprintf("%3.2f",$gpsvalues[5]*3600/1852);							
							$GPSSpdlabel->set_text($spdprint);
							if ($gpsvalues[5]  > 0.1) {
								$Speedprogress2->set_fraction(($gpsvalues[5]*3600/1852)/50);
							} else {
								$Speedprogress2->set_fraction(1);						
							}

						} else {
							$GPSSpdlabel->set_text("0.0");
						}
								
					} else {
						$gpssecs++;
					}
			}	else {
				$GPSCoglabel->set_text("");	
				$Compassbuffer->set_text("");	
			}
			if ($Emergencystatus) {	## resend emergency message?
				$emergencycounter++;
				if ($emergencycounter > 60) {
					$emergencycounter = 0;
					if (-e "$ENV{HOME}/.pskmail/.lastqsl") {
						$Emergencystatus = 0;
						$Emergencydialog->hide();
					} else {
						$SOSmessage = $Emergencyentry->get_text();
						sendposit();		
					}
				}
			}
            
            if (-e "$ENV{HOME}/.pskmail/lastserver") {
                 my $lastserver = `cat $ENV{HOME}/.pskmail/lastserver`;
                 `rm $ENV{HOME}/.pskmail/lastserver`;
                 $lastserver =~ /(\S+)/;
               $Server_entry->set_text($1); # Options
                $server_entry->set_text($1); # Main GUI window
                
                
                my $found = 0;
                foreach my $actualserver (@servers) {
              	
                	if ($actualserver eq $1) {
                			$found = 1;
					}
				} 
				if ($found == 0) {
					$server_entry_spin->append_text ($1);
					push @servers, $1;
					`echo "$1" >> $ENV{HOME}/.pskmail/.servers`;
				}
                
                $lastserver = "";
                open (OPTIONS, ">$ENV{HOME}/.pskmail/.PSKoptions");
                print OPTIONS $Call_entry->get_text(), "\n";
                print OPTIONS $Server_entry->get_text(), "\n";
                print OPTIONS $Blocklen_entry->get_text(), "\n";
                print OPTIONS $Lat_entry->get_text(), "\n";
                print OPTIONS $Lon_entry->get_text(), "\n";
                print OPTIONS $QRG_entry->get_text(), "\n";
                $offset = $QRG_entry->get_text();
                close (OPTIONS);
            }

1;      
}); 
# end timer ######################################################################

open (STATUS,  ">.pskmailstatus");
print STATUS "";
close (STATUS);

unlink "$ENV{HOME}/.pskmail/TxInputfile";



#set the mode

if (-e ".pskmailmode") {
	$mode = `cat $ENV{HOME}/.pskmail/.pskmailmode`;
	if ($mode =~ /TTY/) {
		$mode = "TTY";
		$ttymode_menu_item->set_active(1);
	}
}

`cp $ENV{HOME}/.pskmail/.dopositcopy $ENV{HOME}/.pskmail/.doposit`;

	# write some fields in the GUI
		$GPSCommententry->set_text($conf[10]);
#		$GPSIconentry->set_active(1);	
		$APRSIcon->set_text($conf[9]);
		
# start the receive loop

sendmodemcommand ("server");
sleep 3;
send_linkreq();

$server_entry->set_text($Server_entry->get_text); # Main GUI Server indicator

system ("killall rflinkclient.pl"); ## just in case
sleep 1;
system ("/usr/local/share/pskmail/rflinkclient.pl &> /dev/null & ");							
		

# enter main loop
Gtk2->main();
wait;

`killall rflinkclient.pl`;
if (-e "$ENV{HOME}/.pskmail/id_defined") { 
	unlink "$ENV{HOME}/.pskmail/id_defined";
}
exit(1);

################################
sub getstuff {
################################

my $inputstring;
open (INPUTFILE, "$ENV{HOME}/.pskmail/clientout") or die "cannot open file $!\n";
WRITEHANDLE->autoflush(1);


	while (1) {
			$inputstring = <INPUTFILE>;
			if ($inputstring) {
				print WRITEHANDLE $inputstring unless $inputstring eq "";
			}
			select undef, undef, undef, 0.001;
	}
}

################################
sub watch_callback {
################################

my $line = ""; 

$line = sysreadline(READHANDLE, 0.01);

	my $linecontent = $line;
#print "Linecontent: " , $linecontent;
	chomp $linecontent;
	if ($linecontent =~ m/===TIME!/) {
		$line =~ s/===TIME!\n*//;
	}
	if ($linecontent =~ m/^==Connect from (\S*)/) {
		$statusline1->pop($context_id);	
		$statusline1->push($context_id, "Connect from $1");
		$Statuslabel->set_markup("<span foreground=\"darkgreen\" ><b>Connected</b></span>");
				$showstatus = 1;
		$statusline2->push($context_id2, " " );
		$server_entry_spin->prepend_text($1);			
		$server_entry_spin->set_active(0);
		$mode = "TTY";
		`echo "TTY" > $ENV{HOME}/.pskmail/.pskmailmode`;
		$connected = 1;
		$activemessage = "";
		`echo "Connect from $1\n" >> $ENV{HOME}/.pskmail/clientout`;
		$connect_button->set_label("Quit...");
		$Fileconnectlabel->set_markup("<span foreground=\"darkgreen\"><b> Connect </b></span>");
		if (-e ".$ENV{HOME}/.pskmail/doposit") {
			`cp $ENV{HOME}/.pskmail/.doposit $ENV{HOME}/.pskmail/.dopositcopy`;
			`unlink $ENV{HOME}/.pskmail/.doposit`;
			}
		$mode_label->set_text("CHAT");
	} elsif ($linecontent =~ m/^==Connected/) {
			$statusline1->pop($context_id);
			$statusline1->push($context_id, "");
				$Statuslabel->set_markup("<span foreground=\"darkgreen\" ><b>Connected</b></span>");
				$showstatus = 1;
		$statusline2->push($context_id2, " " );	
	} elsif ($linecontent =~ m/^==Disconnected/) {
			$statusline1->pop($context_id);
			$statusline1->push($context_id, "");
				$Statuslabel->set_markup("<span foreground=\"darkgrey\" ><b>Listening</b></span>");
				$showstatus = 0;
		$statusline2->pop($context_id2);
		$statusline2->push($context_id2, "" );
		if ($mode eq "TTY") {
#			$server_entry_spin->remove_text(0);			
		}
	#kill the session##############################
		open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
		print SESSIONDATA "none";
		close SESSIONDATA;
		`killall rflinkclient.pl`;
		$connected = 0;
		$connect_button->set_label("Connect");
		$Fileconnectlabel->set_markup("<span foreground=\"darkgreen\"><b> Connect </b></span>");
	###############################################
	} elsif ($linecontent =~ m/^ZCZC/) {
		$bulletin_active = 1;
	} elsif ($linecontent =~ m/^NNNN/) {
		$bulletin_active = 0;
	} elsif ($linecontent =~ m/^Your mail:/) {
		$headers_active = 1;
		$statusline1->pop($context_id);	
		$statusline1->push($context_id, "Receiving headers");
		$statusline2->pop($context_id2);
		$statusline2->push($context_id2, "" );
	} elsif ($linecontent =~ m/^~PAQ864RD (\d+)/) {
#print "Line: " , $linecontent, "\n";
		$bintable = $1;
		$binlength = $1;
#print "Length= ", $binlength, "\n";
		$Pfile_length = $1;
		if (-e "$ENV{HOME}/.pskmail/mailfile") {unlink "$ENV{HOME}/.pskmail/mailfile";}
		$bin_active = 1;
#print "bin_active\n"; ##debug
		$statusline1->pop($context_id);	
		$statusline1->push($context_id, "Receiving text $Pfile_length");
		$Frate = 0.0;
				if ($Frate > 1) {$Frate = 1;}
				if ($binlength < 0) {$binlength = 0}; 
				$trate = sprintf ("%d", $Pfile_length - $binlength);
				$Progress->set_fraction($Frate);
				$Progress->set_text($trate);

	} elsif ($linecontent =~ m/^Your file:(.*\.*.*\.*.*) (\d*)$/) {
		$rcvfilename = $1;
		$binlength = $2;
		$Pfile_length = $2;
		$file_active = 1;
		if (-e "$ENV{HOME}/.pskmail/.file") { unlink "$ENV{HOME}/.pskmail/.file"; }
		$statusline1->pop($context_id);	
		$statusline1->push($context_id, "Receiving file $rcvfilename ($Pfile_length)");
		$Frate = 0.0;
				if ($Frate > 1) {$Frate = 1;}
				if ($binlength < 0) {$binlength = 0}; 
				$trate = sprintf ("%d", $Pfile_length - $binlength);
				$Progress->set_fraction($Frate);
				$Progress->set_text($trate);
		$statusline2->pop($context_id2);
		$statusline2->push($context_id2, "" );
	} elsif ($linecontent =~ m/^Your msg: (\d*)$/) {
		if (-e "$ENV{HOME}/.pskmail/.msgout") {unlink "$ENV{HOME}/.pskmail/.msgout"}
		$binlength = $1;
		$Pfile_length = $1;
		$msg_active = 1;
		$statusline1->pop($context_id);	
		$statusline1->push($context_id, "Receiving msg ($Pfile_length)");
		$Frate = 0.0;
				if ($Frate > 1) {$Frate = 1;}
				if ($binlength < 0) {$binlength = 0}; 
				$trate = sprintf ("%d", $Pfile_length - $binlength);
				$Progress->set_fraction($Frate);
				$Progress->set_text($trate);
		$statusline2->pop($context_id2);
		$statusline2->push($context_id2, "" );
	} elsif ($linecontent =~ m/^~SEND/) {
		if (-e "$ENV{HOME}/.pskmail/.msgout") {unlink "$ENV{HOME}/.pskmail/.msgout"}
		$binlength = 0;
		$Pfile_length = 0;
		$msg_active = 1;
		$statusline1->pop($context_id);	
		$statusline1->push($context_id, "Receiving msg ($Pfile_length)");
		$Frate = 0.0;
				if ($Frate > 1) {$Frate = 1;}
				if ($binlength < 0) {$binlength = 0}; 
				$trate = sprintf ("%d", $Pfile_length - $binlength);
				$Progress->set_fraction($Frate);
				$Progress->set_text($trate);
		$statusline2->pop($context_id2);
		$statusline2->push($context_id2, "" );
	} elsif ($linecontent =~ m/^Your wwwpage: (\d*)$/) {
		$binlength = $1;
		$Pfile_length = $1;
		$www_active = 1;
		$statusline1->pop($context_id);	
		$statusline1->push($context_id, "Receiving webpage ($Pfile_length)");
		$Frate = 0.0;
				if ($Frate > 1) {$Frate = 1;}
				if ($binlength < 0) {$binlength = 0}; 
				$trate = sprintf ("%d", $Pfile_length - $binlength);
				$Progress->set_fraction($Frate);
				$Progress->set_text($trate);
		$statusline2->pop($context_id2);
		$statusline2->push($context_id2, "" );
	} elsif ($linecontent =~ m/^~CLOSE/) {	
		`echo "~DISC" >> $Outputfile`;
		sleep 15;
			$statusline1->pop($context_id);
			$statusline1->push($context_id, "");
				$Statuslabel->set_markup("<span foreground=\"darkgrey\" ><b>Listening</b></span>");
				$showstatus = 0;
		$statusline2->pop($context_id2);
		$statusline2->push($context_id2, "" );
		if ($mode eq "TTY") {
			$server_entry_spin->remove_text(0);			
		}
		#kill the session##############################
		open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
		print SESSIONDATA "none";
		close SESSIONDATA;
		$connected = 0;
		$connect_button->set_label("Connect");
		$Fileconnectlabel->set_markup("<span foreground=\"darkgreen\"><b> Connect </b></span>");
		###############################################

		`killall rflinkclient.pl`;
	} elsif ($linecontent =~ m/^~DISC/) {	
			$statusline1->pop($context_id);
			$statusline1->push($context_id, "");
			$Statuslabel->set_markup("<span foreground=\"darkgrey\" ><b>Listening</b></span>");
				$showstatus = 0;
		$statusline2->pop($context_id2);
		$statusline2->push($context_id2, "" );
		if ($mode eq "TTY") {
			$server_entry_spin->remove_text(0);			
		}
		#kill the session##############################
		open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
		print SESSIONDATA "none";
		close SESSIONDATA;
		$connected = 0;
		$connect_button->set_label("Connect");
		$Fileconnectlabel->set_markup("<span foreground=\"darkgreen\"><b> Connect </b></span>");
		###############################################

		`killall rflinkclient.pl`;
		
	} elsif ($linecontent =~ m/^-end-/) {		#end received
			resetprogress ();		
		if ($headers_active) {
			$headers_active = 0;
			purge_headers();
			$statusline1->pop($context_id);	
			$statusline1->push($context_id, "done...");
		}
		if ($msg_active) {
			$msg_active = 0;
				my $Datefrag = "";
				my $Fromfrag = "";
			if (-e "$ENV{HOME}/.pskmail/.msgout") {
				my $txt = `cat $ENV{HOME}/.pskmail/.msgout`;
				extract_attachment ($txt);
				convert_to_mbox ($txt);
				`cp $ENV{HOME}/.pskmail/.msgout $ENV{HOME}/.pskmail/copymsg`;
				unlink "$ENV{HOME}/.pskmail/.msgout";
			}
			$statusline1->pop($context_id);	
			$statusline1->push($context_id, "done, added to Inbox...");
			if ($mode eq "TTY") {
				`echo "Message sent..." >> $Outputfile`;
			}
		}
		if ($www_active) {
			$www_active = 0;
			$statusline1->pop($context_id);	
			$statusline1->push($context_id, "done...");
		}
		if ($file_active) {
			$file_active = 0;
			my $file = `cat $ENV{HOME}/.pskmail/.file`;
			if ($rcvfilename =~ /\.bz2$/) {
				$file = decode_base64 ($file);
				open (RCVFILE, ">", "$ENV{HOME}/.pskmail/downloads/" . $rcvfilename);
				print RCVFILE $file;
				close RCVFILE;
				`bunzip2 -f $ENV{HOME}/.pskmail/downloads/$rcvfilename`;
				substr ($rcvfilename, -4) = "";
			} elsif ($rcvfilename =~ /\.tgz$/) {
				$file = decode_base64 ($file);
				open (RCVFILE, ">", "$ENV{HOME}/.pskmail/downloads/" . $rcvfilename);
				print RCVFILE $file;
				close RCVFILE;
				`gunzip -f $ENV{HOME}/.pskmail/downloads/$rcvfilename`;
				substr ($rcvfilename, -4) = "";
			} elsif ($rcvfilename =~ /\.gz$/) {
				$file = decode_base64 ($file);
				open (RCVFILE, ">", "$ENV{HOME}/.pskmail/downloads/" . $rcvfilename);
				print RCVFILE $file;
				close RCVFILE;
				`gunzip -f $ENV{HOME}/.pskmail/downloads/$rcvfilename`;
				substr ($rcvfilename, -3) = "";
			} elsif ($rcvfilename =~ /\.864$/) {
				open (RCVFILE, ">", "$ENV{HOME}/.pskmail/downloads/" . $rcvfilename);
				print RCVFILE $file;
				close RCVFILE;
				`/usr/local/share/pskmail/unpaq864 $ENV{HOME}/.pskmail/downloads/$rcvfilename`;
				substr ($rcvfilename, -4) = "";
			}
#			unlink "$ENV{HOME}/.pskmail/.file";
			if (-s "$ENV{HOME}/.pskmail/downloads/$rcvfilename") {
				$statusline1->pop($context_id);	
				$statusline1->push($context_id, "File $rcvfilename o.k.");
				`echo "~File stored o.k." >> $Outputfile`;
			} else {
				$statusline1->pop($context_id);	
				$statusline1->push($context_id, "File $rcvfilename not o.k.");
				`echo "File not o.k." >> $Outputfile`;
			}
		}
		if ($bin_active) {
			$bin_active = 0;

			`/usr/local/share/pskmail/unpaq864 $ENV{HOME}/.pskmail/mailfile.864`;
			my $txt = `cat $ENV{HOME}/.pskmail/mailfile`;
			unlink "$ENV{HOME}/.pskmail/mailfile";
			extract_attachment ($txt);		
			convert_to_mbox ($txt);
			$txt = "\n" . $txt;
			
			$buffer->insert($buffer->get_end_iter, $txt);
			$statusline1->pop($context_id);	
			$statusline1->push($context_id, "done, added to Inbox...");
		}
	} elsif ($linecontent =~ m/Message sent/) {
		
		if ($activemessage) {
			
			chomp $activemessage;
			
			my $wholemessage = "./Outbox/" . $activemessage;
			my $senddate = `date`;
			`echo "$senddate >>$ENV{HOME}/.pskmail/ Sent`;
			`echo "$wholemessage" >> $ENV{HOME}/.pskmail/Sent`;
			
			if (-e $wholemessage) {
				`echo "it's still there" >> $ENV{HOME}/.pskmail/messagelog`;
				open SESSION, "$ENV{HOME}/.pskmail/PSKmailsession";
				my $session = <SESSION>;
				chomp $session;
				close SESSION;
				if ($session =~ /Connected/ || $session =~ /TTYConnected/) {
					$statusline1->pop($context_id);	
					$statusline1->push($context_id, "Message sent");
					`unlink $ENV{HOME}/.pskmail/$wholemessage`;
					`echo "you killed it" >> $ENV{HOME}/.pskmail/messagelog`;
					$statusline1->pop($context_id);	
					$statusline1->push($context_id, "Message $activemessage delivered");
					$activemessage = "";
				}
			}
		}
	} elsif ($linecontent =~ m/~File stored/) {
					
					$statusline1->pop($context_id);	
					$statusline1->push($context_id, "File stored o.k.");
	} elsif ($linecontent =~ m/%(\d+:\d+\-.*)/) {  
					$msgtextbuffer->insert($msgtextbuffer->get_end_iter, $1 .  "\n");
					$statusline1->pop($context_id);	
					$statusline1->push($context_id, $linecontent);
	} else {
		if ($line ne "") {
			$buffer->insert($buffer->get_end_iter, $line);
			
			if ($Notebook->get_current_page == 2) {
				$fileviewbuffer->insert($fileviewbuffer->get_end_iter, $line);
			}
			
			if ($bin_active || $file_active || $msg_active || $www_active) {
				$binlength -= length($line);
				$statusline1->pop($context_id);	
				$statusline1->push($context_id, "Receiving msg ($Pfile_length)");
				if ($binlength || $Pfile_length) {
					$Frate = ($Pfile_length -$binlength)/$Pfile_length;
				} else {
					$Frate = 0.001;
				}
				if ($Frate > 1) {$Frate = 1;}
				if ($Frate < 0) {$Frate = 0;}
				if ($binlength < 0) {$binlength = 0}; 
				$trate = sprintf ("%d", $Pfile_length - $binlength);
				$Progress->set_fraction($Frate);
				$Progress->set_text($trate);
			}
			if ($headers_active) {
				if ($line =~ /^(\s*\d+\s.*\d+$)/) {
					my $res = $1;
					$headerbuffer->insert($headerbuffer->get_end_iter, $res . "\n");
					$hd_end_mark = $headerbuffer->create_mark ('end', $headerbuffer->get_end_iter, FALSE);
					$headerview->scroll_mark_onscreen ($hd_end_mark);

					chomp $res;				
					`echo $res >> $ENV{HOME}/.pskmail/headerlist`;
				} else {
					if (length ($line) > 1) { 
						$headers_active = 0; 
						}
				}
			} elsif ($file_active) {
				chomp $line;
				`echo $line >> $ENV{HOME}/.pskmail/.file`;
			} elsif ($bin_active) {
				chomp $line;
				`echo $line >> $ENV{HOME}/.pskmail/mailfile.864`;
			} elsif ($msg_active) {
				chomp $line;
				`echo $line >> $ENV{HOME}/.pskmail/.msgout`;
			}
		}
	}

1;

}

#############################################################
sub sysreadline (*;$) {		# read complete line non-blocking
#############################################################

	my ($handle, $timeout) = @_;
	
#	$handle = qualify_to_ref($handle, caller());
	$handle = \*{"main::" . $handle};
	my $infinitely_patient = (@_ == 1 || $timeout < 0);
	my $start_time = time();
	my $selector = IO::Select->new();
	$selector->add($handle);
	my $line = "";
SLEEP:
	until (at_eol($line)) {
		unless ($infinitely_patient) {
			return $line if time() > ($start_time + $timeout);	
		}
		# sleep only 1 sec before checking again
		next SLEEP unless $selector->can_read(1.0);
INPUT_READY:
		while ($selector->can_read(0.0)) {
			my $was_blocking = $handle->blocking(0);
CHAR:		while ( sysread($handle, my $nextbyte, 1)) {
				$line .= $nextbyte;
				last CHAR if $nextbyte eq "\n";
			}
			$handle->blocking($was_blocking);
			# if incomplete line keep trying
			next SLEEP unless at_eol($line);
			last INPUT_READY;		
		} 
	}
	return $line;
}
sub at_eol($) { $_[0] =~ /\n\z/ }
################################# end sysreadline ####################


#############################################
sub get_status {
#############################################

	my $command = shift @_;
	open (STATUS, ".pskmailstatus");
#	flock (STATUS, LOCK_SH);
	my $status = <STATUS>;
	close (STATUS);
	return $status;


} 
#############################################
sub get_MSGNR {
#############################################

$MSGNR++;
if ($MSGNR > 99) {
	$MSGNR = 0;
}
$Msgout = sprintf ("%d", $MSGNR);
$Msgout = "0" . $Msgout;
$Msgout = "{" . substr ($Msgout, -2);

return ($Msgout);

}
#############################################
sub get_last_MSGNR {
#############################################


$Msgout = sprintf ("%d", $MSGNR);
$Msgout = "0" . $Msgout;
$Msgout = "{" . substr ($Msgout, -2);

return ($Msgout);

}# end 
#############################################

#############################################
sub send_to_mbox {
#############################################

my $msgflag = 0;
my $fromflag = 0;
my $dateflag = 0;
my $subjectflag = 0;
my $datestore;
my $subjectstore;
my $fromstore;
my $date;
my $from;
my $newlines = 0;
my @nomail;

`cp $ENV{HOME}/.pskmail/clientout $ENV{HOME}/.pskmail/.cpclientout `;

open ($fh, "$ENV{HOME}/.pskmail/clientout") or die "Could not open input file";
my @arqtext = <$fh>;
close ($fh);

open ($fh2 , ">>$ENV{HOME}/.pskmail/incoming") or die "Could not open output file";

foreach $line (@arqtext) {

		if ($line =~ m/Date:\s/) {
			$datestore = $line;
			$dateflag = 1;		
		} elsif ($line =~ m/Subject:\s/) {
			$subjectstore = $line;
			$subjectflag = 1;
		} elsif ($line =~ m/From:\s/) {
			$fromstore = $line;
			$fromflag = 1;
		} else {
			if ($dateflag && $subjectflag && $fromflag) {
				
				$msgflag = 1 unless $msgseen {$datestore . $subjectstore . $fromstore}++;
				
				if ($fromstore =~ m/From:\s.*<(.*)>|From:\s(.*)/  && $msgflag) {
					$from = $1 || $2;
					$msgseen {$datestore . $subjectstore . $fromstore}++;
					my $partdate = substr ($datestore, 6, 22);
					eval {
						$partdate =~ m/(\w\w\w),*\s(\d+)\s(\w\w\w)\s\d\d\d\d\s(\d\d:\d\d)/;
						$partdate = $1 . " " . $3 . " " . $2  . " " . $4;
					};
					if (@!) {
						print "ERROR? = @! - $partdate\n";
					} else {
						print $fh2 "From " . $from . " " . $partdate . "\n";
						print $fh2 $fromstore;
						print $fh2 $datestore;
						print $fh2 $subjectstore;
					}
				}
				$fromstore = "";
				$datestore = "";
				$subjectstore = "";
				$fromflag = 0;
				$dateflag = 0;
				$subjectflag = 0;
			
			}
		
			if ($msgflag) {
				if ($line =~ m/-end-/) {
					$msgflag = 0;
				} else {
					if ($line =~ m/^\s*\n/) {
						$newlines++;
						if ($newlines < 2) {
							print $fh2 $line;
						}
					} else {
						$newlines = 0;
						print $fh2 $line;
					}
				}
			} else {
				push @nomail, $line;
			}
		}
}
close ($fh2);
open $fh2, ">$ENV{HOME}/.pskmail/.cpclientout" or die "Could not open output file";
print $fh2 @nomail;
close ($fh2);


}
#############################################
sub extract_attachment {
#############################################
my $txt = shift @_;
my @arqtext = split "\n", $txt;
my $is_attachment = 0;
my $filename = "";
my $base64 = 0;
my $file_active = 0;

	foreach my $line (@arqtext) {	# add newline
		$line .= "\n";
	}
	foreach $line (@arqtext) {
		
		if ($line =~ /Content-disposition: attachment; filename=(.*)/) {
				$is_attachment = 1;
				$filename = $1;
				$filename =~ tr/\"//;
				if (-e "$ENV{HOME}/.pskmail/$filename") { unlink "$ENV{HOME}/.pskmail/$filename";}			
		} elsif ($line =~ /Content-Disposition: attachment;/) {
				$is_attachment = 1;
		} elsif ($is_attachment && $line =~ /filename=(.*)/) {
				$filename = $1;
				$filename =~ tr/\"//;
				if (-e "$ENV{HOME}/.pskmail/$filename") { unlink "$ENV{HOME}/.pskmail/$filename";}
		} elsif ($is_attachment && $line =~ /base64/) {
				$base64 = 1;		
		} elsif ($file_active == 0 && $base64 && $is_attachment && length ($line) > 1) {
				$file_active = 1;

				chomp $line;
				`echo $line >> $ENV{HOME}/.pskmail/$filename`;	

		} elsif ($file_active && length ($line) > 1) {
				chomp $line;
				`echo $line >> $ENV{HOME}/.pskmail/$filename`;	

		} elsif ($file_active && length ($line) == 1){
				last;
		}
	}
	
	if ($filename) {	
		$txt = `cat $ENV{HOME}/.pskmail/$filename`;

		if ($txt) {
			$txt2 = decode_base64 ($txt);
			open (RCVFILE, ">", "$ENV{HOME}/.pskmail/downloads/" . $filename);
			print RCVFILE $txt2;
			close RCVFILE;
			unlink "$ENV{HOME}/.pskmail/$filename";
		}
	}

} # end

#############################################
sub convert_to_mbox {
#############################################

my $txt = shift @_;

my $msgflag = 0;
my $fromflag = 0;
my $dateflag = 0;
my $subjectflag = 0;
my $datestore;
my $subjectstore;
my $fromstore;
my $Tostore;
my $date;
my $from;
my $newlines = 0;
my @nomail;

my @arqtext = split "\n", $txt;

foreach my $line (@arqtext) {	# add newline
	$line .= "\n";
}



open ($fh2 , ">>$ENV{HOME}/.pskmail/Inbox") or die "Could not open output file";

foreach $line (@arqtext) {

		if ($line =~ m/Date:\s/) {
			$datestore = $line;
			$dateflag = 1;
					
		} elsif ($line =~ m/Subject:\s/) {
			$subjectstore = $line . "\n";
			$subjectflag = 1;
		} elsif ($line =~ m/To:\s/) {
			$Tostore = $line;
		} elsif ($line =~ m/From:\s/) {
			$fromstore = $line;
			$fromflag = 1;
		} else {
			if ($dateflag && $subjectflag && $fromflag) {
				
				$msgflag = 1 unless $msgseen {$datestore . $subjectstore . $fromstore}++;
				
				if ($fromstore =~ m/From:\s.*<(.*)>|From:\s(.*)/  && $msgflag) {
					$from = $1 || $2;
					$msgseen {$datestore . $subjectstore . $fromstore}++;
					my $partdate = substr ($datestore, 6, 22);
					eval {
						$partdate =~ m/(\w\w\w),*\s(\d+)\s(\w\w\w)\s(\d\d\d\d)\s(\d\d:\d\d)/;
#`echo "$partdate" >> testinput.txt`;
						$partdate = $1 . " " . $3 . " " . $2  . " " . $5 . " " . $4;
#`echo "$partdate" >> testinput.txt`;
					};
					if (@!) {
						print "ERROR? = @! - $partdate\n";
					} else {
						print $fh2 "\nFrom " . $from . " " . $partdate . "\n";
						print $fh2 $fromstore;
						if ($Tostore) {print $fh2 $Tostore;}
						print $fh2 $datestore;
						print $fh2 $subjectstore;
					}
				}
				$fromstore = "";
				$datestore = "";
				$subjectstore = "";
				$fromflag = 0;
				$dateflag = 0;
				$subjectflag = 0;
			
			}
		
			if ($msgflag) {
				if ($line =~ m/-end-/) {
					$msgflag = 0;
				} else {
					if ($line =~ m/^\s*\n/) {
						$newlines++;
						if ($newlines < 2) {
							print $fh2 $line;
						}
					} else {
						$newlines = 0;
						print $fh2 $line;
					}
				}
			} else {
				push @nomail, $line;
			}
		}
}
close ($fh2);

}
##################################################
sub purge_headers {
##################################################
my $lastnr = 0;
my $line = "";

if (-e "$ENV{HOME}/.pskmail/headerlist") {

	open (HDR, "$ENV{HOME}/.pskmail/headerlist");

	while (<HDR>) {
		$line = $_;
		
		if ($line =~ /^\s*(\d*)\s.*\d*/ && length ($line) > 10) {
			$headers[$1] = $line;
			$lastnr = $1;
		}
	}
	close (HDR);
}
	open (HDR, ">$ENV{HOME}/.pskmail/headerlist");
	$LastHeader = $lastnr;
	for ($i = 1; $i <= $lastnr; $i++) {
			if ($headers[$i]) {print HDR $headers[$i]; }
	}
	close (HDR);


}
##################################################
sub resetprogress {
##################################################
			$Frate = 0;
			$trate = "";
			$Progress->set_fraction($Frate);
			$Progress->set_text($trate);
}

##################################################
sub getconfig {
##################################################
	if (-e "$ENV{HOME}/.pskmail/.pskmailconf") {
		open (CONFIG, "$ENV{HOME}/.pskmail/.pskmailconf");
		my $configdata = <CONFIG>;
		close (CONFIG);
		@conf = split (",", $configdata);
		$conf[3] =~ tr/~/$ENV{HOME}/;
		$conf[4] =~ tr/~/$ENV{HOME}/;
	#	$conf[4] = ">" . $conf[4];
	} else {
		$conf[0] = 0;
		$conf[1] = 0;
		$conf[2] = 0;
		$conf[3] = "";	# Inputfile
		$conf[4] = ""; # commandfile
		$conf[5] = "$ENV{HOME}/.pskmail/server.log";
		$conf[6] = 16;
		$conf[7] = 15;
		$conf[8] = 0;
		$conf[9] = "U";
		$conf[10] = "pskmail $Version";
		$conf[11] = 0;
		$conf[12] = 20;
		$systemconf->show();
	}

}
#################################################
sub maildate {
#################################################
my $date = `date`;
#Wed Jun  6 14:06:20 CEST 2007 to
#Date: Fri, 11 May 2007 11:53:55
	if ($date =~ /(\w\w\w)\s*(\w\w\w)\s*(\d+)\s*(\d\d:\d\d:\d\d)\s\w*\s(\d\d\d\d)/) {
		my $day = $1;
		my $mon = $2;
		my $dat = $3;
		$dat = "0" . $dat;
		$dat = substr ($dat, -2);
		my $tim = $4;
		my $yr = $5;
		return "$day $dat $mon $yr $tim";
	} else {
		return "";
	}
}
##################################
sub timestring {
##################################
	my $all = time();
	my $sec = $all % 60;
	my $min = $all/60 % 60;
	my $hr = $all/3600 % 24;
	my $mytime = sprintf("%02d:%02d:%02d",$hr,$min,$sec);

	return $mytime;
}
##################################
sub minutestring {
##################################
	my $all = time();
	my $min = $all/60 % 5;
	my $myminute = sprintf("%d",$min);

	return $myminute;
}

###################################
	sub connect_pskmail {
####################################
			resetprogress ();

			if ($connected == 0) {
				$connected = 1;
				$activemessage = "";
				$statusline1->pop($context_id);
				$statusline1->push($context_id, "Sending connect...");
#				$Statuslabel->set_text("Connecting");
				$Statuslabel->set_markup("<span foreground=\"red\" ><b>Connecting</b></span>");

				my $connect_time = `date`;
				`echo "Connect: $connect_time\n" >> $ENV{HOME}/.pskmail/clientout`;
				
				open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
				if ($mode eq "TTY") {
					print SESSIONDATA "TTYConnected";
				} else {
					print SESSIONDATA "Connected";
				}
				close SESSIONDATA;
				
				
				
				$connect_button->set_label("Quit...");
				$Fileconnectlabel->set_markup("<span foreground=\"darkred\"><b> Quit </b></span>");
				
				`echo "" > $Inputfile`;
				system ("killall rflinkclient.pl");
				sleep 1;
				
			} else {
				$connected = 0;
				if ($mode eq "TTY") {
					`echo "~CLOSE" >> $Outputfile`; 
					$Statuslabel->set_text("Closing");
				} else {
					`echo "~QUIT" >> $Outputfile`; 
					$statusline1->push($context_id, "Sending QUIT...");
					$Statuslabel->set_text("Quitting");
				}
				$connect_button->set_label("Connect");
				$Fileconnectlabel->set_markup("<span foreground=\"darkgreen\"><b> Connect </b></span>");
				sleep(1);
				my $connect_time = `date`;
				`echo "Disconnect: $connect_time\n" >> $ENV{HOME}/.pskmail/clientout`;
				open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
				print SESSIONDATA "none";
				close SESSIONDATA;
#				`killall rflinkclient.pl`;
			$statusline1->pop($context_id);
			$statusline1->push($context_id, "");
				$Statuslabel->set_markup("<span foreground=\"red\" ><b>Disconnecting</b></span>");
				$statusline2->pop($context_id2);
				$statusline2->push($context_id2, "");			

			}
 	} #end connect

###########################################
		sub  abort {
##########################################
				open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
				print SESSIONDATA "none";
				close SESSIONDATA;
			`killall rflinkclient.pl`;
			$connected = 0;
			$statusline1->pop($context_id);
			$statusline1->push($context_id, "Connect aborted...");
			$Statuslabel->set_markup("<span foreground=\"darkgrey\" ><b>Listening</b></span>");
				$showstatus = 0;
			$connect_button->set_label("Connect");
			$Fileconnectlabel->set_markup("<span foreground=\"darkred\"><b> Connect </b></span>");

		} # end abort_button
		
##########################################
		sub pskmail_exit {
##########################################
			if (-e "$ENV{HOME}/.pskmail/headerlist") {
				send_to_mbox() ;
			}
			sendmodemcommand ("normal");
			if (-e "$ENV{HOME}/.pskmail/id_defined") { 
				unlink "$ENV{HOME}/.pskmail/id_defined";
			}
			`cp $ENV{HOME}/.pskmail/.cpclientout $ENV{HOME}/.pskmail/clientout`;
			untie %memreceive;
			kill('KILL', $pid);
			wait();
			Gtk2->main_quit;
 		} # end file_exit
#############################################
		sub sendposit {
#############################################
			my $lat;
			my $lon;
			
			if (-e "$ENV{HOME}/.pskmail/.gpsval") {
				my $gpsstuff = `cat $ENV{HOME}/.pskmail/.gpsval`;
				@gps = split "," , $gpsstuff;
				$lat = $gps[1];
				$lon = $gps[2];
				$cog = int($gps[4]);
				$spd = sprintf ("%s", int(100 * $gps[5]) / 100) ;
			} else {
				$lat = "Nofix";
			}	
			
			if ($lat eq "Nofix")	{
				$lat = $Lat_entry->get_text;
				$lon = $Lon_entry->get_text;			
			} else {
				$Lat_entry->set_text($lat);
				$Lon_entry->set_text($lon);				
				open (OPTIONS, ">$ENV{HOME}/.pskmail/.PSKoptions");
				print OPTIONS $Call_entry->get_text(), "\n";
				print OPTIONS $Server_entry->get_text(), "\n";
				print OPTIONS $Blocklen_entry->get_text(), "\n";
				print OPTIONS $Lat_entry->get_text(), "\n";
				print OPTIONS $Lon_entry->get_text(), "\n";
				print OPTIONS $QRG_entry->get_text(), "\n";
				$offset = $QRG_entry->get_text();
				close (OPTIONS);
			}			

			open SESSION, "$ENV{HOME}/.pskmail/PSKmailsession";
			my $session = <SESSION>;
			chomp $session;
			close SESSION;
			
			if ($session eq "Connected") {
				`echo "~POSITION $lat $lon" >> $Outputfile`;
				$statusline1->pop($context_id);
				$statusline1->push($context_id, "Sending position...");
				$Statuslabel->set_markup("<span foreground=\"darkgreen\" ><b>Connected</b></span>");
				$showstatus = 1;
				print "~POSITION $lat $lon\n";			
			} else {
						my $lat_sign;
						my $lon_sign;
						
                        if ($lat < 0) {
							$lat = abs $lat;
							$lat_sign = "S";
						} else {
							$lat_sign = "N";
						}
						if ($lon < 0) {
							$lon = abs $lon;
							$lon_sign = "W";
						} else {
							$lon_sign = "E";
						}
						$aprs_lat = (($lat - int($lat)) * 60) + int($lat)* 100;
						if (int($lat) != 0 && abs($lat) < 10.0) {
							$aprs_lat = "0" . $aprs_lat;
						} elsif (int($lat) == 0) {
							$aprs_lat= "00" . $aprs_lat;
						} else {
							$aprs_lat = $aprs_lat;
						}
                        $aprs_lat =~ s/\,/\./;
						
						if ($aprs_lat =~ /(\d\d\d\d)\.(\d\d)/) {
							$aprs_lat = $1 . "." . $2;
} elsif ($aprs_lat =~ /(\d\d\d\d)\.(\d)/) {
							$aprs_lat = $1 . "." . $2 . "0";
						} elsif ($aprs_lat =~ /(\d\d\d\d)/) {
							$aprs_lat = $1 . ".00";
						} else {
							$aprs_lat = $1 . "0000.00";
						}

						
						$aprs_lon = (($lon - int($lon)) * 60) + int($lon)* 100;
						if (int($lon) == 0) {
							$aprs_lon = "000" . $aprs_lon;
						} elsif (int($lon) < 10) {
							$aprs_lon = "00" . $aprs_lon;
						} elsif (int($lon) < 100) {
							$aprs_lon = "0" . $aprs_lon;
						} else {
							$aprs_lon =  $aprs_lon;
						}
                         $aprs_lon =~ s/\,/\./;

						if ($aprs_lon =~ /(\d\d\d\d\d)\.(\d\d)/){
							$aprs_lon = $1 . "." . $2;
} elsif ($aprs_lon =~ /(\d\d\d\d\d)\.(\d)/){
							$aprs_lon = $1 . "." . $2 . "0" ;
						} elsif ($aprs_lon =~ /(\d\d\d\d\d)/) {
							$aprs_lon = $1 . ".00";
						} else {
							$aprs_lon = "00000.00";
						}
						($aprs_sign,$aprs_status) = get_positmsg();
				
							my $queryposit = `cat $ENV{HOME}/.pskmail/.doposit`;
							
							if ($queryposit =~ /WX/) {
								my $wxcontent = `cat $ENV{HOME}/.pskmail/.wx`;
								$aprs_status .= $wxcontent;
							}

						
						if ($spd) {
							$posit = "!" . $aprs_lat . $lat_sign . "/" . $aprs_lon . $lon_sign . $aprs_sign . $cog . "/" . $spd . "/" . $aprs_status;
						} else {
							$posit = "!" . $aprs_lat . $lat_sign . "/" . $aprs_lon . $lon_sign . $aprs_sign . $aprs_status;
						}
						if ($Emergencystatus) {
							$posit = "!" . $aprs_lat . $lat_sign . "\\" . $aprs_lon . $lon_sign . "!" . $SOSmessage;
						}
						send_aprsmessage($posit);
							

			}
		} # end sendposit

