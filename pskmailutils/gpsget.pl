#!/usr/bin/perl

# gpsget.pl gets position and time from a gps connected to the system. The gps
# is expected to send standard nmea strings that can be parsed using the 
# GPS::NMEA module. Output is directed to a file called .gps 

# This program is published under the GPL license.
#   Copyright (C) 2007 Pär Crusefalk (per@crusefalk.se)
# 
# *    gpsget.pl is free software; you can redistribute it and/or modify
# *    it under the terms of the GNU General Public License as published by
# *    the Free Software Foundation; either version 2 of the License, or
# *    (at your option) any later version.
# *
# *    gpsget.pl is distributed in the hope that it will be useful,
# *    but WITHOUT ANY WARRANTY; without even the implied warranty of
# *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# *    GNU General Public License for more details.
# *
# *    You should have received a copy of the GNU General Public License
# *    along with this program; if not, write to the Free Software
# *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

use GPS::NMEA;
use Getopt::Std;
use Time::Local;
use POSIX;
use strict;
use warnings;

# Where is the gps connected and at what speed ?
my $gpsport = "/dev/ttyUSB0";
my $gpsspeeed = 4800;
my %rawnmea;
my $nlat;
my $nlon;
my ($hr, $min, $sec);
my ($day, $month, $year);
my $timestr;
my $count=0;

    # Get command line options
    getopt('f', \ my %opts ); #-f devicename

    # did we receive a command line option ?
    if ($opts{f} ne ''){
 	$gpsport = $opts{f};
    };
    print "Using port $opts{f}\n";
    print "Output is in the file .gps so execute this within ~/mail if used with a client.\n";
    # Lets try to open the port
    my $gps = GPS::NMEA->new(Port => $gpsport, 
                             Baud => 4800);
    # Read and print the data
    while(1) {
		# get lon/lat
        	my($ns,$lat,$ew,$lon) = $gps->get_position;
        	print "$lat $lon ";
            	my $d = $gps->{NMEADATA};
                my $tl = $$d{time_utc};
                my $t = substr ($tl, 0, 8);
                my $dd = $$d{ddmmyy};
                
                if ($lat =~ /(\d+)\.(\d\d)(\d*)/) {
                        $nlat = $1 + ($2  + $3/10000)/60;
                }
                $nlat = sprintf ("%02.6f", $nlat);
                
                if ($lon =~ /(\d+)\.(\d\d)(\d*)/) {
                        $nlon = $1 + ($2  + $3/10000)/60;
                }
                $nlon = sprintf ("%03.6f", $nlon);
                
                if ($dd =~ /(\d\d)(\d\d)(\d\d)/) {
                        ($day, $month, $year) = ($1, $2, $3);
                }
                if ($t =~ /(\d\d):(\d\d):(\d\d)/) {
                        ($hr, $min, $sec) = ($1, $2, $3);
                }
                
                $timestr = timegm($sec, $min, $hr, $day, $month-1, $year+2000-1900);
                
                open (GPSF, "+>", ".gps");
                print GPSF $count++, " ", $nlat, " ", $nlon, " ", $timestr, "\n";
                close (GPSF);    
		sleep(5);
	}
