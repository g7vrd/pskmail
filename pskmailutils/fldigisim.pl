#! /usr/bin/perl -w

use IO::Handle;
use Getopt::Std;

getopts("vhq:d:s:c:");

my $Version = "Fldigisimulator-0.1";
my $delay = 0.1;
my $quality = 100;
my $serverdir = "server";
my $clientdir = "client";

if ($opt_v) {
	print "$Version (C) 2007 PA0R\n";
	$opt_v = "";
	exit;
}
if ($opt_s) {
	$serverdir = $opt_s;
}
if ($opt_c) {
	$clientdir = $opt_c;
}
if ($opt_d) {
	$delay = $opt_d - 1;
}
if ($opt_q) {
	$quality = $opt_q;
}
if ($opt_h) {
	print "$Version (C) 2007 PA0R\n\n";
	print "	use: fldigisim.pl [-v] [-h] [-q #] [-s dir] [-c dir]\n\n";
	print "	-h	help\n";
	print "	-v	output version\n";
	print "	-d	delay (seconds)\n";
	print "	-q	channel quality (%)\n";
	print "	-s	server directory (rel. to homedir)\n";
	print "	-c	client directory (rel. to homedir)\n\n";
	print "Enjoy!!\n";
	$opt_h = "";
	exit;
}

print "Flfidi simulation - channel=$quality% - delay = $delay seconds\n";

###########################################
# file names:
my $serverin = "$ENV{HOME}/$serverdir/gMFSK.log";
my $clientin = "$ENV{HOME}/$clientdir/gMFSK.log";
my $serverout = "$ENV{HOME}/$serverdir/gmfsk_autofile";
my $clientout = "$ENV{HOME}/$clientdir/gmfsk_autofile";
###########################################
# server part writes to ~/client/gMFSK.log


if ($pid = fork) {
###########################################
# SERVER TO CLIENT PART
###########################################
# input: gmfsk_autofile from server : $serverout
# output: 	gMFSK.log for server(TX): $serverin
#			gMFSK.log for client(RX): $clientin

print "Starting server->client channel\n";

	open ($fh1, ">>", $clientin);
	$fh1->autoflush(1);
	open ($fh2, ">>", $serverin);
	$fh2->autoflush(1);
	my $input = ""; #input from server
	while (1) {
		if (-e $serverout) {
			$input = `cat $serverout`;
			`rm $serverout`;

			my $tx = maketx();
			my $rx = makerx();
			
			print $fh2 "\n" . $tx;
			for (split//, $input) {
				if ($_ =~ /\n/) {
					print $fh2 $_ . $tx;
				} elsif (ord($_) == 1) {
					print $fh2 "<SOH>";
				} elsif (ord($_) == 4) {
					print $fh2 "<EOT>";
				} else {
					print $fh2 $_;
				}
			}
			
			print $fh1 "\n" . $rx;
			for (split//, $input) {
			
				$_ = qrm($_);
				
				if ($_ =~ /\n/) {
					print $fh1 $_ . $rx;
				} elsif (ord($_) == 1) {
					print $fh1 "<SOH>";
				} elsif (ord($_) == 4) {
					print $fh1 "<EOT>";
				} else {
					print $fh1 $_;
				}
				select undef, undef, undef, $delay;
			}
		} else {
				select undef, undef, undef, 0.05;	
		}
	}
	close $fh1;
	close $fh2;
	waitpid ($pid, 0);
	exit;
} else {
###########################################
# CLIENT PART
###########################################
# input: gmfsk_autofile from client : $clientout
# output: 	gMFSK.log for client(TX): $clientinin
#			gMFSK.log for server(RX): $serverin

print "Starting client->server channel\n";

	open ($fh3, ">>", $clientin);
	$fh3->autoflush(1);
	open ($fh4, ">>", $serverin);
	$fh4->autoflush(1);
	my $input = "";
	while (1) {
		if (-e $clientout) {
			$input = `cat $clientout`;
			`rm $clientout`;

			my $tx = maketx();
			my $rx = makerx();
			
			print $fh3 "\n" . $tx;
			for (split//, $input) {
				if ($_ =~ /\n/) {
					print $fh3 $_ . $tx;
				} elsif (ord($_) == 1) {
					print $fh3 "<SOH>";
				} elsif (ord($_) == 4) {
					print $fh3 "<EOT>";
				} else {
					print $fh3 $_;
				}
			}
			
			print $fh4 "\n" . $rx;
			for (split//, $input) {
			
				$_ = qrm($_);
				
				if ($_ =~ /\n/) {
					print $fh4 $_ . $rx;
				} elsif (ord($_) == 1) {
					print $fh4 "<SOH>";
				} elsif (ord($_) == 4) {
					print $fh4 "<EOT>";
				} else {
					print $fh4 $_;
				}
				select undef, undef, undef, $delay;
			}
		} else {
				select undef, undef, undef, 0.05;	
		}
	}
	close $fh3;
	close $fh4;
1;
}

##########################################
sub makerx {
##########################################

$datestr = getdate();

return ("RX" . $datestr); 
}
##########################################
sub maketx {
##########################################

$datestr = getdate();

return ("TX" . $datestr); 
}

##########################################
sub getdate {
##########################################
$ds = `date`;
$ds =~ /.*\s(\w\w\w)\s(\d*)\s(\d\d:\d\d):\d\d.*(\d\d\d\d)/;

$mon = monthnumber($1) + 1;
$monstr = sprintf ("%d", $mon );
$monstr = substr("00". $monstr, -2);
$daystr = sprintf ("%s", $2);
$daystr = substr("00". $daystr, -2);

$datestr = sprintf (" (%d-%s-%2d %s): ", $4,$monstr,$daystr,$3);
}

###############################################
sub monthnumber {
###############################################
	my $mon = shift @_;
	my $count = 0;
	my @months = qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
	foreach $month (@months) {
		if ($month =~ /$mon/i) {
			last;
		}
		$count++;
	}
	return $count;
} # end

###############################################
sub qrm {
###############################################
$char = shift @_;

	if (int(rand(1) * 100) <= $quality) {
		return $char;
	} else {
		return "*";
	}
}
###############################################