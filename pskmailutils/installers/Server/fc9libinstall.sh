#! /bin/sh

#echo "Installing Fldigi-2.05 for pskmail"

#yum install libportaudio2  fldigi
yum install  libhamlib2  libhamlib-utils

echo "Installing necessary SSL libraries for pskmail server"

yum install libnet-ssleay-perl libdigest-crc-perl
yum install libio-socket-ssl-perl elinks lynx libhamlib-utils 
yum install perl-CPAN
 
echo "Installing network app modules via cpan"

cpan Mail::POP3Client
cpan Email::LocalDelivery
cpan Email::Folder
cpan Net::SMTP::TLS
cpan IO::Multiplex

exit 1

