#! /usr/bin/perl -w

# get permanent data from somewhere

if (-e "/mnt/removable/pskmailperm.tar.gz") {
        `cp /mnt/removable/pskmailperm.tar.gz ./`;
        `cp /mnt/removable/fldigi.tar.gz ./`;
} elsif (-e "/media/sda1/pskmailperm.tar.gz") {
        `cp /media/sda1/pskmailperm.tar.gz ./`;
        `cp /media/sda1/fldigi.tar.gz ./`;
} elsif (-e "/mnt/sda/pskmailperm.tar.gz") {
        `cp /mnt/sda/pskmailperm.tar.gz ./`;
        `cp /mnt/sda/fldigi.tar.gz ./`;
} elsif (-e "/media/sdb1/pskmailperm.tar.gz") {
        `cp /media/sdb1/pskmailperm.tar.gz ./`;
        `cp /media/sdb1/fldigi.tar.gz ./`;
} elsif (-e "/mnt/sdb/pskmailperm.tar.gz") {
        `cp /mnt/sdb/pskmailperm.tar.gz ./`;
        `cp /mnt/sdb/fldigi.tar.gz ./`;
} else {
        print "Don't know where to get it...";
        exit;
}
`tar zxvf pskmailperm.tar.gz`;
`tar zxvf fldigi.tar.gz`;
exit;
