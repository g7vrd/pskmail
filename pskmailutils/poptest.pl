#! /usr/bin/perl -w

############################################################
# testing program for gmail (SSL) pop
# it checks if Mail::POP3Client has been properly installed.
#
#	If the module does not work $count will be -1.
#
#	Good luck!!!
#
############################################################

use Mail::POP3Client;

$myuser = "youruser" . "\@gmail.com";
$mypassword = "yourpassword";

$pop = new Mail::POP3Client( USER     => $myuser,
                               PASSWORD => $mypassword,
                               HOST     => "pop.gmail.com",
                               USESSL   => "true",
                             );
my $count = $pop->Count();

print "COUNT=$count\n";

for( $i = 1; $i <= $count; $i++ ) {
    foreach( $pop->Head( $i ) ) {
      /^(From|Subject):\s+/i && print $_, "\n";
    }
  }
  $pop->Close();

