#! /usr/bin/perl -w

`/home/guest/pskmailutils/saveit.sh`;

@media = `ls /mnt`;
my $found;

foreach $medium (@media) {
	chomp $medium;
#	print $medium, "\n" unless $medium =~ /cdrom/;
	if ($medium !~ /cdrom/i) {
		$fname = "/mnt/" . $medium . "/pskmailperm.tar.gz";
		if (-e "$fname") {
			print "Location found in $medium\n";
			`mv pskmailperm.tar.gz "$fname"`;
			$found = 1;
			last;
		} 
	} 
}
if ($found) {
	print "Config stored in $fname\n";
} else {
	print "Sorry, could not store config!\n";
}
exit;
