<?php 
	/*	Log viewer for fldigi & pskmail, (c) 2008 Par Crusefalk <per@crusefalk.se>
		This program is free software; you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation; either version 2 of the License, or
		(at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
	*/


	/* This is a tail function, returns linestoread no of lines from the file */
	function readlastlinesoffile($file,$linestoread)
        {
		// Arrays used here
		$line_array=array();
		$newarray=array();

		$linesread=0;
		$file_size = filesize($file);
		/* open the file, move to the end and seek out x no of new lines*/
                $fd = fopen($file, "r") or die("Cant open file $file.");
		// move to the end and hold that position
		fseek($fd,0,SEEK_END) ;
    		$position = ftell($fd) ;

		// read lines and check that we are still within the file
		while($linesread <= $linestoread || $file_size+$position<=0)
		{
			if(fgetc($fd) == "\n")
			{
				$slask = fgets($fd);
				if (strlen($slask)>1)
					$line_array[] = $slask;
				$linesread++;
			}
		    	fseek($fd,$position) ;
    			$position-- ;
		}
                fclose($fd);
                
		// put the new stuff at the bottom. TBD: this could be controlled by the url		
		$newarray = array_reverse($line_array);		

		// Lets output what we have
                $current = reset($newarray);
                if ($current) print(prepare4print($current));
                while($current = next($newarray))
                {
                        print(prepare4print($current));
                }
        }

	// remove dangerous characters and convert them to html
	function prepare4print($fix4print)
	{
		// Convert all unwanted characters into html
                $mystr = htmlentities($fix4print,ENT_COMPAT);
                $mystr = str_replace (chr(x7F), ' ', $mystr);
                $mystr = str_replace (SOH, '<b><i>SOH</i></b>', $mystr);
                $mystr = str_replace (EOT, '<b><i>EOT</i></b>', $mystr);

                if (strpos($mystr,"TX (20")===FALSE)
                	$mystr = $mystr . "<br>\n";
                else
                	$mystr = "<font color=\"#FA0000\">" . $mystr . "</font><br>\n";
	
		return $mystr;
	}


?>
<html>
<head>
<?php
$myrefresh = $_GET['refresh'];	
if ($myrefresh>0)
{
  print("<meta http-equiv=\"refresh\" content=\"$myrefresh\">");
}
?>
<title>SM0RWO pskmail server</title>
</head>
<body>
You may select how many lines to view and how often the page should be refreshed ! <br>
For 40 lines refreshed every 60 seconds, use the following url:<br>
<a href="http://www.crusefalk.se/gmfsk/index.php?lines=30&refresh=20">http://www.crusefalk.se/index/gmfsk/index.php?lines=30&refresh=20 </a><br><br><br>
<small>
<?php
        $filename = "gMFSK.log";
	$mylines = $_GET['lines'];
	if ($mylines > 0)
	{
	    readlastlinesoffile($filename,$mylines);
	}
	else{
	readlastlinesoffile($filename,100);
	}
?>
</small>
<A Name=bottom></A>
</body>
</html>

