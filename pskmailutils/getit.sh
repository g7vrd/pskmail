#!/usr/bin/env bash
# Get home folder from archive on removable device
cd ~
cp /mnt/removable/pskmailperm.tar.gz ./
tar zxvf pskmailperm.tar.gz
rm pskmailperm.tar.gz
