#! /usr/bin/perl -w


# ARQ module arq.pm by PA0R. This module is part of the PSK_ARQ suite of
# programs. PSK_ARQ adds an arq layer to keyboard oriented protocols like
# PSK31, PSK63, MFSK, MT63 etc.

# arq.pm includes the arq primitives common to server and client.

# This program is published under the GPL license.
#   Copyright (C) 2005, 2006, 2007, 2008
#       Rein Couperus PA0R (rein@couperus.com)
#
# *    arq.pm is free software; you can redistribute it and/or modify
# *    it under the terms of the GNU General Public License as published by
# *    the Free Software Foundation; either version 2 of the License, or
# *    (at your option) any later version.
# *
# *    arq.pm is distributed in the hope that it will be useful,
# *    but WITHOUT ANY WARRANTY; without even the implied warranty of
# *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# *    GNU General Public License for more details.
# *
# *    You should have received a copy of the GNU General Public License
# *    along with this program; if not, write to the Free Software
# *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# Date: 020709

#########################################
# link layer spec for PSK_ARQ
#########################################

=header1 PSK_ARQ MODULE

generic block format:	   <SOH>dcl[info])12EF<EOT|SOH>
						   		||| |     +-checksum (4xAlphaNum)
						    	||| +-----block (1 ... 128 chars)
						    	||+-------block type
						    	|+--------stream id
						    	+---------protocol number

Frame:					<Block><Block>....<stat>

SendQueue:
	<     ><     ><Block><Block><Block><Block><Block><Block><Block>
				  |											|
		   		  Firstsent									Lastblock

ReceiveQueue:
	<     ><     ><     ><Block><      ><Block><Block><Block><Block>
						|									 |
		  				Goodblock	 						 EndBlock

listen --- connect       ---data ---data --- data --- statreq              data---statreq       disc
		|	|				  |              |             |      |  |
		connected 			data ---					statrprt             statrprt  ack
=cut

use Digest::CRC qw(crc16);
use Digest::MD5 qw(md5_base64);
use Time::Local;
use Fcntl;
use Fcntl qw/:flock/;
use Socket;
use Net::hostent;
use IO::Socket;
use DB_File;
use lib qw{./lib ../lib};
use IPC::SysV qw(IPC_RMID IPC_CREAT S_IRWXU);

my $proto = 0;
my %myservers = ();
my $eot = sprintf("%c", 0x04);

 $Version = "Pskmail 0.9.2";

$SIG{PIPE} = 'IGNORE';

$sendbeacon = mkflag();

    my $IPCTXKEY = 6789;
    my $txid = msgget($IPCTXKEY,  0666 | IPC_CREAT);
    my $type_sent = 1;

    die "msgget failed: $!\n" unless defined $txid;

	sendmodemcommand ("server");

	sleep 1;

	if (-e "$ENV{HOME}/.pskmail/squelch.lk") {	# reset the squelch
		unlink "$ENV{HOME}/.pskmail/squelch.lk";
	}




%Callindex = ();
%Routes = ();

getshortcalls();

# init the routes hash from the routes file

					my $routes = "";
					my @routelines = `cat $ENV{HOME}/.pskmail/routes`;
					foreach $line (@routelines) {
						if ($line =~ /(\S+) (\S+) (\d+)/) {
							my $pre = $2 . " " . $3;
							$Routes{$1} = $pre;
						}
					}

my $logfile = "$ENV{HOME}/.pskmail/server.log";
$Inputfile = "";			# Default Rx input from gmfsk

 my ($Iamserver, $ServerCall, $monitor, $ShowBlock, $debug, $output, $TxInputfile, $Inputfile, $Txdelay, $Framelength, $Pingdelay, $rigptttune, $Aprs_connect, $Aprs_beacon, @Aprs_port, @Aprs_address, @prefixes, $positmessage);

$debug = 0;
$nosession = "none";
$Txdelay = 0;



##SERVERDEBUG


if (-e "$ENV{HOME}/.pskmail/.tx.lck") { unlink "$ENV{HOME}/.pskmail/.tx.lck"; }

if (-e "$ENV{HOME}/.pskmail/./pskmailrc.pl" ) {	#must be server
	eval `cat $ENV{HOME}/.pskmail/pskmailrc.pl`;
}else {
	$Iamserver = 0;
 	my @configdata;
 	if (-e "$ENV{HOME}/.pskmail/.pskmailconf") {
 		open (CONFIG, "$ENV{HOME}/.pskmail/.pskmailconf")or die "Could not open file\n";
 			$configdata = <CONFIG>;
 		close (CONFIG);
 		@confrecord = split ",", $configdata;

 		$debug = $confrecord[0];
 		$monitor = $confrecord[1];
 		$ShowBlock = $confrecord[2];
 		$Inputfile = "$ENV{HOME}/" . $confrecord[3];
 		$output = ">$ENV{HOME}/" . $confrecord[4];
 		$logfile = "$ENV{HOME}/" . $confrecord[5];
 		$Max_retries = $confrecord[6];
 		$Maxidle = $confrecord[7];
 		$Txdelay = $confrecord[8];
 		$positmessage = $confrecord[9] . $confrecord[10];
 		$offset = $confrecord[11];
 		$posit_second = $confrecord[12];
 	} else {
 		logprint ("CONFIG FILE NOT THERE\n");
 	}
 }
######################## parameter dialog for puppy version ###########
if ($Iamserver) {

	print "\n### Server v. $Version. (C) 2009 PA0R\n";

	if ($ServerCall eq "N0CAL") {
		print "\n### Please configure the server by answering some questions\n";
		print "### You can make your parameters permanent by editing the file pskmailrc.pl\n";
		print "### Use the geany or gedit editor (or vi) for that.\n\n";
		while (1) {
			print "\nWhich callsign do you want to use? ";
			$ServerCall = uc(<STDIN>);
			chomp $ServerCall;
			print "What is the address of your SMTP server? ";
			$relay = <STDIN>;
			chomp $relay;
			print "Your latitude (decimal degrees): ";
			my $lat = <STDIN>;
			chomp $lat;
			print "Your longitude (decimal degrees): ";
			my $lon = <STDIN>;
			chomp $lon;
			print "Your beacon message: ";
			my $positmessage = <STDIN>;
			chomp $positmessage;

			my $lat_sign;
			my $lon_sign;
			if ($lat < 0) {
				$lat = abs $lat;
				$lat_sign = "S";
			} else {
				$lat_sign = "N";
			}
			if ($lon < 0) {
				$lon = abs $lon;
				$lon_sign = "W";
			} else {
				$lon_sign = "E";
			}

			$aprs_lat = (($lat - int($lat)) * 60) + int($lat)* 100;

			if (int($lat) != 0 && abs($lat) < 10.0) {
				$aprs_lat = "0" . $aprs_lat;
			} elsif (int($lat) == 0) {
				$aprs_lat= "000" . $aprs_lat;
			} else {
				$aprs_lat = $aprs_lat;
			}

			if ($aprs_lat =~ /(\d\d\d\d\.\d\d)/) {
				$aprs_lat = $1;
			} elsif ($aprs_lat =~ /(\d\d\d\d\.\d)/) {
				$aprs_lat = $1 . "0";
			} elsif ($aprs_lat =~ /(\d\d\d\d)/) {
				$aprs_lat = $1 . ".00";
			} else {
				$aprs_lat = $1 . "0000.00";
			}


			$aprs_lon = (($lon - int($lon)) * 60) + int($lon)* 100;
			if (int($lon) == 0) {
				$aprs_lon = "00000" . $aprs_lon;
			} elsif (int($lon) < 10) {
				$aprs_lon = "00" . $aprs_lon;
			} elsif (int($lon) < 100) {
				$aprs_lon = "0" . $aprs_lon;
			} else {
				$aprs_lon =  $aprs_lon;
			}

			if ($aprs_lon =~ /(\d\d\d\d\d\.\d\d)/){
				$aprs_lon = $1;
			} elsif ($aprs_lon =~ /(\d\d\d\d\d\.\d)/){
				$aprs_lon = $1 . "0";
			} elsif ($aprs_lon =~ /(\d\d\d\d\d)/) {
				$aprs_lon = $1 . ".00";
			} else {
				$aprs_lon = "00000.00";
			}
			$Aprs_beacon = $aprs_lat . $lat_sign . "/" . $aprs_lon . $lon_sign . "&" . $positmessage;
			print "\nUsing the following data:\n";
			print "Server call: ", $ServerCall, "\n";
			print "SMTP server: ", $relay, "\n";
			print "APRS beacon: ", $Aprs_beacon, "\n\n";
			print "Is that correct? (y/n) [y]\n";
			my $a = <STDIN>;
			chomp $a;
			if ($a eq "y" || $a eq "") {
				print "\n";
				last;
			}
		}
	}
	`echo "$ServerCall,$relay,$Aprs_beacon" > $ENV{HOME}/.pskmail/.manual_config`;
}

 $TxInputfile = "ENV{HOME}/.pskmail/TxInputfile";
 ############ end config #####################

if ($Iamserver && -e "$ENV{HOME}/.pskmail/id_defined") {
	unlink "$ENV{HOME}/.pskmail/id_defined";
}

 $TxInputfile = "$ENV{HOME}/.pskmail/TxInputfile";

my $ClientCall = "";
my $CallerCall = "";
my $Call = "";
my $hiscall = "";
logprint("Program start\n");

###################### constants ###############################
my $Conreq = "c";
my $Conack = "k";
my $Statreq = "p";
my $Statrprt = "s";
my $Conid = "i";
my $Disreq = "d";
my $Disack = "b";
my $Abort = 'a';
my $Unproto = 'u';

my $Streamid = "0";
my $Current_session = "0";
my $InputString = "";
my $Sessionnumber = 0;

my $HeaderStart = " ";
my $FrameEnd = " ";
my $Bufferlength = 64;
$Framelength = 17;

###################### status variables #######################
my $Modem = "PSK250";		# PSK63, PSK125, DOMINOEX8, DOMINOEX11
my $Blockindex = 4;			# Data block length (2^x)
my $MaxDataBlocks = 8;		# Max. number of data blocks to be sent
my $Lastblockinframe = 0;	# Flag for last block in frame
my $Idle_counter = 0;		# seconds from last <SOH>
my $Connect_time = 0;		# connect time in seconds
my $Interval_time = 0;		# 500 seconds interval
my $InputLine = "";			# Input from gmfsk
my $TxFlag = 0;				# TX on
my $ConnectFlag = 0;		# Station is connected
my $linkquality = 0;		# Nr. of missing blocks for link quality
my $payloadlength = 32;		# Average length of payload received
my $max_idle = 17;			# Dynamic timing slot initial value
#tie %memreceive, "DB_File", "$ENV{HOME}/.pskmail/.arq_memreceive.dbm" or die "Can't open .arq_memreceive:$!\n";
#tie %aprs_store, "DB_File", "$ENV{HOME}/.pskmail/.aprsposits.dbm" or die "Can't open .aprsposits:$!\n";
							# hashes for storing previous receive data and aprsposits
my $lastpolldata = "";		# last poll data received correctly
my $laststatusdata = "";	# last status data received
my $last_eot = 0;			# was last modem character an <eot> ?

######### my status
my $Firstsent = 0;			# First block  I sent last turn
my $Lastblock = 0;			# Last block I sent last turn
my $Endblock = 0;			# Last  I received o.k.
my $Goodblock = 0;			# Last block I received conseq. o.k, 1st in send queue
my $Lastqueued = 0;			# Last block in my send queue
my $TXServerStatus= "";
my $ServerStatus = ""; 		# Listen, Connect_req, Disconnect_req, Abort_req
my @Missing = ();			# List of repeat requests
my $MissString = "";		# List of repeat requests
my $ReceivedLastBlock = 0;	# Flag for end of frame
##################

######### his status
my $HisGoodblock = 0;		# Other station's Good block
my $HisLastblock = 0;		# Other station's Block last sent
my $HisEndblock = 0;		# Other station's last received block
my @HisMissing = ();		# Other station's missing blocks
###################

######### status of my rig
$rigptttune = 0 unless $rigptttune;
my $AutoTune = 1;		# If 1 then fq has changed and ATU need to tune antenna
###################

##################### queues ##################################
my $TxTextQueue = "";		# Text in from mail engine
my @SendQueue = ();			# Array of data ready for sending
my $RxTextQueue = "";		# Text string
my @ReceiveQueue = ();		# Array of msg received ok.
my $TextOut = "";

my @options = ();
my $Latitude = "";
my $Longitude = "";
my %pingdb;
my $inputbytes = 0;

##################### APRS unproto stuff ######################
my %Owned_list = ();
my ($host, $port, $line);
#my $host_out = $Aprs_address;
#my $port_out = $Aprs_port;
my %Messagehash;
my $lastmessage = "";
my $Maxlinktime = 36000;
my $unattended = 0;

###############################################################
my $gpsnr = 0;

getoptions();

$StartHeader = sprintf("%c", '1');
$FrameEnd = sprintf("%c%c", '4','10');
$BlockLengthStr= sprintf("%c", $Blockindex + 48);
$BlockLength = (2 ** $Blockindex) ;
$wait_a_second = 0;

$Call = $ClientCall;

my $Startmessage = "";
if ($Iamserver) {
	$Startmessage .= "Program start: " . getdate() . "\n";
}

open ($logfh, ">>", $logfile) or die "Can not write to logfile!\n";
print $logfh $Startmessage;
close ($logfh);

if ($Iamserver == 0) {	# remove gps file if present
	if (-e ".gps") {
		unlink ".gps";
	}
}

if ($Iamserver && $Aprs_connect) {	#connect to aprs backbone


# fork off a handler for aprs...

    die "can't fork: $!" unless defined($kidpid = fork);

    if ($kidpid) {
		# don't do anything here....
		sleep 1;
    }    else {
	    my %RFout_list = ();
	    my $connectcount = 0;
	    my $aprs_is_connect_time = time;
##debug

		while (1) {
			my $cntr = 0;
		    # create a tcp connection to the specified host and port
		    while (1) {
				$connectcntr++;
				my $maxaprs = $#Aprs_address + 1;
				$maxaprs = 1 unless $maxaprs; ## fix for client
				$aprsinx = $cntr % $maxaprs;

			    eval {
			    	$handle_out = IO::Socket::INET->new(Proto     => "tcp",
			                                    	PeerAddr  => $Aprs_address[$aprsinx],
			                                    	PeerPort  => $Aprs_port[$aprsinx])
			           or die "can't connect to port $Aprs_port[$aprsinx] on $Aprs_address[$aprsinx]:";
			    };
			    if ($@) {
			    	print "Cannot connect...\n";
			    	sleep 10;
					$cntr++;
			    	if ($Aprs_address[$aprsinx + 1]) {logprint ("Reconnecting $cntr ($Aprs_address[$aprsinx + 1])\n");}
			    } else {
			    	$cntr = 0;
					$connectcntr = 0;
			    	logprint ("Connected to $Aprs_address[$aprsinx]\n");
			    	last;
			    }
			    sleep 10 * $connectcntr;;
		    }

					$passw = dohash($ServerCall);

					sleep 1;

		           print $handle_out "user $ServerCall pass $passw vers $Version filter u/PSKAPR\n";

					sleep 10;

		    # split the program into two processes, identical twins
		    die "can't fork: $!" unless defined($kidpid = fork());

		    # the if{} block runs only in the parent process
		    if ($kidpid) {
		        # copy the socket and handle the aprs message
		        while (defined ($line = <$handle_out>)) {
		            if ($line) {
		 #           	print STDOUT $line;
		 				$line = filter_aprs ($line);
				        if ($line) {handle_aprs ($line);}
				        $line = "";
		            }
		            select undef, undef, undef, 0.01;

		        }
		        kill("TERM", $kidpid);                  # send SIGTERM to child
		    }
		    # the else{} block runs only in the child process
		    else {
		        # copy standard input to the socket
		        while (1) {
		        	select undef, undef, undef, 0.01; # take some rest...
		            if (-e "$ENV{HOME}/.pskmail/.aprsmessage") {
		            	open ($fh, "$ENV{HOME}/.pskmail/.aprsmessage");
		            	$MSG = <$fh>;
		            	close ($fh);
		            	unlink ("$ENV{HOME}/.pskmail/.aprsmessage");
						eval {
							if ($MSG) {
								print $handle_out $MSG or die "Cannot print to aprs: $@";
							}
						};
						if ($@ && $@ =~ /Cannot prin/) {
							logprint ($@);
						} else {
							if ($MSG) {
								logprint ("Send>APRS-IS:$MSG\n");
							} else {
								logprint ("No aprs message in file\n");
							}
						}
		            	$MSG = "";

		            }

		        }

		    }

		    sleep 20;
		}
##end debug


	} # end child

}

    die "can't fork: $!" unless defined($msgqueuepid = fork);

    if ($msgqueuepid) {
		# don't do anything here....
    } else {		# message queue child...
		if (-e "$ENV{HOME}/.pskmail/id_defined") {
			exit;
		}
	    my $IPCKEY = 9876;
	    my $id = msgget($IPCKEY, IPC_CREAT | S_IRWXU);

	    my $rcvd;
	    my $type_rcvd;
	    my $squelch = 0;
		my $Queue_input = "";
		my $debug = 0;
		my $squelchtime = 0;

		`touch $ENV{HOME}/.pskmail/id_defined`; # only start 1 copy

	    if (defined $id) {
	    	while(1) {
	            if (msgrcv($id, $rcvd, 1, 0, 0)) {
	                ($type_rcvd, $rcvd) = unpack("l! a*", $rcvd);
					if ($rcvd || $rcvd eq "0") {


						if (ord ($rcvd) == 1) {
							if ($last_eot == 1) {
								$last_eot = 0;
							}
							if (-e "$ENV{HOME}/.pskmail/.tx.lck") { unlink "$ENV{HOME}/.pskmail/.tx.lck"; }

							if ($squelch == 1) {
								$Queue_input .= "<SOH>\n";

								while (-e "$ENV{HOME}/.pskmail/.input") {
									select undef, undef, undef, 0.001;
								}
								open (MOUT, ">" , "$ENV{HOME}/.pskmail/.input");
								print MOUT $Queue_input;
								close MOUT;
								$Queue_input = "<SOH>";
								$squelchtime = time();
							} else {
								$Queue_input .= "<SOH>";
								$squelch = 1;
								`touch $ENV{HOME}/.pskmail/squelch.lk`;
								$squelchtime = time();
							}
						} elsif (ord ($rcvd) == 2) {	## <STX>
							# ignore
						} elsif (ord ($rcvd) == 4) {
							if ($last_eot == 0) {		## only 1 <EOT>
								$Queue_input .= "<EOT>\n";
								$squelch = 0;
								if (-e "$ENV{HOME}/.pskmail/squelch.lk") {
									unlink "$ENV{HOME}/.pskmail/squelch.lk";
								}

									while (-e "$ENV{HOME}/.pskmail/.input") {
										select undef, undef, undef, 0.01;
									}
									open (MOUT, ">" , "$ENV{HOME}/.pskmail/.input");
									print MOUT $Queue_input unless $Queue_input eq "<EOT>";
									close MOUT;

								$Queue_input = "";
							}
							$last_eot = 1;
						} elsif (ord ($rcvd) == 6) {
							if (-e "$ENV{HOME}/.pskmail/.tx.lck") { unlink "$ENV{HOME}/.pskmail/.tx.lck"; }
							if (-e "$ENV{HOME}/.pskmail/squelch.lk") { unlink "$ENV{HOME}/.pskmail/squelch.lk";}
						} elsif (ord ($rcvd) == 31) {	## <US> opens squelch
								`touch $ENV{HOME}/.pskmail/squelch.lk`;
								$squelchtime = time();
						} else {
							if ($squelch == 1) {
								$Queue_input .= $rcvd;
								my $time_now = time();


								if ($time_now - $squelchtime > 10) {	# reset squelch after 10 secs.
									$squelch = 0;
									$squelchtime = $time_now;

									if (-e "$ENV{HOME}/.pskmail/squelch.lk") {
										unlink "$ENV{HOME}/.pskmail/squelch.lk";
									}
								}
							}

						}
					} else {

						my $time_now = time();


						if ($time_now - $squelchtime > 10) {	# reset squelch after 10 secs.
							$squelch = 0;
							$squelchtime = $time_now;
							if (-e "$ENV{HOME}/.pskmail/squelch.lk") {
								unlink "$ENV{HOME}/.pskmail/squelch.lk";
							}
						}
						select (undef , undef, undef, 0.01);

					}

					select (undef , undef, undef, 0.01);
	            } else {
	                die "# msgrcv failed\n";
	            }
	        } # end while

	       msgctl($id, IPC_RMID, 0) || die "# msgctl failed: $!\n";
	    } else {
	        die "# msgget failed$!\n";
	    } # end $id defined

		unlink "$ENV{HOME}/.pskmail/id_defined";
		exit;
	}

1;

###########################################################

###########################################################
sub newsession {	# new session number
###########################################################
#	$SessionNumber = ord($Streamid) - 32;
	++$SessionNumber;
	if ($SessionNumber > 63) { $SessionNumber = 0;}
	$Streamid = sprintf ("%c", $SessionNumber + 32);
	return $Streamid;
}

###########################################################
sub newblocknumber { # get new blocknumber
###########################################################

	$Lastqueued++;

	if ($Lastqueued >= $Bufferlength) {
		$Lastqueued -= $Bufferlength;
	}

	return $Lastqueued ;

}

###########################################################
sub makeindex { # make transmitable index from number
###########################################################
my $index = shift @_;
my $character = "";

$character = sprintf ("%c", ($index % $Bufferlength) + 32);

return $character;

}

###########################################################
sub contime {		# UTC time of this connect
###########################################################
#	(my $sec, my $min, my $hr) =gmtime;
#	$Contime = sprintf ("%02d:%02d", $hr, $min);

}
############################################################
sub checksum {		# Checksum of header + block
############################################################
# Time + password + header + block
############################################################

my $String = shift (@_);
my $Encrypted = "0000" . sprintf ("%X", crc16($String));

	return substr ($Encrypted, -4);

}

############################################################
sub newconnectblock {	# Connect (client:port, server:port)
# c block = Client:port Server:port <streamnr. (0)> <max. blocklen>
# e.g.: '00cEA3FG:1024 PI4TUE:24 4'
############################################################
	my $server = shift @_;
	my $Call = shift @_;

	if (exists $myservers{$server}) {
		$proto = 2;
	} else {
		$proto = 0;
	}

=header1
#	if (-e "$ENV{HOME}/.pskmail/calls.txt") {
#		open (CALLS, "$ENV{HOME}/.pskmail/calls.txt");
#		my @calls = <CALLS>;
#		foreach my $call (@calls) {
#			chomp $call;
#			my $logval = $call . "," . $server . "\n";
#			if ($server =~ /$call/) {
#				$proto = 2;
#				last;
#			} else {
#				$proto = 0;
#			}
#		}
#	}
=cut
	$Connect_time = 0; # start timer
	if ($proto == 0) {
		return ("0"
		. "0"		#Streamid
		. $Conreq
		. $ClientCall
		. ":1024 "
		. $server
		. ":24 "
		. $BlockLengthStr);
	} elsif ($proto == 2) {
		my $shortcall = "";
		my $shortserver = "";

		$shortcall = shortcall($ClientCall);
		$shortserver = shortcall($server);


		if ($shortcall && $shortserver) {
			return ("2"
			. "0"		#Streamid
			. $Conreq
			. $shortcall
			. $shortserver
			. $BlockLengthStr);
		} else {
			return ("0"
			. "0"		#Streamid
			. $Conreq
			. $ClientCall
			. ":1024 "
			. $server
			. ":24 "
			. $BlockLengthStr);
		}
	}

}

############################################################
sub ttyconnectblock {	# Connect (caller:port, my:port)
# c block = Caller:port My:port <streamnr. (0)> <max. blocklen>
# e.g.: '00cEA3FG:87 PI4TUE:87 4'
############################################################
	my $ServerCall = shift @_;
	my $Call = shift @_;

	$Connect_time = 0; # start timer
	return ("0"
	. "0"	#Streamid
	. $Conreq
	. $Call
	. ":87 "
	. $ServerCall
	. ":87 "
	. $BlockLengthStr)
}
###############################################################
sub newackblock {		# Connect acknowledge (server:port, client:port)
# k block = Server:port Client:port <streamnr.> <max. blocklen>
# e.g: '00kPI4TUE:24 EA3FG:1024 8'
###############################################################
	my $server = shift @_;
	my $Call = shift @_;
	my $ro = "";

		my $time = time();
		my $pre = $server . " " . $time . "\n";
		$Routes{$Call} = $pre;
		open (ROUTES, ">$ENV{HOME}/.pskmail/routes");
		foreach $key (keys %Routes) {
			$value = $Routes{$key};
			$ro = $ro . $key . " " . $value . "\n";
		}
#		print $ro;
		print ROUTES $ro;
		close (ROUTES);


	$Connect_time = 0; # start timer

$proto=0;	# switch off until properly debugged....

	if ($proto == 0) {
		return ("0"
		. $Streamid
		. $Conack
		. $server
		. ":24 "
		. $Call
		. ":1024 "
		. $BlockLengthStr);
	} elsif ($proto == 2) {
		my $shortserver = "";
		my $shortcall = "";

		$shortcall = shortcall($ClientCall);
		$shortserver = shortcall($server);

=head1
		if (exists $Callindex{$server}) {
			$shortserver = $Callindex{$server};
		}

		if (exists $Callindex{$ClientCall}) {
			$shortcall = $Callindex{$ClientCall};
		}
=cut
		if ($shortserver && $shortcall) {
			return ("2"
			. $Streamid
			. $Conack
			. $shortserver
			. $shortcall
			. $BlockLengthStr);
		} else {
			return ("0"
			. $Streamid
			. $Conack
			. $server
			. ":24 "
			. $Call
			. ":1024 "
			. $BlockLengthStr);
		}
	}
}
###############################################################
sub ttyackblock {		# Connect acknowledge (server:port, client:port)
# k block = Server:port Client:port <streamnr.> <max. blocklen>
# e.g: '00kPI4TUE:87 EA3FG:87 4'
###############################################################
	my $caller = shift @_;
	my $Call = shift @_;

	$Connect_time = 0; # start timer

	return ("0"
	. $Streamid
	. $Conack
	. $ClientCall
	. ":87 "
	. $CallerCall
	. ":87 "
	. $BlockLengthStr);
}
###############################################################
sub pollblock {		# poll
#p frame = <last block tx><last block rx ok><last block rx> <missing blocks>
#e.g.: '00pXHCAB'
###############################################################

	my ($MyLast, $Good, $Last, @RptBlocks) =  @_;

	my $MyLastblock = sprintf ("%c", $MyLast + 0x20);

	my $thisgoodblock = sprintf("%c", $Goodblock + 32);
	my $thisLastblock = sprintf("%c", $Endblock + 32);

	my $misses = join('', @RptBlocks);
	return ("0"
	. $Streamid
	. $Statreq
	. $MyLastblock
	. $thisgoodblock
	. $thisLastblock
	. $misses
	);

}

################################################################
sub statreport {		# Status report (End, Good, Lastrx, Missing)
#p frame = <last block tx><last block rx ok><last block rx> <missing blocks>
#e.g.: '00sXHCAB'
################################################################
	my ($MyLast, $Good, $Last, @RptBlocks) =  @_;

		my $MyLastchar = sprintf ("%c", $MyLast + 0x20);
		my $Goodchar = sprintf ("%c", $Goodblock + 32);
		my $Lastchar = sprintf("%c", $Endblock + 32);

		my $misses = join('', @RptBlocks);
		if ($Goodchar eq $Lastchar) {
			$misses = "";
		}
		return ("0"
		. $Streamid
		. $Statrprt
		. $MyLastchar
		. $Goodchar
		. $Lastchar
		. $misses
		);
}

###############################################################
sub identblock {		# Identify (mycall, hiscall)
#i frame = '00iPI4TUE de PA0R'
###############################################################
	my ($Call, $hiscall) = @_;

	if ($Iamserver == 1) {
		return (
			"0"
			. $Streamid
			. $Conid
			. $hiscall
			. " de "
			. $Call);
	}else {
		return (
			"0"
			. $Streamid
			. $Conid
			. $Call
			. " de "
			. $hiscall);

	}

}

##############################################################
sub disconnectblock {		# Disconnect session
#d frame = ""
#e.g.: '00d'
##############################################################

	return (
	"0"
	. $Streamid
	. $Disreq);

}

##############################################################
sub abortblock {		# Abort session
#a frame = ""
#e.g.: '00a'
##############################################################

	return (
	"0"
	. $Streamid
	. $Abort);

}

##############################################################
sub pingblock {		# e.g. Ping frame
# u block = From:port
# e.g: '00uPA0R:7 '
##############################################################

	if ($Iamserver == 0) {
		return (
		"0"
		. $Streamid
		. $Unproto
		. $Call
		. ":7 ");
	} else {		# I am a server, send a 71
		return (
		"0"
		. "0"	#Streamid
		. $Unproto
		. $ServerCall
		. ":71 ");

	}

}
##############################################################
sub beaconblock {		# Beacon frame
# u block = From:port  data
# e.g: '00uPA0R:72 Beacon text '
##############################################################
		my $date = getgmt();
		if ($date =~ /\s(\d\d:\d\d:\d\d)/) {
			$date = $1;
		}

		return (
		"0"
		. "0"	#Streamid
		. $Unproto
		. $ServerCall
		. ":72 "
		. "$Version\n - "
		. $date
		. " ");

}
##############################################################
sub ui_messageblock {		# UI email frame
# u block = From:port  data
# e.g: '00uPA0R:25 Message'
##############################################################
		my $message = shift @_;
#		if (length ($message) > 67) {
#			$message = substr($message, 0 , 67);
#		}

		return (
		"0"
		. "0"	#Streamid
		. $Unproto
		. $Call
		. ":25 "
		. "$message\n");

}
##############################################################
sub ui_aprsblock {		# UI aprs frame
# u block = From:port  data
# e.g: '00uPA0R:26 Message'
##############################################################
		my $message = shift @_;
#		if (length ($message) > 67) {
#			$message = substr($message, 0 , 67);
#		}

	if ($Iamserver) {
		return (
		"0"
		. "0"	#Streamid
		. $Unproto
		. $ServerCall
		. ":26 "
		. "$message");

	} else {
		return (
		"0"
		. "0"	#Streamid
		. $Unproto
		. $Call
		. ":26 "
		. "$message");
	}
}

##############################################################
sub aprs_linkblock {		# UI link frame
# block = Call><ServerCall
# e.g: '00uPA0R><PI4TUE'
##############################################################

	getoptions();

		return (
		"0"
		. "0"	#Streamid
		. $Unproto
		. $Call
		. "><"
		. $ServerCall
		. " ");
}
##############################################################
sub aprs_linkackblock {		# UI link ack frame
# block = Call><ServerCall
# e.g: '00uPI4TUE<>PA0R '
##############################################################

		return (
		"0"
		. "0"	#Streamid
		. $Unproto
		. $ServerCall
		. "<>"
		. $Call
		. " ");
}
##############################################################
sub Queue_txdata {
##############################################################

my $Inpt = shift @_;

if ($Inpt) {
	$TxTextQueue .= $Inpt;
}

my $newqueuedblock = 0;
	my $qlength = $Lastqueued - $HisGoodblock;

# wipe the send queue
		if ($qlength == 0) {
			for (my $ii = 0; $ii < $Bufferlength; $ii++){
				$Sendqueue [$ii] = "";
			}
		}

	if ($qlength < 0) { $qlength += $Bufferlength; }

	if ($qlength > $Framelength) { return }; # next time better....

	while (length $TxTextQueue > 0 ) {

		$newqueuedblock = newblocknumber();

		$Sendqueue[ $newqueuedblock ] =  substr ($TxTextQueue, 0, $BlockLength);

 		$Lastqueued = $newqueuedblock;
		$qlength = $Lastqueued - $HisGoodblock;
		if ($qlength < 0) {
			$qlength += 64;
		}

		if (length $TxTextQueue >= $BlockLength) {
			$TxTextQueue = substr($TxTextQueue, $BlockLength);
		} else {
			$TxTextQueue = "";
			last;
		}
		if ($debug == 5){
			print $TxTextQueue, "\n";
		}


		last if ($qlength > $Framelength) ;

	}


	if ($debug == 5) {
		printf ("Lastqueued=%d, Queuelength=%d\n", $Lastqueued, $qlength);
	}
}


##############################################################
sub packdata {		# Add header
# <0x20...0x5f><Data>
# e.g.: '00jThis is data for'
##############################################################

	my $Currentblock = shift @_;
	my $Data = shift @_;

if ($Data) {
	return (
	"0"
	. $Streamid
	. $Currentblock
	. $Data);
}

}

#############################################################
sub make_block { #Adds SOH and checksum
# e.g.: '<SOH>00jThis is data for'akj0
#############################################################
	my $info = shift @_;
		if ($info) {
			my $check = checksum ($info);

			return ( $StartHeader . $info . $check);
		}
}

#############################################################
sub sendit {		# send routine
#############################################################
	my $sendstring = shift @_;
	my $counter = 0;

	my $index = index ($sendstring, sprintf("%c", 0x04));

	if ($index > 0) {

		sleep ($Txdelay);

		if ($rigptttune == 1) {
			ptttune();	# initiate an autotune if required
		}


  		if ($Iamserver) {
  			$sendstring = "\037" . $sendstring . "\000";
  		} else {
  			$sendstring = "\037" . $sendstring . "\000";
  		}

  		my $sleeptimer = 0;	##software squelch

		while (-e "$ENV{HOME}/.pskmail/squelch.lk") {
  			sleep 1;
  			$sleeptimer++;
  			if ($sleeptimer > 3) {
  				unlink "$ENV{HOME}/.pskmail/squelch.lk";
  				last;
  			}
		}

		msgsnd($txid, pack("l! a*", $type_sent, $sendstring), 0 ) or die "# msgsend failed: $!\n";

		`touch $ENV{HOME}/.pskmail/.tx.lck`;

		$Sendstring = "";

		$Idle_counter = 0; # Activity, reset counter

		if ($wait_a_second) {
			sleep ($wait_a_second);
			$wait_a_second = 0;
		}
	}

}
#############################################################
sub sendbulletin {		# send routine
#############################################################
	my $sendstring = shift @_;
	my $counter = 0;
	my $count = 0;
	my $bulletinout = "";

		my @bulletinlines = split "\n", $sendstring;
		push @bulletinlines, "NNNN\n";

		foreach $bulletinline (@bulletinlines) {
			if ($bulletinline =~ /NNNN/) {
				$count = 4;
			}
			$bulletinline .= "\n";
			$bulletinline = make_block ($bulletinline);
			$bulletinline .= "\004";
			$bulletinout .= $bulletinline ;
			$count++;
			if ($count > 3) {
				$bulletinout .= "\000";
				while (-e "$ENV{HOME}/.pskmail/.tx.lck") {
					select (undef,undef,undef,0.1);
				}
				msgsnd($txid, pack("l! a*", $type_sent, $bulletinout), 0 ) or die "# msgsend failed: $!\n";
				$bulletinout = "";
				`touch $ENV{HOME}/.pskmail/.tx.lck`;
				$count = 0;
			}
		}
}

############################################################
sub send_frame { #send blocks in queue
# frame = [<id>]<data><data><stat><EOT>
############################################################
my $Payload = shift @_;
my $charindex = 0;
my $outstring = "";

Queue_txdata($Payload);	##debug


	if ($TXServerStatus eq "TXAbortreq") {			# send abort frame
		my $finfo = abortblock();
		$outstring .= make_block($finfo);

	} elsif ($TXServerStatus eq "TXPing") {
		$AutoTune = 1; #
		my $info = pingblock ();
		$Lastblockinframe = 1;
		$outstring .= make_block($info);

	} elsif ($TXServerStatus eq "TXBeacon") {
        	$AutoTune = 1; # Always tune before beacon
		my $info = beaconblock ();
		$Lastblockinframe = 1;
		$outstring .= make_block($info);

	} elsif ($TXServerStatus eq "TXUImessage") {

		my $info = ui_messageblock ($Payload);
		$Lastblockinframe = 1;
		$outstring .= make_block($info);

	} elsif ($TXServerStatus eq "TXaprsmessage") {

		my $info = ui_aprsblock ($Payload);
		$Lastblockinframe = 1;
		$outstring .= make_block($info);

	} elsif ($TXServerStatus eq "TXlinkreq") {

		my $info = aprs_linkblock();
		$Lastblockinframe = 1;
		$outstring .= make_block($info);

	} elsif ($TXServerStatus eq "TXlinkack") {
		$AutoTune = 1; # Always tune before link
		my $info = aprs_linkackblock();
		$Lastblockinframe = 1;
		$outstring .= make_block($info);

	} elsif ($TXServerStatus eq "TXConnect_ack") {	# send connect ack

		my $info = newackblock ($ServerCall, $ClientCall);
		$Lastblockinframe = 1;
		$outstring .= make_block($info);
		$linkquality = 2;
		$AutoTune = 1; # Always tune before connect
		if ($Iamserver) {
			# tell APRS-IS we've got him....
			$Owned_list{$ClientCall} = time();
			logprint ("Added $ClientCall to link list\n");
			# send APRS-IS confirm
			$MSG = "$ServerCall>PSKAPR,TCPIP*::PSKAPR   :GATING $ClientCall";
			aprs_send($MSG);
				print "$ClientCall->$ServerCall\n";
				my $time = time();
				$pre = $ServerCall . " " . $time . "\n";
				$Routes{$ClientCall} = $pre;

				my $ro = "";
				open (ROUTES, ">$ENV{HOME}/.pskmail/routes");
				foreach $key (keys %Routes) {
					$value = $Routes{$key};
					$ro = $ro . $key . " " . $value . "\n";
				}
#				print $ro;
				print ROUTES $ro;
				close (ROUTES);
		}
	} elsif ($TXServerStatus eq "TTYConnect_ack") {	# send connect ack
		my $info = ttyackblock($CallerCall, $ClientCall);
		$Lastblockinframe = 1;
		$outstring .= make_block($info);
		$linkquality = 2;
		$wait_a_second = 2;

	} elsif ($TXServerStatus eq "TXConnect") {		# send connect request

		my $info = newconnectblock($ServerCall, $ClientCall);
		$Lastblockinframe = 1;
		$outstring .= make_block($info);
	} elsif ($TXServerStatus eq "TTYConnect") {		# send connect request

		my $info = ttyconnectblock($ServerCall, $ClientCall);
		$Lastblockinframe = 1;
		$outstring .= make_block($info);
	} elsif ($TXServerStatus eq "TXDisconnect") {	# send disconnect request
		my $info = disconnectblock();
		$outstring .= make_block($info);
		$Lastblockinframe = 1;

	} elsif ($TXServerStatus eq "TXPoll") {			# send poll

#		printf ("Sending poll: Lastblock=%d, Endblock=%d, Goodblock=%d, Missing=%s\n", 					$Lastblock, $Endblock, $Goodblock, $Missing);

		my $info = pollblock($Lastblock, $Endblock, $Goodblock, @Missing);

		$Lastblockinframe = 1;

		if ($Iamserver) {
			while (1) {
				last if time() % 10 == 0;
				select undef, undef, undef, 0.1;
			}
		} else {
			while (1) {
				last if time() % 10 == 5;
				select undef, undef, undef, 0.1;
			}
		}
		$outstring .= make_block($info);

	} elsif ($TXServerStatus && $TXServerStatus eq "TXStat") {			# send status

#		printf ("Sending status: Lastblock=%d, Endblock=%d, Goodblock=%d, Missing=%s\n", 					$Lastblock, $Endblock, $Goodblock, $Missing);

		my $info = statreport($Lastblock, $Endblock, $Goodblock, @Missing);

		$Lastblockinframe = 1;

		$outstring .= make_block($info);

	} elsif ($TXServerStatus eq "TXTraffic") {		# traffic
## id block
		if ($Interval_time > 500) {				# every 500 seconds
			$Interval_time = 0;
			my $info = identblock($ServerCall, $ClientCall);
			$outstring .= make_block($info);
		}
## Data

		if ($debug == 5) { #debug
			printf ("Payload=%s\n", $Payload);
		}

#		Queue_txdata($Payload);

		my $queuelength = ($Lastqueued - $HisGoodblock) % $Bufferlength;	# lenght of payload queue
		if ($queuelength < 0) {
			$queuelength += $Bufferlength;
		}

		if ($debug == 5) {	#debug
			print "payloadqueue =", $queuelength, "\n";
		}

		my $blocks_sent = 0;
		my $text = "";
## missing data
		foreach $blockindex (@HisMissing) {
			$info = packdata($blockindex, $Sendqueue[(ord $blockindex) - 32]);
			if ($info) {
				$outstring .= make_block($info);
				$blocks_sent++;
			}
		}
## queued data

		if ($debug == 5) {	#debug
			printf ("HisEndblock=%d, Blocks_sent=%d, Last_queued=%d, Maxdata=%d\n", 						$HisEndblock, $blocks_sent, $Lastqueued, $MaxDataBlocks);
		}

		for ($runvar = $HisEndblock + 1;
				($queuelength > 0)
				&& ($runvar <= $HisEndblock + ($MaxDataBlocks - $blocks_sent));
				$runvar++) {

			if ($debug == 5 ) {	#debug
				printf ("Runvar=%d, Lastqueued=%d\n", $runvar, $Lastqueued);
			}

			if ($runvar > ($Bufferlength -1)) {
				$charindex = $runvar - $Bufferlength;
			} else {
				$charindex = $runvar;
			}
			$Lastblock = $charindex;

			if ($debug == 5) {	#debug
				printf ("Runvar=%d, lastqueued=%d\n", $runvar, $Lastqueued);
			}

			if (($Lastqueued - $runvar) < 0) {
				last if ($Lastqueued -$runvar + $Bufferlength) == 0;
			 } else {
				last if ($runvar > $Lastqueued) ;
			}
			my $char = sprintf("%c", $charindex + 32);

			if ($Sendqueue[$charindex]) {
				$info = packdata($char, $Sendqueue[ord ($char) - 32]);

			if ($debug == 5) {	#debug
				printf ("charindex=%d, char=%s", $charindex, $char);
			}

				$outstring .= make_block($info);

				last if ($runvar == $Lastqueued);

			} else { last; }
		}
## status block
		$info = statreport($Lastblock, $Endblock, $Goodblock, @Missing);
		$outstring .= make_block($info);
		$Lastblockinframe= 1;
	}

	if ($Lastblockinframe == 1) {
		$outstring .= $FrameEnd;
		$Lastblockinframe = 0;
	}

	if ($debug == 0) {
		sendit($outstring);
		sleep 4;
	} else  {
		logprint ($outstring);
		logprint ("\n");
		sendit($outstring);
		sleep 4;
	}


}

####################### end send_frame ##############################

####################################################################
sub disconnect {
####################################################################
$TXServerStatus = "TXDisconnect";
send_frame();
return;
}

#####################################################################
sub handle_rxqueue {		# Do we have consecutive good buffers?
#####################################################################

my $runvar = 0;
my $Endpoint = 0;
my $index = 0;
my $inx = 0;
my $cell = "";
my $Character = "";
my $runvarinit;
$MissString = "";
@Missing = ();
$Missing = "";

	if ($HisLastblock == $Goodblock) {
		$Endblock = $Goodblock;
		$Endpoint = $Goodblock; ##debug
		return;
	} elsif ($HisLastblock < ($Goodblock)) {
		$Endpoint = $HisLastblock + $Bufferlength;
	} else {
		$Endpoint = $HisLastblock;
	}


	if ($debug == 5) {
		printf ("Hislast=%d, Goodblock=%d, Endblock=%d, Endpoint=%d\n", $HisLastblock, 				$Goodblock, $Endblock, $Endpoint);
	}

	# set missing blocks

	my $Goodstuff = 1;
	my $missers = 0;

	if ($Goodblock == $Bufferlength -1) {
		$runvarinit = 0;
	} else {
		$runvarinit = $Goodblock +1;
	}

	my $cntruns = 0;

	for ($runvar = $runvarinit; $runvar <= $Endpoint; $runvar++) {

			$cntruns++;

			if ($cntruns > 8) { last; }	##debug

			if ($runvar > ($Bufferlength - 1)) {
				$index = $runvar - $Bufferlength;
			} else {
				$index = $runvar;
			}

		if ($ReceiveQueue[$index]) {

			if ($debug == 5) {	#debug
				printf ("%d - %s\n", $index, $ReceiveQueue[$index]);
			}

			$Character = "";
			$Endblock = $index; 	#set Endblock


			if ($Goodstuff == 1) {
				$Goodblock = $index;
				$RxTextQueue .= $ReceiveQueue[$index];
				$ReceiveQueue[$index] = "";
				$RxTextQueue = getlocale ($RxTextQueue);
			}

		} else {

			$Goodstuff = 0;
			$missers++;
#			if ($missers > $MaxDataBlocks){
#				$Endblock = $index; # safety net, something is wrong...
#				last;
#			}
			$Character = makeindex($index);
			push @Missing, $Character;
			$MissString = join ('', @Missing);
			$MissString = substr ($MissString, 0, $MaxDataBlocks -1 );

			if ($debug == 5) {
				printf("Endpoint=%d, Index=%d, Info:%s", $Endpoint, $index,
					$ReceiveQueue[$index]);
				printf("\nMissString=%s\n", $MissString);
			}

		}
	}

	if (length ($MissString) == 8 && $Iamserver) {
		$MissString = "";
		$HisLastblock = $Goodblock;
	}

}

#####################################################################
sub unframe {		# get frame from gmfsk and unpack
#####################################################################

my $TextFile = shift @_;
my $Block = "";
my $BlockIndex = 0;
my $ii = 0;
my $closure = "";
#my $BigEarserverport = 0;

#$debug = 2;

#	while ($closure ne "<EOT>") {
		if ($debug == 2) {
			printf ("Input=%s\n", $TextFile);
		}

		my $BlockStart = index ($TextFile, "<SOH>");
		if ($debug == 2) {
			printf ("Start=%d\n", $BlockStart);
		}
		if ($BlockStart < 0) { # not enough stuff yet
			 return $TextFile;
		}	else {
			$Idle_counter = 0; # but we have a block coming...

		}


		$Block = substr ($TextFile, $BlockStart);
		if ($debug == 2) {
			printf ("Block=%s\n", $Block);
		}

		my $BlockEnd = index ($Block, "<SOH>", 5);
		if ($BlockEnd < 0 ) {
			$BlockEnd = index ($Block, "<EOT>");
		}

		if ($BlockEnd < 0) {  return $TextFile;  } # not enough stuff yet

		if ($debug == 2) {
			printf ("End=%d\n", $BlockEnd);
		}


		$Block = substr ($Block, 0, $BlockEnd + 5);
#=cut
 		reset;
#logprint ("\nBLOCK:$Block\n");
		if ($Block =~ /<SOH>(<SOH>.*)/s) {
			$Block = $1;
			$TextFile = $1;
		}
		if ($Block =~ /<EOT>(<SOH>.*)/s) {
			$Block = $1;
			$TextFile = $1;
		}

		if ($Block =~ /<SOH>((.)(.)(.)(.*))(....)(<EOT>)/s ||
			$Block =~ /<SOH>((.)(.)(.)(.*))(....)(<SOH>)/s) {
#logprint ("\nBLOCKCHECK:$Block\n");

			my $checkinfo = $1;
			my $operand = $4;
			my $payload = $5;
			my $check = $6;
			$closure = $7;
#logprint ("\nINFO:$1,$4,$5,$6,$7\n");

			if ($debug == 5) {
				print "All   :",$1, "\n";
				print "Proto :", $2, "\n";
				print "Stream:", $3, "\n";
				print "Oper  :" ,$4, "\n";
				print "Data  :", $5, "\n";
				print "Check :", $6, "\n";
				print "Close :", $7, "\n";
			}


			$Current_session = $3;

	## got it, remove from queue
#logprint ("\nQUEUE_IN:$TextFile\n");

			$TextFile = substr ($TextFile, index($TextFile, $1) + 4 + length ($1) + 5);

#logprint ("\nQUEUE_OUT:$TextFile\n");
#$debug = 2;

			if ($debug == 2) {
				printf ("Txtfromfile_now:%s\n", $TextFile);
				print checksum($checkinfo), "\n";
				print $check, "\n";
			}
#$debug = 0;
	## checksum o.k.?
			if ($check eq checksum($checkinfo)) {
				if ($debug == 2) {
					print "checksum o.k.\n" ;
					printf ("operand=%s\n", $operand);
				}
=head1	# BigEar experimental, removed from mainstream
				if ($BigEarserverport && $Iamserver) {
						my $packet = $Block;
						my @parts = split "\n", $packet;
						$packet = join "<|>", @parts;
						`echo '$packet' >> $ENV{HOME}/.pskmail/.bigear`;
				}
=cut
	## store checksum + data, you never know if you are going to need it...
=head1
				if ($operand eq "k") {
					$memreceive{$check} = "<SOH>" . $checkinfo . $check . "<EOT>";
				}
				if ($operand eq "k") {
					$memreceive{$check} = "<SOH>" . $checkinfo . $check . "<EOT>";
				}
				if ($operand eq "p") {
					delete $hash{$lastpolldata};
					$memreceive{$check} = "<SOH>" . $checkinfo . $check . "<EOT>";
					$lastpolldata = $check;
				}
				if ($operand eq "s") {
					delete $hash{$laststatusdata};
					$memreceive{$check} = "<SOH>" . $checkinfo . $check . "<EOT>";
					$laststatusdata = $check;
				}
=cut

	## monitor
				if ($monitor) {
					if ($ShowBlock) {
						my $logtxt = $Block . "\n";
						logprint ($logtxt);
					}
					if (ord $4 > 95) {
						if (substr ($4, 0, 1) eq "p") {
							my $lastblockvalue = ord (substr ($5, 0, 1)) - 32;
							my $goodblockvalue = ord (substr ($5, 1, 1)) - 32;
							my $endblockvalue = ord (substr ($5, 2, 1)) - 32;
							my $missingvalue = substr ($5, 3);

							my $logtxt = sprintf ( "> Poll   : last=%d good=%d end=%d missing=%s\n",
								$lastblockvalue, $goodblockvalue, $endblockvalue, $missingvalue);
							logprint ($logtxt);
						}
						if (substr ($4, 0, 1) eq "s") {
							my $lastblockvalue = ord (substr ($5, 0, 1)) - 32;
							my $goodblockvalue = ord (substr ($5, 1, 1)) - 32;
							my $endblockvalue = ord (substr ($5, 2, 1)) - 32;
							my $missingvalue = substr ($5, 3);

							my $logtxt = sprintf ( "> Status : last=%d good=%d end=%d missing=%s\n",
								$lastblockvalue, $goodblockvalue, $endblockvalue, $missingvalue);
							logprint ($logtxt);
						}
						if (substr ($4, 0, 1) eq "a") {
							my $logtxt = sprintf ( "> Abort \n\n");
							logprint ($logtxt);
						}
						if (substr ($4, 0, 1) eq "c") {
							my $logtxt = sprintf ( "> Connect: %s\n\n",  $5);
							logprint ($logtxt);
						}
						if (substr ($4, 0, 1) eq "d") {
							my $logtxt = sprintf ( "> Disconnect\n\n");
							logprint ($logtxt);
						}
						if (substr ($4, 0, 1) eq "i") {
							my $logtxt = sprintf ( "> Ident  : %s\n\n", $5);
							logprint ($logtxt);
						}
						if (substr ($4, 0, 1) eq "u") {
							my $logtxt = sprintf ( "> Unproto: %s\n\n", $5);
							logprint ($logtxt);
						}
					} else {
						my $zahl = (ord $4) - 32;
						my $logtxt = sprintf ( "> Data: %2d\n%s\n", $zahl, $5);
						logprint ($logtxt);
					}
				}	# end monitor

				if ($closure eq "<EOT>") {

					$ServerStatus = "EOT";
					$ReceivedLastBlock = 1;
				}

				if ($Iamserver == 1) {
					open SESSION, "$ENV{HOME}/.pskmail/PSKmailsession";
					my $session = <SESSION>;
					chomp $session;
					close SESSION;
					if($session eq "none") {
						if ($operand eq "a" ||
							$operand eq "s" ||
							$operand eq "p" ||
							$operand eq "d" ||
							$operand eq "i" ) {
							return "";
						}

					}
				}

	## abort
				if ($operand eq 'a') {		#t.b.d.
					$ServerStatus = "Abort";
					return $TextFile;
	## unproto services
				} elsif ($operand eq 'u'){
						if ($payload =~ /(.*):(\d+) (.*)/s) {
		## ping service

							if ($2 == 72) {
								log_ping($1,$2);
							}
							elsif ($2 == 7 || $2 == 71) {
								log_ping($1,$2);
								if ($Iamserver == 1) {
									if ($2 == 7) {
										sleep ($Pingdelay);
										$AutoTune = 1;	# autotune if required
										sleep (int (rand(10)));
										$TXServerStatus = "TXPing";
										send_frame();
										$payload = "";
									}
								}
							}
							if ($2 == 25) {			# unproto email
									log_ping($1,$2);
								if ($Iamserver == 1) {
									$AutoTune = 1;	# autotune if required
									$RxTextQueue .= "~MSG " . $1 . " " . $3 . "\n";
									$TXServerStatus = "TXPing";
									sleep (int (rand(10)));
									send_frame();
									$payload = "";
								}
							}
							if ($2 == 26) {			# aprs message
								if ($Iamserver == 1) {
									my $cont = $3;
									if (index ($cont, ">PSKAPR*::") < 0) {
										if ($rigptttune == 1) {
											$AutoTune = 1;	# autotune if required
											ptttune();
										}
										my $QSL = sprintf("%cQSL %s de %s",0x01, $1, $ServerCall);
										my $qslcheck = checksum ($QSL);
										$QSL .= " " . $qslcheck . $eot;

										sleep (int (rand(10)));

										sendit ($QSL . $eot);

										my $msgcall = $1;
										aprs_serv($1, $3); # $1 = Call, $3 = Message
										log_ping($msgcall,$2);
									}
								} else {
									aprs_client($1, $3);
									log_ping($msgcall,$2);
								}
								$payload = "";
							}

				 		} elsif ($Iamserver == 1 && $payload =~ /(\S+)><(\S+)/){ # APRS link request
				 			# add station to owned_list
				 			if ($2 eq $ServerCall) {
								$Owned_list{$1} = time();
								logprint ("Added $1 to list\n");

								# send APRS-IS confirm
								$MSG = "$ServerCall>PSKAPR,TCPIP*::PSKAPR   :GATING $1";
								aprs_send($MSG);

								# add to routing table
								print "$1->$2\n";
								my $time = time();
								my $pre = $2 . " " . $time . "\n";
								$Routes{$1} = $pre;
								my $ro = "";
								open (ROUTES, ">$ENV{HOME}/.pskmail/routes");
								foreach $key (keys %Routes) {
									$value = $Routes{$key};
									$ro = $ro . $key . " " . $value . "\n";
								}
#								print $ro;
								print ROUTES $ro;
								close (ROUTES);
								# tell the station we got him...
								$Call = $1;
								set_txstatus("TXlinkack");
								send_frame();
								# add call to data base

				 			}


				 		} elsif ($Iamserver == 0 && $payload =~ /(\w*)<>(\w*)/){		# APRS link ack
							# set linked flag (tbd)
						}
	## connect request ('PA0R:1024 PI4TUE:24 4')
				} elsif ($operand eq 'c') {

					if ($payload =~ ?(\w\w\w)(\w\w\w)(\d)?s) {
						my $longcall = "";
						my $longserver = "";
						my $init_length = $3;
						while (($shortinx, $shortval) = each (%Callindex)) {
#print "$shortinx, $shortval\n";
							if ($1 eq $shortval) {
								$longcall = $shortinx;
							}
							if ($2 eq $shortval) {
								$longserver = $shortinx;
							}
#							if ($longserver && $longcall) {
#								last;
#							}
						}
			print "$longcall, $longserver\n";
						$payload = $longcall . ":1024 " . $longserver . ":24 " . $init_length;
					}
					if ($payload =~ /(.*):(.*)\s(.*):(.*)\s(.)/s) {
						if ($debug == 2) {
							print $1, "\n";
							print $2, "\n";
							print $3, "\n";
							print $4, "\n";
							print $5, "\n";
						}

						if ($Iamserver) {

							if ($3 ne $ServerCall) {
								logprint ("Not my call...\n");
								return($TextFile);
							} else{
								#check if we are connected already
								open SESSIONDATA, "$ENV{HOME}/.pskmail/PSKmailsession";
								my $session = <SESSIONDATA>;
								chomp $session;
								close SESSIONDATA;

								if ($session ne "beacon" && $session ne "none" && $session ne $1) {
									logprint ("Already connected to $1, $session\n");
									return($TextFile);
								}

								$AutoTune = 1;	# initiate an autotune if required
								newsession();
								$ServerStatus = "Connect_req";
								$Call = $1;				# set the call
								$ClientCall = $1;
								log_ping($Call, $2);		# > mheard list
								my $Maxlen = (ord $5) - 48;
#								if ($Maxlen <= $Blockindex) {
									$Blockindex = $Maxlen;	# set max blocklength
									$BlockLengthStr= sprintf("%c", $Blockindex + 48);
									$BlockLength = (2 ** $Blockindex) ;
									$Modem = getspeed();
									if ($Modem eq "PSK250") {
										$max_idle = 4;
									} elsif ($Modem eq "PSK125"){
										$max_idle = 8;
									} elsif ($Modem eq "PSK63"){
										$max_idle = 17;
									} else {
										$max_idle = 25;
									}

									# todo: setup ports
#								}
								if ($debug == 2) {
									logprint ($ServerStatus . "\n");
									logprint ("Called by $ClientCall\n");
									logprint ("Max block length = $5\n");
								}
							}
						} else {	# I am a client
								log_ping($Call, $2);		# > mheard list
							if ($3 ne $options[0]) {
								logprint ("Not my call...\n");
								return($TextFile);
							} else {
				# client connect request
								$CallerCall = $1;
								$Mode = $2;

								$Moderequest = $4;

								if ($Moderequest == 87) {
									$ServerStatus ="TTY_req";
									`echo "$CallerCall" > $ENV{HOME}/.pskmail/.mastercall`;

								} else {
									$ServerStatus = "Connect_req";
								}

								$Maxlen = (ord $5) - 48;

								$Blockindex = $Maxlen;	# set max blocklength
								$BlockLengthStr= sprintf("%c", $Blockindex + 48);
								$BlockLength = (2 ** $Blockindex) ;

								$Modem = getspeed();
								if ($Modem eq "PSK63") {
									$max_idle = 17;
								} elsif ($Modem eq "PSK125") {
									$max_idle = 8;
								} elsif ($Modem eq "PSK250") {
									$max_idle = 8;
								} else {
									$max_idle = 25;
								}

								if ($debug == 2) {
									logprint ($ServerStatus . "\n");
									logprint ("Called by $CallerCall\n");
									logprint ("Max block length = $5\n");
								}

							}
						}

					}
	## Connect acknowledge	(PI4TUE:24 PA0R:1024 4)
				} elsif ($operand eq 'k') {
					if ($payload =~ ?(\w\w\w)(\w\w\w)(\d)?s) {
						my $longcall = "";
						my $longserver = "";
						my $init_length = $3;
						while (($shortinx, $shortval) = each (%Callindex)) {
							if ($2 eq $shortval) {
								$longcall = $shortinx;
							}
							if ($1 eq $shortval) {
								$longserver = $shortinx;
							}
							if ($longserver && $longcall) {
								last;
							}
						}
						$payload = $longserver . ":24 " . $longcall . ":1024 " . $init_length;
					}

					if ($Iamserver == 0 && $payload =~ /.*\s(.*):\d+\s\d/ && $1 eq $options[0]){ #my  call
						$Streamid = $Current_session unless $Iamserver;
						$Idle_counter = 0;
						$ServerStatus = "Connect_ack";

						if ($payload =~ /(.*)\s.*\s(.)/s){
							$Maxlen = ord $2 - 48;
							if ($Maxlen <= $Blockindex) {
								$Blockindex = $Maxlen;	# set max blocklength
							}


							my $pingcall = $1;
							$pingcall =~ /(.*):(\d*)/;
							log_ping ($1, $2);
						}
						return $TextFile;
					}
	## disconnect
				} elsif ($operand eq 'd' && $Current_session eq $Streamid) {
					$ServerStatus = "Disconnect_req";
					if ($Iamserver == 0) {
###try to kill the session#######################################
						open CLIENTOUT, ">>$ENV{HOME}/.pskmail/clientout";
						print CLIENTOUT "\n==Disconnected", "\n";
						close CLIENTOUT;
						open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
#						print SESSIONDATA $nosession;
						print SESSIONDATA "none";
						close SESSIONDATA;
						set_txstatus("TXDisconnect");
						send_frame();
						$mystring = "";
						$session = $nosession;
						`killall rflinkclient.pl`;
#################################################################

					}
					return $TextFile;
	## status block ('abcdef')
				} elsif ($operand eq 's' && $Current_session eq $Streamid) {
					if ($payload =~ /(.)(.)(.)(.*)/s) {
						$HisLastblock = ord ($1) - 32;
						my $HisGoodblockchar = $2;
						my $HisEndblockchar = $3;
						$HisMissString = $4;

						if ($HisGoodblockchar eq $HisEndblockchar){
							$HisMissString = "";
						}

						set_blocklength ($HisMissString);

						@HisMissing = split('', $HisMissString);

						$ServerStatus = "Status_rx";

						$HisGoodblock = (ord $HisGoodblockchar) - 32;
						$HisEndblock = (ord $HisEndblockchar) - 32;

						############### purge send queue #############
						my $ij;
						my $jj;
						for ($ij = $HisGoodblock; $ij > ($HisGoodblock - 17); $ij--) {
							if ($ij < 0) {
								$jj =$ij + 64;
							} else {
								$jj = $ij;
							}
							$Sendqueue[$jj] = "";
						}


						##############################################

						if ($debug == 2) {
							print $HisLastblock, "\n";
							print $HisGoodblock, "\n";
							print $HisEndblock, "\n";
							print $HisMissString, "\n";
						}

						handle_rxqueue();

						$ReceivedlastBlock = 1;

						return $TextFile;
					}
	## poll block ('abcdef')
				} elsif ($operand eq 'p' && $Current_session eq $Streamid) {
					if ($payload =~ /(.)(.)(.)(.*)/s) {
						$HisLastblock = ord($1) - 32;
						$HisGoodblock = ord($2) -32;
						$HisEndblock = ord($3) - 32;
						$HisMissString = $4;

						set_blocklength ($HisMissString);

						@HisMissing = split('', $HisMissString);
						$ServerStatus = "Poll_rx";

						if ($debug == 2) {
							print $HisLastblock, "\n";
							print $HisGoodblock, "\n";
							print $HisEndblock, "\n";
							if ($HisMissingString) {
								print $HisMissingString, "\n";
							}
						}

						handle_rxqueue();

						$ReceivedlastBlock = 1;

						return $TextFile;
					}
## data block
				} else {
					if ($Current_session ne $Streamid) {
						return $TextFile;
					}
					$ServerStatus = "Data";
					my $Current = (ord $operand) - 32;

					if ($debug == 2) {		##debug
						printf ("Current=%d\n", $Current);
						printf ("Payload=%s\n", $payload);
					}

					$payloadlength *= 7;
					$payloadlength += length ($payload);
					$payloadlength /= 8;
					if ($Iamserver) {
						$Modem = getspeed();
						if ($payloadlength > 32) {
							if ($Modem eq "PSK63") {
								$max_idle = 16;
							} elsif ($Modem eq "PSK125") {
								$max_idle = 8;
							} elsif ($Modem eq "PSK250") {
								$max_idle = 4;
							} else {
								$max_idle = 27;
							}
						} elsif ($payloadlength > 16) {
							if ($Modem eq "PSK63") {
								$max_idle = 10;
							} elsif ($Modem eq "PSK125") {
								$max_idle = 5;
							} elsif ($Modem eq "PSK250") {
								$max_idle = 3;
							} else {
								$max_idle = 15;
							}
						} elsif ($payloadlength > 8) {
							if ($Modem eq "PSK63") {
								$max_idle = 8;
							} elsif ($Modem eq "PSK125") {
								$max_idle =4;
							} elsif ($Modem eq "PSK250") {
								$max_idle =2;
							} else {
								$max_idle = 12;
							}
						} else {
							if ($Modem eq "PSK63") {
								$max_idle = 7;
							} elsif ($Modem eq "PSK125") {
								$max_idle = 3;
							} elsif ($Modem eq "PSK250") {
								$max_idle = 2;
							} else {
								$max_idle = 10;
							}
						}
						if ($Modem eq "PSK63" ) {
							$max_idle = 17;
						} elsif ($Modem eq "PSK125") {
							$max_idle = 8;
						} elsif ($Modem eq "PSK250") {
							$max_idle = 4;
						} else {
							$max_idle = 25;
						}
					} else {
						$Modem = getspeed();
						if ($payloadlength > 32) {
							if ($Modem eq "PSK63") {
								$max_idle = 12;
							} elsif ($Modem eq "PSK125") {
								$max_idle = 6;
							} elsif ($Modem eq "PSK250") {
								$max_idle = 3;
							} else {
								$max_idle = 18;
							}
						} elsif ($payloadlength > 16) {
							if ($Modem eq "PSK63") {
								$max_idle = 8;
							} elsif ($Modem eq "PSK125") {
								$max_idle = 4;
							} elsif ($Modem eq "PSK250") {
								$max_idle = 2;
							} else {
								$max_idle = 12;
							}
						} elsif ($payloadlength > 8) {
							if ($Mode eq "PSK63") {
								$max_idle = 6;
							} elsif ($Modem eq "PSK125") {
								$max_idle = 3;
							} elsif ($Modem eq "PSK250") {
								$max_idle = 2;
							} else {
								$max_idle = 9;
							}
						} else {
							if ($Modem eq "PSK63") {
								$max_idle = 5;
							} elsif ($Modem eq "PSK125") {
								$max_idle = 3;
							} elsif ($Modem eq "PSK250") {
								$max_idle = 2;
							} else {
								$max_idle = 8;
							}
						}
					}

#		print "Blocklength= ", $payloadlength, "\n";

					$ReceiveQueue[$Current] = $payload;

					if ($debug == 2) {			##debug
						for (my $ij = 0; $ij < $Bufferlength ; $ij++) {
							if ($ReceiveQueue[$ij]) {
								printf ("%d-%s\n", $ij, $ReceiveQueue[$ij]);
							}
						}
					}

				}
			} else {	# we received <EOT> but no valid status, so poll for a repeat...
				if ($closure eq "<EOT>" && $Current_session eq $Streamid) {
					open SESSION, "$ENV{HOME}/.pskmail/PSKmailsession";
					my $session = <SESSION>;
					chomp $session;
					close SESSION;
					if ($session ne "none" && $session ne "beacon") {
						# send poll block
						$Serverstatus = "Poll_rx";
#						set_txstatus("TXPoll");
#						send_frame("");
					}
				}
			} # end checksum

		}
#	} # end while
return $TextFile;
}

#####################################################################
sub gettxinput {
#####################################################################

if (-s "$ENV{HOME}/.pskmail/TxInputfile") {
	my $txinfile = `cat $ENV{HOME}/.pskmail/TxInputfile`;
	unlink "$ENV{HOME}/.pskmail/TxInputfile";
print "::$txinfile\n";
	my @strcharacters = split //, $txinfile;

	foreach my $badchar (@strcharacters) {
			if (ord ($badchar) < 5) {
				$badchar = sprintf ("|Ctl-%c|", ord ("A") + ord	($badchar));
			}
	}

	$txinfile = join "", @strcharacters;

	$txinfile =~ s/=?ISO-8859-1?Q?//g ;				# get rid of this strange stuff

	while($txinfile =~ /=([89ABCDEF][0-9A-F])/) { 	#quoted printables?
			$zahl = hex($1);
			$zahlstring = sprintf("&%d;", $zahl);	# use html coding of ISO 8859-1
			reset
			$txinfile =~ s/=[89ABCDEF][0-9A-F]/$zahlstring/ ;
	}

	$txinfile =~ s/<SOH>/<SOHSign>/g;
	$txinfile =~ s/<EOT>/<EOTSign>/g;

	my @characters = split (//, $txinfile);		# anything left?
	foreach my $char (@characters) {
		if (ord ($char) > 127) {
			$char = "&" . ord ($char) . ";";	# html encoding of ISO 8859-1
		}
	}

	$txinfile = (join "", @characters);

	$txinfile =~ tr/\200-\377/./;	# just in case....

#print "$txinfile\n";

	return $txinfile;
}

Queue_txdata("");

return "";

}
#########################################################
sub listening {
#########################################################
#		my $string = getinput();
my $debug = 0;
my $string = "";
my $dummy;
#my $bigearreader = shift @_;

	if ($debug == 1) {	##DEBUG

								my $debugv = get_idle();

								open (DOUT, ">>" , "testtxt");
								print DOUT ">>>>", "Listening,$debugv", "\n";
								close DOUT;
	}					##DEBUG end

	if (-e "$ENV{HOME}/.pskmail/.input") {
		open (INPT, "<", "$ENV{HOME}/.pskmail/.input");
		@instring = <INPT>;
		close INPT;
		unlink "$ENV{HOME}/.pskmail/.input";
		$string = join "", @instring;

		if ($string =~ /<EOT>/) {
			if (-e "$ENV{HOME}/.pskmail/squelch.lk"){
				unlink "$ENV{HOME}/.pskmail/squelch.lk";
			}
		}

		if ($debug == 2) {	##DEBUG
								open (DOUT, ">>" , "testtxt");
								print DOUT ">>>>", $string, "\n";
								close DOUT;
		}					##DEBUG end

	} else {
=head1
		if (-e "$ENV{HOME}/.pskmail/.bigearout") {
				my $bigearline = `cat $ENV{HOME}/.pskmail/.bigearout`;
				`rm $ENV{HOME}/.pskmail/.bigearout`;
				if ($bigearline) {
					if ($bigearline !~ /<SOH>00u\S+:\d+\s/) {

						print "ARQ:", $bigearline;
					}
				}
				$bigearline = "";
		} else {
			return;
		}
=cut		# BigEar experimental, removed form main stream
		return;
	}
		$TextFromFile .= $string;

		my $count = 0;

		$dummy = unframe($string);


		return;
}

#####################################################################
sub initialize {
#####################################################################

	$Call = $ClientCall;

	for (my $i = 0; $i < $Bufferlength; $i++) {
		$ReceiveQueue[$i] = "";
	}

#	sysopen (INFH, $Inputfile, O_NONBLOCK|O_RDWR);

}

#####################################################################
sub get_rxstatus {
#####################################################################

	return $ServerStatus;
}
#####################################################################
sub reset_rxstatus {
#####################################################################

	$ServerStatus = "Listening";
}
#####################################################################
sub get_call {
#####################################################################

	return $ClientCall;
}

#####################################################################
sub set_txstatus {
#####################################################################

	$TXServerStatus = shift @_;

}
#####################################################################
sub set_connectstatus {
#####################################################################

	$ConnectedFlag = "Connected";

}
#####################################################################
sub reset_connectstatus {
#####################################################################

	$ConnectedFlag = "Disconnected";

}
#####################################################################
sub get_connectstatus {
#####################################################################

	return $ConnectedFlag ;

}

#####################################################################
sub reset_arq  {
#####################################################################
$Firstsent = 0;
$Lastblock = 0;
$Endblock = 0;
$Goodblock = 0;
$Lastqueued = 0;
@Missing = ();
$MissString = "";
$HisGoodblock = 0;		# Other station's Good block
$HisLastblock = 0;		# Other station's Block last sent
$HisEndblock = 0;		# Other station's last received block
@HisMissing = ();		# Other station's missing blocks
@SendQueue = ();
$TxTextQueue = "";
$RxTextQueue = "";
@ReceiveQueue = ();
$Idle_counter = 0;		# seconds from last <SOH>
$Connect_time = 0;		# connect time in seconds
$Interval_time = 0;		# 500 seconds interval
if (-e $TxInputfile) { unlink $TxInputfile; }

}

#####################################################################
sub get_rxqueue  {
#####################################################################

return $RxTextQueue;

}

#####################################################################
sub reset_rxqueue  {
#####################################################################

	$RxTextQueue = "";
}

#####################################################################
sub get_rxqueue_status  {
#####################################################################
my @outlist = ();
	push @outlist, $Lastblock;
	push @outlist, $Endblock;
	push @outlist, $Goodblock;
	push @outlist, $MissString;
	return @outlist;

}
#####################################################################
sub check_lastblock  {
#####################################################################

	my $flag = $ReceivedLastBlock;
	$ReceivedLastBlock = 0;
	return $flag;
}

#####################################################################
sub inc_idle  {
#####################################################################
sleep (1);
if ($Iamserver && -e "$ENV{HOME}/.pskmail/.tx.lck") {
	$Idle_counter = 0;
} else {
	$Idle_counter++;
}
$Connect_time++;
$Interval_time++;

}

#####################################################################
sub get_idle  {
#####################################################################

return $Idle_counter;

}
#####################################################################
sub get_maxidle  {
#####################################################################

return $max_idle;
}

#####################################################################
sub reset_idle  {
#####################################################################

$Idle_counter = 0;

}

#####################################################################
sub get_connect_time  {
#####################################################################

return $Connect_time;

}
#####################################################################
sub get_sendqueue  {
#####################################################################

	my $qlength = $Lastqueued - $HisGoodblock;

	if ($qlength < 0) { $qlength += $Bufferlength };

return $qlength;

}

#####################################################################
sub set_blocklength {
#####################################################################
my $errors = shift @_;
my $trouble = length $errors;

############### constants.... ##############
=header
my $LQ_down = 1;
my $LQ_up1 = 2;
my $LQ_up2 = 3;
my $LQ_up3 = 4;
my $LQ_preset = 2;
=cut
my $LQ_down = 1;
my $LQ_up1 = 0;
my $LQ_up2 = 1;
my $LQ_up3 = 2;
my $LQ_preset = 2;


my $Min_index = 3;
my $Max_index = 6;
############################################

	if ($Lastqueued == $HisGoodblock) { return };


	if ($trouble == 0) {
			$linkquality -= $LQ_down;
			if ($Blockindex == 3) {
				$linkquality -= $LQ_down;
			}
	} elsif ($trouble == 1) {
			$linkquality += $LQ_up1;
	} elsif ($trouble == 2) {
			$linkquality += $LQ_up2;
	} else {
			$linkquality += $LQ_up3;
	}

	if ($linkquality < 0) {
		$linkquality = 0;
	}
	if ($linkquality > 9) {
		$linkquality = 9;
	}

	if ($linkquality > 6) {
		if ($Blockindex > $Min_index) {	# minimum 16 chars
			$Blockindex--;
			$linkquality = $LQ_preset;
		}
	} elsif ($linkquality < 1) {
		if ($Blockindex < $Max_index) {	# maximum 128 chars
			$Blockindex++;
			$linkquality = $LQ_preset;
		}
	}

	$BlockLength = (2 ** $Blockindex) ;
}

################################################################
sub send_ping {
################################################################

$TXServerStatus = "TXPing";
send_frame();

}
################################################################
sub send_uimessage {
################################################################
my $message = shift @_;
$TXServerStatus = "TXUImessage";
send_frame($message);

}
################################################################
sub send_aprsmessage {
################################################################
my $message = shift @_;
$TXServerStatus = "TXaprsmessage";
send_frame($message);

}
################################################################
sub send_linkreq {
################################################################

$TXServerStatus = "TXlinkreq";
send_frame();

}
################################################################
sub get_positmsg {
################################################################

return $positmessage;

}

################################################################
sub log_ping {
################################################################
my $pingcall = shift @_;
my $service = shift @_;
#if ($service =~ /\d*/) { $service = $1;}
$pingcall .= ":";
$pingcall .= $service;

if (exists($pingdb{$pingcall})) {
	my $pingvalue = $pingdb{$pingcall};
	my @values = split ",", $pingvalue;

	$values[1]++;
	$pingdb{$pingcall} = sprintf "%s,%d", getdate(), $values[1];
} else {
	$pingdb{$pingcall} = sprintf "%s,%d", getdate(), 1;
}

if ($Iamserver) {
	open ($lg, ">$ENV{HOME}/.pskmail/pskdownload/pings.log");
	print $lg $Startmessage;
	while ((my $key, my $pvalue) = each (%pingdb)) {
		my $logcall = substr ($key . "        ", 0, 13);
		$pvalue =~ s/,/ - /;
		my $logline = $logcall . $pvalue;
		print $lg $logline, "\n";
	}
	close ($lg);
} else {
	open ($lg, ">$ENV{HOME}/.pskmail/pings.log");
	print $lg $Startmessage;
	while ((my $key, my $pvalue) = each (%pingdb)) {
		my $logcall = substr ($key . "        ", 0, 13);
		$pvalue =~ s/,/ - /;
		my $logline = $logcall . $pvalue;
		print $lg $logline, "\n";
	}
	close ($lg);

}
}

################################################################
sub getgmt {
################################################################
my @array_month = qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
my @array_week = qw(Sun Mon Tue Wed Thu Fri Sat);
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = gmtime time;
my $fixyear = 1900 + $year;
my $abbmonth = $array_month[$mon];
my $abbweek = $array_week[$wday];
my $fixmday = substr(" " . $mday, -2);
my $fixhr = substr("0" . $hour, -2);
my $fixmin = substr("0" . $min, -2);
my $fixsec = substr("0" . $sec, -2);
my $outtime = $abbweek . " " . $abbmonth . " " . $fixmday . " " . $fixhr
		. ":" . $fixmin . ":" . $fixsec . " UTC " . $fixyear . "\n";


return $outtime;


}

################################################################
sub getdate {
################################################################
my $timedate = getgmt();
$timedate =~ /(\w*)\s*(\w*)\s*(\d*)\s*(\d\d:\d\d):\d\d\s(.*)\s*(\d\d\d\d)/;
my $outtime = sprintf "%s", "$4 UTC $2-$3-$6";

return $outtime;

}
################################################################
sub getlocale {
################################################################
	my $txt = shift @_;

	while ($txt =~ /\&(\d\d\d);/) {
		my $char = chr ($1);
		reset
		$txt =~ s/\&\d\d\d;/$char/;
	}
return $txt;
}

################################################################
sub logprint {
################################################################
	my $txt = shift @_;
	my $dt = getdate();
$logfile = "$ENV{HOME}/.pskmail/server.log";
open ($logfh, ">>", $logfile) or die "Cannot open logfile";
flock ($logfh, LOCK_EX);	# lock logfile for write
print $dt . ": " . $txt;
print $logfh $dt . ": " . $txt;
close ($logfh);

}


###############################################################
sub aprs_send {	#message, port
###############################################################
	if ($Aprs_connect == 1) {
		my $MSG = shift @_ ;

		$MSG .= "\n";

		my $aprstime = `date -u +"%d %H:%M"`;
		$aprstime =~ /(\d+)\s+(\d\d:\d\d)/;
#		$aprstime =~ /\w+\s+\w+\s+(\d+)\s+(\d\d:\d\d)/;
		$error = `echo "$1 $2> $MSG" >> $ENV{HOME}/.pskmail/aprslog`;

		open ($fh, ">$ENV{HOME}/.pskmail/.aprsmessage");
		flock ($fh, LOCK_EX);
		print $fh $MSG;
		close ($fh);

	}
}
###############################################################
sub getoptions {
###############################################################
	if ($Iamserver == 0 && -e "$ENV{HOME}/.pskmail/.PSKoptions") {
		open (OPTIONS, "$ENV{HOME}/.pskmail/.PSKoptions") or die "No options file\n";
		@options = <OPTIONS>;
		close (OPTIONS);

		chomp $options[0];
		chomp $options[1];
		chomp $options[2];
		chomp $options[3];
		chomp $options[4];

		$ClientCall = $options[0];
		$Call = $ClientCall;
		$ServerCall = $options[1];
		$Blockindex = $options[2];
		$Latitude = $options[3];
		$Longitude = $options[4];
	}

}

###############################################################
sub checklink {
###############################################################

my $linkcall = shift @_;

	if (exists($Owned_list{$linkcall})) {

		my $linktime = time - $Owned_list{$linkcall};

		if($linktime <= $Maxlinktime ){
				$Owned_list{$linkcall} = time ();
				print "setting link record: $linkcall,$linktime\n";
				return 1;
		} else {
			return 0;
		}
	}
0;
}
###############################################################
sub deletelink {
###############################################################
my $deletecall =    @_;

delete ($Owned_list{$deletecall});
}

###############################################################
sub aprs_serv {
###############################################################
my $AprsCall = shift @_;
my $Message = shift @_;

#	if (checklink ($AprsCall) == 1) {
	if (1){	#always on receive...

		$Call = $AprsCall;

		if (checklink ($AprsCall)) {
			$Owned_list{$AprsCall} = time();
		}

		if ($Message =~ /&&/) {
			if (exists($aprs_store{$AprsCall})) {
				$Message = $aprs_store{$AprsCall};
				my $MSG = $AprsCall . ">PSKAPR,TCPIP*:$Message";
				aprs_send ($MSG);
			}

		} elsif (index ($Message, "!") == 0 ||
			index ($Message, "@") == 0 ||
			index ($Message, "=") == 0 ) {

			my $MSG = $AprsCall . ">PSKAPR,TCPIP*:$Message";
			aprs_send ($MSG);
			$aprs_store{$AprsCall} = $Message;
			if (checklink ($AprsCall)) {
				$Owned_list{$AprsCall} = time();
			}

		} elsif ($Message =~ m/(\w+\-*\d*)\s+(.*)/) {
			my $To_call = $1;
			my $message = $2;

			if ($message !~ />PSKAPR/) {

			if (checklink ($To_call)) {
				$Owned_list{$To_call} = time();
			}

				if (checklink($To_call)) {
					if (substr($message, 0, 0) ne ":"){
						$To_call .= "     ";
						$To_call = substr ($To_call , 0, 9);
						$message = $AprsCall . ">PSKAPR*::$To_call:$message";
						$TXServerStatus = "TXaprsmessage" ;
						send_frame( $message);
					}
				} else {

					$To_call .= "     ";
					$To_call = substr ($To_call , 0, 9);
					my $MSG = $AprsCall . ">PSKAPR,TCPIP*::$To_call:$2" ;
					if (substr($2, 0, 0) ne ":"){
						aprs_send ($MSG) ;
					}
				}
			} # end if

		}

	} else {
#		logprint ("$AprsCall not linked...\n");

	}
}

###############################################################
sub aprs_client {
###############################################################
my ($Call, $Message) = @_;
#":PE1FTV   :Message test...."
#":PE1FTV   :Message test....{003"
#":PE1FTV   :ack003"
#":PE1FTV   :rej003"
#":BLN1     :Bulletin...."
#":BLNA     :Announcement...."
#":BLN3WXGRP:Group bulletin...."
#PE1FTV>PSKAPR::PA0R :ack31
#PE1FTV>PSKAPR::PA0R :[AA] Sorry ... out of office .. 73, Ad{49

my $msgdate = getdate();
$msgdate =~ /(\d*:\d*)\s.*/;
$msgdate = $1;
my $displaymessage;
my $To;
my $From;
my $Aprsmessage;

	if ($Message =~ m/(\w*\-*\d*).*::(\w*\-*\d*)\s*:(.*)/) {
		$Message .= "\n";
		$From = $1;
		$To = $2;
		$Aprsmessage = $3;
		$displaymessage = $msgdate . "-" . $1 . "-" . $3;

		#`echo ">$Message >> $ENV{HOME}/.pskmail/clientout`;
		if ($To eq $ClientCall || $To eq "APRPACK  ") {

			if ($Message =~ m/:ack\d*/) {
				# don't display acks
			} elsif ($Message =~ m/(\w*\-*\d*).*{(\d*|\w)/ ) {

				my $MSG = "$1 ack$2\n";

				$TXServerStatus = "TXaprsmessage";
				send_frame($MSG);

				`echo $displaymessage >> $ENV{HOME}/.pskmail/clientout`;
			} else {

				`echo $displaymessage >> $ENV{HOME}/.pskmail/clientout`;
			}
	} elsif ($Message =~ m/(\w*\-*\d*)\s(.*)/) {
		$To = $1;
		if ($To eq $ClientCall || $To eq "APRPACK  ") { # only messages to myself
				$displaymessage = $msgdate . "-" . $1 . "-" . $2;
				`echo $displaymessage >> $ENV{HOME}/.pskmail/clientout`;
		}
	} else {
		# don't know what to do.
	}
}

}
###############################################################
sub aprs_connect {
###############################################################
my ($host_out, $port_out) = @_;
my $handle_out;

   # create a tcp connection to the specified host and port
	eval {
		$handle_out = IO::Socket::INET->new(Proto     => "tcp",
	                                    PeerAddr  => $host_out,
	                                    PeerPort  => $port_out)
	           or die "can't connect to port $port_out on $host_out: $!";

	};

	if ($@) {
		logprint ($@)  ;
	} else {
		logprint ("Connected to $host_out:$port_out\n");
	}
	return $handle_out;
}


###############################################################
# dohash
#
# Steve Dimse, K4HG, released this algorithm to the public domain
# April 11, 2000.  He posted it to the APRSSIG mailing list in
# the form of C source code.
#
# This function takes a callsign as input and returns the password.
# SSID is stripped from the callsign before computing the password,
# so any SSID will result in the same password.
#
#
sub dohash
{
  my $call = $_[0];

  my $kKey = 0x73e2;

  my $short_call = $call;
  $short_call =~ tr/a-z/A-Z/;           # Convert to uppercase
  $short_call =~ s/(\w+)\-*.*/$1/g;     # If SSID, remove it

  my $hash = $kKey;                     # Initialize with the key value

  my $i = 0;
  my $len = length( $short_call );
  $short_call = $short_call . "\0";     # Add 0x00 to make sure we don't run off the end

  while ($i < $len)                     # Loop through the string two bytes at a time
  {
    my $char = substr($short_call,$i,1);
    #printf( "%s\n", $char );

    $hash = $hash ^ ( ord($char) <<8 ); # Xor high byte with accumulated hash
    $i++;

    $char = substr($short_call,$i,1);
    #printf( "%s\n", $char );

    $hash = $hash ^ ord( $char );       # Xor low byte with accumulated hash
    $i++;
  }
  $hash = $hash & 0x7fff;               # Mask off the MSB so number is always positive
#  print "Hash = $hash\n";
  return($hash);
}

#########################################################
sub filter_aprs {
#########################################################
my $line = shift @_;

#00uPI4TUE:26 PA0R>PSKAPR*::PA0R     :test{087852
#
my $pre;

#DJ0LN>APRS,TCPIP*,qAC,THIRD::PA0R :3rd message test
#print "FIL0:", $line, "\n";

		if (index ($line, "PSKAPR") > 0) {# server->server
#			print "FIL::". $line, "\n";
			if ($line =~ /^(\S+)>.*GATING\s(\S+)/) {
				print "$2->$1\n";
				my $time = time();
				$pre = $1 . " " . $time . "\n";
				$Routes{$2} = $pre;
				my $ro = "";
				open (ROUTES, ">$ENV{HOME}/.pskmail/routes");
				foreach $key (keys %Routes) {
					$value = $Routes{$key};
					$ro = $ro . $key . " " . $value . "\n";
				}
#				print $ro;
				print ROUTES $ro;
				close (ROUTES);
			}


		}
		if ($line =~ /::(\S+)\s*:/) {
#				print "FIL1:", $1, " " ,$line, "\n";
				if (exists ($Routes{$1})) {
					print "FIL2:", $line, "\n";
					return ($line) unless $line =~ /::\S+\s*::/;
				}
		}

		return "";

}
###############################################################
sub handle_aprs {
###############################################################

my $line = shift @_;

if ($line) {
#	print $line;
#	$error = `echo "< $line" >> $ENV{HOME}/.pskmail/aprslog`;
}

my $fromcall ;
my $groupcall;
my $path;
my $type;
my $tocall;
my $message;
my $mesgnumber;

	my @tailout = `tail -n 4 $ENV{HOME}/.pskmail/aprslog | grep GATING`;
	foreach my $rec (@tailout) {
		if (index ($rec, $ServerCall) > 0) {
			$rec =~ /(\d*) (\d\d):(\d\d)> .*GATING\s(\w*)/;
			my $minutes = $3;
			my $hours = $2;
			my $mday = $1;
			my ($mon, $year, $sec) = (localtime)[4,5,0];

			my $gatetime = timelocal ($sec, $minutes, $hours, $mday, $mon, $year);
			my $gatingcall = $4;
			$gatingcall =~ tr/_//;	# remove '_'

			$RFout_list{$gatingcall} = $gatetime;

			push (@prefixes, $gatingcall); # add to prefixlist if needed
			my %seen = ();
			foreach my $ccall (@prefixes) {
				$seen{$ccall}++;
			}
			@prefixes = keys %seen;


		}else {
			if ($rec =~ /< (\w*\d*\w*\-*\d*)>PSKAPR.*GATING\s(\S+)/)        {
				my $gatingcall = $2;
				$gatingcall =~ tr/_//;	# remove '_'
				if (exists ($RFout_list{$gatingcall})) {
					delete $RFout_list{$gatingcall};
					delete $Owned_list{$gatingcall};
				}
			}
     	}
	}

	$line =~ tr/\r\b\f/_/;

		if ($line =~ /^(\w*\-*\d*)>(\w*),*(.*?):GATING (\w*\d*\w*\-*\d*)_*/) {	# Gating message

		if ($1 ne $ServerCall) {
			my $dropcall = $4;
			chop $dropcall ;
			if (exists ($Owned_list{$dropcall})) {
				delete ($Owned_list{$dropcall});
			}
			delete ($RFout_list{$dropcall});

			print "Dropping $dropcall from record\n";
		}
	} elsif ($line =~ /^(\w*\-*\d*)>(\w*),*(.*?):(>|=|!|\?|@)(.*)/) {	# posits, no gating at present
		;	# do nothing (yet)
#		print "Alt data:$line\n";
	} elsif ($line =~ /^(\w*\-*\d*)>(\w*),*(.*?):(.)(\w*\-*\d*)\s*:(.*)(\{*.*)/) {	# message
		$fromcall = $1;
		$groupcall = $2;
		$path = $3;
		$type = $4;
		$tocall = $5;
		$message = $6;
		if  ($message =~ /(.*)(\{\d*)/) {
			$message = $1;
			$mesgnumber = $2;
		} elsif ($message =~ /(.*)(\{\w)/){
			$message = $1;
			$mesgnumber = $2;
		} else {
			$mesgnumber = "";
		}

		if ($type eq ":") {
			if ($message =~ /(ack\d*)/) {		# clean ack message
				$message = $1;
			} elsif ($message =~ /(ack\w)/) {	# clean ack message
				$message = $1;
			}
#			print "From: $fromcall To: $tocall Mesg: $message Nr: $mesgnumber\n"; # gate message > RF

			my $sendmessage = $fromcall . ">PSKAPR::" . $tocall . " :" . $message . $mesgnumber;

			foreach my $messagetime (keys %Messagehash) {	# reset the message array
				if (time - $Messagehash{$messagetime} > 30 ){
					delete $Messagehash{$messagetime};
				}
			}

			my $messagekey = $fromcall . $mesgnumber;

			if (exists($Messagehash{$messagekey})) {
													# send ack via aprs?? how?
			} else {
				$Messagehash{$messagekey} = time;	# add message to table

				if (exists($RFout_list{$tocall})) {
						send_aprsmessage($sendmessage);
						sleep 20;					# allow time for ack...
				}
			}

		} elsif ($type eq "{") {
			if ($message =~ /GATING (\w*\d*\w*\-*\d*)/) {
				my $dropcall = $1;
				$dropcall =~ tr/_//;
				print "Drop link $dropcall\n";
				if (exists ($Owned_link{$dropcall})) {
					delete ($Owned_link{$dropcall}); # drop link
				}
				foreach my $ownedcall (keys %Owned_list) {
					print "OWNED:",$ownedcall, "\n";
				}
			}
		}
	}


}
###############################################################
sub queue_aprs_message {
###############################################################
my $fromcall = shift @_;
my $message = shift @_;
my $mesgnumber = shift @_;

my $mesgkey = $fromcall . $mesgnumber;

# dump key/message into hash
$Messagehash{$mesgkey} = $message;
# add key to FIFO

 }

##############################################################
sub getspeed {
##############################################################
	open (SPD, "$ENV{HOME}/.pskmail/.modem");
	my $speed = <SPD>;
	chomp $speed;
	close (SPD);
	return $speed;
}

##############################################################
sub mkflag {	# closure making simple flags (0/1)
#				use: 	$myflag = mkflag();
#						$yourflag = mkflag();
#						$myflag->{SET}->();
##############################################################
	my $var = 0;
	my $bundle = {
		"SET" => sub { $var = 1 },
		"GET" => sub { return $var },
		"RESET" => sub { $var = 0 },
	};
	return $bundle;
}
###############################################################
sub getshortcalls {
###############################################################


	if (-e "$ENV{HOME}/.pskmail/calls.txt") {

		open (CALLS, "$ENV{HOME}/.pskmail/calls.txt");
		my @indexcalls = <CALLS>;
		close (CALLS);

		foreach my $callinx (@indexcalls) {
		chomp $callinx;
			if ($callinx) {
				my $index =shortcall($callinx);
				$Callindex{$callinx} = $index;
			}
		}
		$proto = 2;
	}
}
###############################################################
sub shortcall {
###############################################################

my $call = shift @_;
my $fingerprint = md5_base64($call);
my $shortprint =  substr($fingerprint, -3);
$shortprint  =~ tr/0123456789\/\+/abcdefghijXX/;

return $shortprint;
}
###############################################################
sub gettxtqueue {
###############################################################
	return length ($TxTextQueue);

}
 ##################################################
 sub getarqconfig {
 ##################################################
 	my @conf = ();
 	my $configdata;
 	if (-e "$ENV{HOME}/.pskmail/.pskmailconf") {
 		open (CONFIG, "$ENV{HOME}/.pskmail/.pskmailconf");
 		my $configdata = <CONFIG>;
 		close (CONFIG);
 		@conf = split (",", $configdata);
# logprint ($configdata, "\n");
 		return $configdata;
 	} else {
 		$conf[0] = 0;
 		$conf[1] = 0;
 		$conf[2] = 0;
 		$conf[3] = "$ENV{HOME}/.pskmail/gMFSK.log";	# Inputfile
 		$conf[4] = "$ENV{HOME}/.pskmail/gmfsk_autofile"; # commandfile
 		$conf[5] = "$ENV{HOME}/.pskmail/server.log";
 		$conf[6] = 16;
 		$conf[7] = 15;
 		$conf[8] = 0;
 		$conf[9] = "@";
 		$conf[10] = "pskmail $Version";
 		$conf[11] = 0;
 		$conf[12] = 20;

 	}
 	return $configdata;
 }


#####################################################################
# Set status for need to handle autotuner
sub set_autotune {
#####################################################################
	$AutoTune = shift @_;
	#print "AutoTune:$AutoTune.\n";
}

###############################################################
# Send a PTTTUNE command to fldigi, this triggers an autotune for
# ICOM autotuners (when the rig is set to "PTT TUNE" /SM0RWO
sub ptttune {
###############################################################
	if ($AutoTune != 0) {
		sendcmd("PTTTUNE");
		sleep (2);
		$AutoTune = 0;
	}
}

################################################
# Send an XML coded command to fldigi, "mode" used here will change
# later when fldigi is done. /SM0RWO
sub sendcmd {
################################################

	my $command =shift @_;
	my $cmdstring = "\<cmd\>\<mode\>" . $command . "\</mode\>\</cmd\>\n\000";

	msgsnd($txid, pack("l! a*", $type_sent, $cmdstring), 0 ) or die "# msgsend failed: $!\n";

	#print $cmdstring;

#	open (CMDOUT, $output);
#	print CMDOUT $cmdstring;
#	close (CMDOUT);
}
################################################
sub getgpstime {
################################################

	my $gpstime = `cat .gps`;
	if ($gpstime =~ /.*\s.*\s.*\s(\d+)/) {
		(my $sec, my $min, my $hr) = gmtime($1 + 1);
		my $out = sprintf("%02s:%02s:%02s", $hr, $min, $sec);
		$out;
	}

}


################################################
sub getgpspos {
################################################
		my $lat;
		my $lon;
		my $gpstime = `cat .gps`;
		if ($gpstime =~ /(\d+)\s(.*)\s(.*)\s\d+/) {
			if ($gpsnr != $1) {
				$gpsnr = $1;
				($lat,$lon) = ($2, $3);
			} else {
				($lat,$lon) = ("Nofix", "Nofix");
			}
		}

}
#################################################
sub point_handler {
#################################################
  my $last_return=shift()||1; #the return from the last call or undef if first
  my $point=shift(); #current point $point->fix is true!
  my $config=shift();
	  if ($point->fix) {
		open (OUTGPS, ">.gps");
	  	print OUTGPS $last_return, " ", $point->lat, " ", $point->lon, " ", $point->time, "\n";
	  	close (OUTGPS);
	  } else {
		open (OUTGPS, ">.gps");
	  	print "No fix\n";
	  	close (OUTGPS);
	  }
  return $last_return + 1; #Return a true scalar type e.g. $a, {}, []
                           #try the interesting return of $point
}

#############################################################
sub sendmode {		# send mode to modem
#############################################################
	my $modemstring = shift @_;

	$modemstring = sprintf("<cmd><mode>%s</mode></cmd>%c",$modemstring, 0x00);
	$type_sent = 1;
	msgsnd($txid, pack("l! a*", $type_sent, $modemstring), 0 ) or die "# msgsend failed: $!\n";

}
#############################################################
sub sendmodemcommand {		# send command to modem
#############################################################
	my $cmdstring = shift @_;

	$cmdstring = sprintf("<cmd>%s</cmd>%c",$cmdstring, 0x00);
	$type_sent = 1;
	msgsnd($txid, pack("l! a*", $type_sent, $cmdstring), 0 ) or die "# msgsend failed: $!\n";

}

###################### END ####################################
1;
