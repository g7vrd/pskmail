#! /bin/bash



#myuser=$1

#if [ -z $1 ]; then
#	echo "I need to know your user"
#	echo "Usage: sudo ./install.sh user"
#	exit
#fi

INSTALLDIR="/usr/local/share/pskmail_server"

PWDIR=$PWD

echo "Install pskmail_server in $INSTALLDIR for user $myuser"

echo "Copying start script to /usr/local/bin"

cp -f -p pskmail_server /usr/local/bin

if [ -d $INSTALLDIR ]; then
	echo "$INSTALLDIR already exists"
else
	mkdir $INSTALLDIR
fi

echo "Copying scripts to $INSTALLDIR"

cp -f rflinkserver.pl $INSTALLDIR
cp -f -p arq.pm $INSTALLDIR
cp -f paq8l $INSTALLDIR
cp -f paq864 $INSTALLDIR
cp -f unpaq864 $INSTALLDIR

if [ -e $HOME/.pskmail/rflinkusers/rflink_users.db ]; then
	cp $HOME/.pskmail/rflinkusers/rflink_users.db $PWDIR/keepdb
fi

if [ -e $HOME/.pskmail/pskmailrc.pl ]; then
	cp $HOME/.pskmail/pskmailrc.pl $PWDIR/keepconfig
fi

if [ -e $HOME/.pskmail/.node_data ]; then
	cp $HOME/.pskmail/.node_data $PWDIR/keepnode
fi

if [ -e $HOME/.pskmail/telnetusers ]; then
	cp $HOME/.pskmail/telnetusers $PWDIR/keepusers
fi


if [ -e $HOME/.pskmail/qrg/freqs.txt ]; then
	cp $HOME/.pskmail/qrg/freqs.txt $PWDIR/keepfreqs
fi

if [ -d $HOME/.pskmail/localmail ]; then
	cp -r $HOME/.pskmail/localmail $PWDIR/keepmail
fi

if [ -d $HOME/.pskmail/pskdownload ]; then
	cp -r $HOME/.pskmail/pskdownload $PWDIR/keepfiles
fi

if [ -e $HOME/.pskmail/routes ]; then
	cp $HOME/.pskmail/routes $PWDIR/keeproutes
fi


rm -r $HOME/.pskmail

echo "Making new ~/.pskmail directory"

cd $HOME

mkdir .pskmail

cd .pskmail

mkdir uploads
mkdir pskdownload
mkdir rflinkusers
mkdir qrg
mkdir localmail
mkdir bulletins

cd $HOME

echo "Setting ownership to $SUDO_USER"


cp -r $PWDIR/rflinkusers $HOME/.pskmail
cp -r $PWDIR/qrg $HOME/.pskmail
cp -f $PWDIR/pskmailrc.pl .pskmail
cp -f $PWDIR/testfile .pskmail
cp -f $PWDIR/crapmail.txt .pskmail
touch .pskmail/aprslog

if [ -e $PWDIR/keepdb ]; then
	cp $PWDIR/keepdb $HOME/.pskmail/rflinkusers/rflink_users.db
	rm $PWDIR/keepdb
	echo "Restoring your old user database"
fi

if [ -e $PWDIR/keepconfig ]; then
	cp $PWDIR/keepconfig $HOME/.pskmail/pskmailrc.pl
	rm $PWDIR/keepconfig
	echo "Restoring your old config file"
fi

if [ -e $PWDIR/keepnode ]; then
	cp $PWDIR/keepnode $HOME/.pskmail/.node_data
	rm $PWDIR/keepnode
	echo "Restoring your old nodes file"
fi

if [ -e $PWDIR/keepusers ]; then
	cp $PWDIR/keepusers $HOME/.pskmail/telnetusers
	rm $PWDIR/keepusers
	echo "Restoring your old telnetusers file"
fi

if [ -e $PWDIR/keepfreqs ]; then
	cp $PWDIR/keepfreqs $HOME/.pskmail/qrg/freqs.txt
	rm $PWDIR/keepfreqs
	echo "Restoring your old freqs file"
fi

if [ -e $PWDIR/keeproutes ]; then
	cp $PWDIR/keeproutes $HOME/.pskmail/routes
	rm $PWDIR/keeproutes
	echo "Restoring your old routes file"
fi

if [ -d $PWDIR/keepmail ]; then
	cp -r $PWDIR/keepmail $HOME/.pskmail/localmail
	rm -r $PWDIR/keepmail
	echo "Restoring your old mail file"
fi

if [ -d $PWDIR/keepfiles ]; then
	cp -r $PWDIR/keepfiles $HOME/.pskmail/pskdownload
	rm -r $PWDIR/keepfiles
	echo "Restoring your old file content"
fi


chown -R $SUDO_USER:$SUDO_USER .pskmail


echo "Pskmail is now ready for use. Please restart the application after the first config."

exit 1
