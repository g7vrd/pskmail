#! /usr/bin/perl -w

# This script gets the time from the internet. 
# Copyright 2008 Rein Couperus PA0R
# This script will only work if you run it as root (you can make a cron job 
# for it under root). For your Timezone edit the n=16 !!

$timefile = `elinks -dump http://www.timeanddate.com/worldclock/city.html?n=16`;

@timearray = split '\n', $timefile;

foreach $timevalue (@timearray) {
#	if ($timevalue =~ /\s\s\s*(\d\d:\d\d:\d\d)/) {
	if ($timevalue =~ /(\d\d:\d\d:\d\d)/) {
		$timeout = $1;
		last;
	}
}

if ($timeout =~ /(\d\d):(\d\d):(\d\d)/) {
	$newhr = $1;
	$newmin = $2;
	$newsec = $3;
} else {
	exit;
}

$datevalue = `date +%m%d`;
chomp $datevalue;

$newtime = $datevalue . $newhr . $newmin . "." . $newsec;
$newtime = "date " . $newtime . "\n";

system ("$newtime");

exit;
