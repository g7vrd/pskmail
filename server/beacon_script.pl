#! /usr/bin/perl -w

# PSKmail beacon script to be called from a cron tab
# will only send beacon if idle (no connect).

my $Beacontext = "\nPI4TUE PSKmail server - rprts to pa0r\@eudxf.org - tnx";

if (-e "PSKmailsession") {
	open (CONN, "PSKmailsession");
	my @stat = <CONN>;
	close (CONN);
	
	if ($stat[0] eq "beacon") {
		`echo "$Beacontext" > gmfsk_autofile`;
	}
}
