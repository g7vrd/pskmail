#! /usr/bin/perl -w

# This module handles events and prints texts, such as error messages,
# to a log file or console.
#

# This program is published under the GPL license.
#   Copyright (C) 2005
#       P�r Crusefalk SM0RWO (per@abo.mine.nu)
# 
# *    logger.pm is free software; you can redistribute it and/or modify
# *    it under the terms of the GNU General Public License as published by
# *    the Free Software Foundation; either version 2 of the License, or
# *    (at your option) any later version.
# *
# *    logger.pm is distributed in the hope that it will be useful,
# *    but WITHOUT ANY WARRANTY; without even the implied warranty of
# *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# *    GNU General Public License for more details.
# *
# *    You should have received a copy of the GNU General Public License
# *    along with this program; if not, write to the Free Software
# *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# local variables
my $logfile = "applog.log";


###########################################################
sub logme		# Main sub for handling log messages
###########################################################
{
	###########################################################
	# Arguments here are: Error message, 
	# severity of this log and
	# optionally if this should result in a messagebox for the operator (TBD)
	# Severity is:
	# 0 = error
	# 1 = warning
	# 2 = info
	# 3 = extra super duper plenty amount of info for debugging. malloc, open file etc... (seldom used).
	# Example: logme($@, 0);
	# Example: logme("Connected to SM0RWO",2);
	# The resulting logfile may look like:
	# 06-01-03 10:00:34 (0) : Failed to open configuration file
	# 06-01-03 10:02:10 (2) : Connected to SM0RWO
	###########################################################

	my($message, $level, $display) = @_; 
	my $noofargs = @_;
	my $logentry ="";
	my $datestr = "";
		
	# There should be something to show, just move on if not
	if ($message ne "")
	{
		
		# Create message text
		$datestr = gettime();			# Date
		$logentry .= $datestr . " ";
		if ($level ne "") {				# Level
			$logentry .= "(" . $level . ") : ";	
		} else {
			$logentry .= "(X) : ";
		}
		
		$logentry .= $message . "\n";			# Message		
	
		# Now we select what to do with it
		if ($display == 1)  { 				# Display a messagebox
			writetolog($logentry);
			messagebox($message); 			# Write stuff to display a messagebox
		} else {
			writetolog($logentry);
		}		
	}
}

###########################################################
sub writetolog 		# Write error message to log file,
			# errors here aren't properly handled
###########################################################
{
	my $logtext = shift @_;

	open(LH, ">>", $logfile);
	flock(LH,2);		# Lock for exclusive write
        print "$logtext";	# Print to console
        print LH "$logtext";	# Print to file
        close LH;		# Close and release lock
}

###########################################################
sub messagebox		# Show messagebox for operator
###########################################################
{
	my $logtext = shift @_;
	# Here's work to do...
}

###########################################################
sub changelogfile		# Change the default log file
				# If this had been a class then this would have
				# been excellent. One object per log...
###########################################################
{
	my $filename = shift @_;
	if ($filename ne "")
	{
		$logfile = $filename;	
	}
}

################################################################
sub gettime {
################################################################
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=gmtime(time);
	my $now = sprintf("%4d-%02d-%02d %02d:%02d:%02d ", $year+1900,$mon+1,$mday,$hour,$min,$sec);
	return $now;
}


###################### END ####################################
