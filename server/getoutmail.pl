#! /usr/bin/perl -w

$test = `ps aux | grep psk_arq | wc`;

$test =~ m/\s*(\d*)\s/;

if ($1 < 3) {
	exit;	# pskmail not active
}

use File::Find;

$inpath = "/var/spool/postfix/deferred";
$outpath = "/home/rein/mail/Outbox/";
$myuser = "rein:users";

sub process_files {
	
	my @outarray= ();
	
	$name = $File::Find::name . "\n" unless -d ;
	if ($name) {
		$filename = $name;
#print "Filename=$filename";
		$shortname = $name;
		$shortname =~ s{^.*/}{};
		$putname = $outpath . $shortname;
		chomp $putname;
#print "Putname=$putname . \n";
		
		open $fhout, ">", "$putname";

#print "Postcatting:$filename";				
		@file = `/usr/sbin/postcat $filename`;
		foreach $line (@file) {		
			if ($line =~ m/^To: /) {
				print $fhout "~SEND\n";
				print $fhout $line;
				print $fhout $keepsubject;
			} elsif ($line =~ m/Subject: /) {
				$keepsubject = $line;
			} elsif ($line =~ m/^\n$/) {
				$body = 1;
			} elsif ($line =~ m/\*{3}\sHEADER\s/) {
				# do nothing
			} elsif ($line =~ m/\*{3}\sMESSAGE\s/) {
				# do nothing
			} else {
				if ($body) {
#					$line = tr/\r/\n/;
					print $fhout $line;
				}
			}
		}
		print $fhout ".\n.\n";
		close ($fhout);
		`chown $myuser $putname`;
		$body = 0;
	}
}
find (\&process_files, $inpath);

`/usr/sbin/postsuper -d ALL`;

exit;
