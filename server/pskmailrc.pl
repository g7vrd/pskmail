#! /usr/bin/perl -w

# Last change: 130209

##########################################################################
# Parameter file fo PSKmail server
##########################################################################

 $Iamserver = 1 ;			# must be 1 for server
 $ServerCall = "N0CAL";			# server needs a callsign!!
 $Pingdelay = 0;
 $Beaconminute = 3;			# Not currently used, to be removed
 @Beaconarray = qw (1 0 0 0 0);		# Experimental, controls beacon behaviour during minute 0-4
 $commandcall = "PA0R";			# Remote control option

 $relay = "mysmtpserver";		# set this to your smtp server, use "smtp.gmail.com" for gmail
 $smtphelo = 'gmail.com';		# used when TLS'ing, necessary for gmail
 $smtpauthlevel = 0;			# MUST SET THIS! 0 = smtp server does not need authentication, 1 = plain auth used, 2 = tls auth used
 $smtptlsuser = "";			# Intended for smtp auth. Will try to use if set. Gmail needs "user\@gmail.com"
 $smtptlspass = "";			# Password for smtp tls
 $smtptlsport = 587;			# Gmail uses non standard ports, seen 465 and 587 so far

 $email_limit = 20000;		# truncates email after xxx characters

####################### SWITCH the monitor on/off ########################
 $monitor = 1;		# text & status
 $ShowBlock = 0;	# raw blocks

#######################The following parameters are normally ok....######
 $debug = 0;
 $Max_retries = 8;	# Max. nr. of polls before disconnect
 $Maxidle = 17;		# Nr. of seconds to wait for next block
 $Maxwebpage = 4000;	# Max. nr. of characters per web page
 $Txdelay = 0 ;		# Extra wait time for TX/RX switching
 $Framelength = 17;	# Nr. of frames in a block
 $nosession = "beacon";

 $dbaddress = "";
 $dbpass = "";
 $dbfile = "$ENV{HOME}/.pskmail/rflinkusers/rflink_users.db";	# users database
 $output = ">$ENV{HOME}/.pskmail/gmfsk_autofile";		# Tx output to gmfsk
 $TxInputfile = "$ENV{HOME}/.pskmail/TxInputfile";				# Tx input for rflinkserver.pl
 $Inputfile = "$ENV{HOME}/.pskmail/gMFSK.log";			# Rx input from gmfsk

####################### application control #############################
 $period = 60;			# beacon period in minutes (60, 30, 20, 15, 10)
 $Emails_enabled = 1;	# enable internet mail
 $Web_enabled = 1;		# enable web access
#########################################################################


 #########################################################################
 # Aprs-is port - Position: ddmm.mmN/dddmm.mmE
 $Aprs_connect = 1;
 { # position with leading zero's !!
 my $latitude = "0000.00N";    # format: decimal degrees x 100 + decimal minutes xxxx.xxN
 my $longitude = "00000.00E";  # format: decimal degrees x 100 + decimal minutes xxxxx.xxE
 my $serverstatus = "PSKmail-0.8";

 $Aprs_beacon = $latitude . "P" . $longitude . "&" . $serverstatus; ## Fixes symbol, don't change
 }
 @Aprs_port = qw (1314 1314 10151 2023 10151);
 @Aprs_address = qw (netherlands.aprs2.net germany.aprs2.net rotate-filter.aprs.net ahubswe.net second.aprs.net);
 @prefixes = qw();
 $posit_time = 10; # minutes
 #########################################################################
 # rig control (scheduler, scanner)
 $scheduler = 0;
 $rigtype = 351;
 $rigrate = 4800;
 $rigdevice = "/dev/ttyS0";
 $rigptttune = 0; 	# If 1 then ICOM "PTT TUNE" is in use
 ##########################
 $scanner = "";
 $qrgfile = "$ENV{HOME}/.pskmail/qrg/freqs.txt";
 $freq_offset = 1;	# minutes after '0'
 @freq_corrections = qw (0 0 0 0 0); # trx frequency correction
 $traffic_qrg = 0;
 #########################################################################
 1;

