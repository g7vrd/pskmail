#! /bin/sh

# lib installer for debian based systems (ubuntu).

apt-get install libportaudio2
apt-get install  libhamlib2  libhamlib-utils


echo "Installing necessary SSL libraries for pskmail server"

apt-get install libnet-ssleay-perl libdigest-crc-perl
apt-get install libio-socket-ssl-perl elinks curl lynx libhamlib-utils

echo "Installing network app modules via cpan"

cpan Mail::POP3Client
cpan Email::LocalDelivery
cpan Email::Folder
cpan Net::SMTP::TLS
cpan MIME::Lite
cpan IO::Multiplex

exit 1

