#! /usr/bin/perl -w

# ARQ module rflinkserver.pl by PA0R. This module is part of the PSK_ARQ suite of
# programs. Rflinkserver contains the server protocol engine which adds an arq layer
# to keyboard oriented protocols like PSK31, PSK63, MFSK, MT63 etc.

# Rflinkserver.pl includes the arq primitives for the server.

# This program is published under the GPL license.
#   Copyright (C) 2005, 2006, 2007, 2008, 2009
#       Rein Couperus PA0R (rein@couperus.com)
#
# *    rflinkserver.pl is free software; you can redistribute it and/or modify
# *    it under the terms of the GNU General Public License as published by
# *    the Free Software Foundation; either version 2 of the License, or
# *    (at your option) any later version.
# *
# *    rflinkserver.pl is distributed in the hope that it will be useful,
# *    but WITHOUT ANY WARRANTY; without even the implied warranty of
# *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# *    GNU General Public License for more details.3
# *
# *    You should have received a copy of the GNU General Public License
# *    along with this program; if not, write to the Free Software
# *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#Last change: 020709


use lib "/usr/local/share/pskmail_server";
use Mail::POP3Client;
use Net::SMTP;
use Net::SMTP::TLS;
use DB_File;
use IO::Handle;
use MIME::Base64;
use MIME::Lite;
use Time::Local;
use Email::LocalDelivery;
use Email::Folder;
use IO::Select;

my $period = 30;
my $debug_mail = 1;
my $monitor = 1;
my $opensystem = 1;
my $Use_ssl = 0;
my $email_limit = 20000;
my $Emails_enabled = 1;
my $Web_enabled = 1;
#my $BigEarserverport = "";
my $select = IO::Select->new();

use arq;

$SIG{INT} = \&catcher;

my ($ServerCall, $Inputfile, $output, $dbfile, $dbpass, $dbaddress, $relay, $smtpauthlevel, $smtptlsuser, $smtptlspass, $smtptlsport, $Max_retries, $Maxidle,$Maxwebpage, $nosession, $commandcall, $Beaconminute, $Aprs_connect, $Aprs_beacon, $Aprs_port, $Aprs_address, @prefixes, $posit_time, $scheduler, @schedule, $rigtype, $rigrate, $rigdevice, $scanner, $qrgfile, $freq_offset, @freq_corrections, @Beaconarray, $traffic_qrg);

$traffic_qrg = 0;
#$BigEar = "";

if (-e "$ENV{HOME}/.pskmail/pskmailrc.pl") {
	eval `cat $ENV{HOME}/.pskmail/pskmailrc.pl`
	or die "No config file: $@\n";
}

if (-e "$ENV{HOME}/.pskmail/.manual_config") {
	my $config = `cat $ENV{HOME}/.pskmail/.manual_config`;
	chomp $config;
	($ServerCall,$relay,$Aprs_beacon) = split ",", $config;
	`rm $ENV{HOME}/.pskmail/.manual_config`;
}

	`touch $ENV{HOME}/.pskmail/aprslog`;

#print $Aprs_beacon, "\n";
if ($Aprs_beacon !~ /\d\d\d\d\.\d\d[NS]P\d\d\d\d\d\.\d\d[EW]&/ &&
	$Aprs_beacon !~ /\d\d\d\d\.\d\d[NS]\/\d\d\d\d\d\.\d\d[EW]/) {
	logprint("Wrong APRS beacon format. Please adjust pskmailrc.pl\n");
	print ("Latitude format is: ddmm.mm, padded with leading and trailing zeros\n");
	print ("e.g.: 4420.34 or 0400.11 or 0000.10\n\n");
	print ("Longitude format is: dddmm.mm, padded with leading and trailing zeros\n");
	print ("e.g.: 14420.34 or 00400.11 or 00000.10\n\n");
	print "Please stop and restart\n";
}

if (-e "$ENV{HOME}/.pskmail/scheduler.pl") {
	eval `cat $ENV{HOME}/.pskmail/scheduler.pl` or die "Could not interpret scheduler file\n";
		# get the schedule if there is one...
	$scheduler = 1;
}

my $modem = "PSK250";

setmode ($modem);

### connect to BigEar server...

=head1 # bigear experimental, removed from main stream
$BigEar = bigearconnect ();

if ($BigEar) {
	print "Connected to PSKBigEar\n";
} else {
	print "BigEar not available";
}
=cut

my ($reader, $writer);
pipe($reader, $writer);
$writer->autoflush(1);

my $ConnectStatus = "Listening";
my $TextFromFile = "";
my $Status = "Listen";
my $RxStatus = "";
my $Systemstatus = "";
my $Retries = 0;
my $RxReady = 0;
my $outputstring = "";

my $Version = "0.9.2";
my $Pop_host = 	"";	# pop3 provider
my $Pop_user = 	"";	# pop user
my $Pop_pass = 	"";	# pop password
my $attachment = ""; # attachment file name
my $address = 	""; 	# set return address for SMTP....
my $mailuser = 	"$ENV{HOME}/.pskmail/.mailuser"; 	# data store where the mail is received with fetchmail...
my $findupasswd = "";	# password for APRS

my $Usercalls = "";
my $usrmessage = "";
my $call = "";
my $dummy = 0;		# dummy variable
my $mymailnumber = 0;
my @msgarray = ();
#scheduler
my $systime = 0;
my $lasthour = 0;
#scanner
my $oldhour = 0;
my $oldmins = 0;
my @freqs = ();
my @modes = ();
my @freqhrs = ();
my @rigmod = ();
# beacon
my $Beacon_sent = 0;
my @Beacons_sent = qw (0 0 0 0 0);	# Keeps track of beacons, have they been sent this time around ?


		if ($scanner && -e "$ENV{HOME}/.pskmail/qrg/freqs.txt") {
			open ($fh, $qrgfile) or die "Cannot open the scanner file!";
			@freqhrs = <$fh>;
			close ($fh);

			$hour = (time / 3600) % 24;		#/
			$cat = $freqhrs[0];
			print "Scanning: $cat";
			print "Offset = $freq_offset minute(s)\n";
			chomp $cat;
			@freqs = split (",", $cat);

			my $pskmodes = $freqhrs[1];
			@modes = split (",", $pskmodes);

			if (exists $freqhrs[2]) {
				my $rigmodes = $freqhrs[2];	## PATCH IS0GRB-3 11092008
				@rigmod = split (",", $rigmodes);
			}
		}


$ConnectStatus = "Listening";

print "$Aprs_connect\n";

# open aprs write port

#my ($udp_ipaddr,$udp_portaddr);

$error = `echo "none" > $ENV{HOME}/.pskmail/PSKmailsession`;
$error = `touch zb`;

initialize();

	open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
	print SESSIONDATA $nosession;
	close SESSIONDATA;

#get users from data base
	getusercalls();

{
	my @shortcallusers = split (" ", ($ServerCall . " " . $Usercalls));
	open (CALLS, ">$ENV{HOME}/.pskmail/calls.txt");
	foreach my $call (@shortcallusers) {
		print CALLS $call, "\n";
	}
	close CALLS;
}

=head1
########### fork off a listner for BigEar server ##############
 	my $bigearline;

	if ($bigearpid = fork)	{
		# this is the parent, continuing below
	} else {
		# listens for bigear, writes to pipe.
		die "cannot fork the bigear listener: $!\n" unless defined $bigearpid;

		while (1) {
			if ($BigEar) {
					eval {
					local $SIG{ALRM} = sub {die "alarm"};
					alarm 1;
					eval {
						$bigearline = <$BigEar>;
					};
					alarm 0;
				};
				alarm 0;
				if ($@ && $@ =~ /alarm/) {
					print "Timeout reading BigEar\n";
				} elsif ($@) {
					print "$@,\n";
				}
			}

			if ($bigearline) {
				if ($bigearline !~ /$ServerCall/) {
					print $bigearline;
					open (LINE, ">$ENV{HOME}/.pskmail/.bigearout");
					print LINE $bigearline;
					close LINE;
				}
				$bigearline = "";
			}

			if (-e "$ENV{HOME}/.pskmail/.bigear") {
				my $packet = `cat $ENV{HOME}/.pskmail/.bigear`;
				`rm $ENV{HOME}/.pskmail/.bigear`;
				eval {
					print $BigEar $packet or die "noprint";
				};
				if ($@ && $@ =~ /noprint/) {
					sleep 10;
					$BigEar = bigearconnect();
					$BigEar->autoflush(1);

					print "Reconnect to BigEar\n";
				}
			}
			select (undef, undef, undef, 0.1);
		}
	}
=cut

if ($pid = fork) {
	close $writer;


	while (1) {
		my $readline= "";
		eval {
			local $SIG{ALRM} = sub { die "timeout" };
			alarm 10;
			eval {
				$readline = <$reader>;
			};
		};
		alarm 0;
		die if $@ && $@ !~ /timeout/;

##### send bulletin ########################################

		if (-e "$ENV{HOME}/.pskmail/pskdownload/bulletin") {
			my $bulletintext = `cat $ENV{HOME}/.pskmail/pskdownload/bulletin`;
			my $dt = `date -u`;
			$bulletintext = "ZCZC\nQTC de $ServerCall \n$dt\n\n" . $bulletintext . "NNNN\n";
			sendbulletin ($bulletintext);
			`rm $ENV{HOME}/.pskmail/pskdownload/bulletin`;
		}
##### send fleetcodes ########################################

		if (-e "$ENV{HOME}/.pskmail/pskdownload/fleetcode") {
			my $bulletintext = `cat $ENV{HOME}/.pskmail/pskdownload/fleetcode`;
			my $dt = `date -u`;
			$bulletintext = "ZFZF\nQTC de $ServerCall \n$dt\n\n" . $bulletintext . "NNNN\n";
			sendbulletin ($bulletintext);
			`rm $ENV{HOME}/.pskmail/pskdownload/fleetcode`;
		}
############################################################
		if ($readline) {
			chomp($readline);
			if ($monitor) {
				print $readline, "\n";
			}
			$_ = $readline;

			if (m/~TEST/) {
				`cat $ENV{HOME}/.pskmail/testfile >> $ENV{HOME}/.pskmail/TxInputfile`;
			} elsif (m/~QUIET!/) {
####### switch beacon off #################################
				if (get_session() eq $commandcall) {
					$nosession = "none";
					print "Beacon off...\n";
					`echo "Beacon off" > $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~BEACON!/) {
####### switch beacon on #################################
				if (get_session() eq $commandcall) {
					$nosession = "beacon";
					`echo "Beacon on" > $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~SPEED250!/) {
####### switch modem #################################
					sleep 10;
					sendmode("PSK250");
					open (CMD, ">.modem");
					print CMD 'PSK250';
					close (CMD);
			} elsif (m/~SPEED125!/) {
####### switch modem #################################
					sleep 10;
					sendmode("PSK125");
					open (CMD, ">.modem");
					print CMD 'PSK125';
					close (CMD);
			} elsif (m/~SPEED63!/) {
####### switch modem #################################
					sleep 10;
					sendmode("PSK63");
					open (CMD, ">.modem");
					print CMD 'PSK63';
					close (CMD);
			} elsif (m/~QSY!/) {
####### switch frequency #################################
					if ($traffic_qrg) {
						sleep 10;
						$dummy = `rigctl -m $rigtype -r $rigdevice -s $rigrate "F" $traffic_qrg`;
						print "QSY to $traffic_qrg!\n";
					}
			} elsif (m/~STATUS?/) {
####### get server status #################################

					my $uptime = `uptime`;
					my $mem = `df | grep sda`;
					if ($mem =~ /(\d+%)/) { $mem = "MM = " . $1 . "\n";}
					my $pingout = "No internet connection";
					my @ping = ();
					eval {
						@ping = `ping -c 1 pskmail.org 2>/dev/null`;
					};
					if($ping[1] && $ping[1] =~ /(time=\d*\.*\d* ms)/) {
						$pingout = "Ping " . $1;
					}
					`echo "\nServer status:\n$uptime$mem$pingout\n" > $ENV{HOME}/.pskmail/TxInputfile`;


			} elsif (m/~ROUTES/) {
####### send routes table to client  #################################
				if (-e "$ENV{HOME}/.pskmail/routes") {
					my $routes = "";
					my @routelines = `cat $ENV{HOME}/.pskmail/routes`;
					my @sortedlist = sort { $a cmp $b } @routelines;
					foreach my $line (@sortedlist) {
						if ($line =~ /(\S+) (\S+) (\d+)/) {
							my $rec = $3;
							my $tim = time();
							my $dur = int (($tim - $rec) / 60);
							if ($dur < 1440) {
								my $add = sprintf ("%s -> %s %d\n", $1,$2,$dur);
								$routes .= $add;
							}
						}
					}

					`echo "$routes" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {
					`echo "Sorry, no routes available" > $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~GETBEACONS/) {
####### switch beacon on #################################
				my $call = get_session();
					my $beacons = getbeacons($call);
					`echo "$beacons" > $ENV{HOME}/.pskmail/TxInputfile`;

			}elsif (m/~QTC\?/) {
####### get list of headers #################################

				getuserdata(get_session());

				if ($Pop_host) {
					eval {
						list_mail() ; # get mail from pop server
					};
					if ($@) {
						`echo "Cannot get the mail headers...$@" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						logprint ("QTC received...\n");
						if ($monitor && $debug_mail) {
							print `cat $ENV{HOME}/.pskmail/mailheaders`;
						}
						if (-s "$ENV{HOME}/.pskmail/mailheaders" < 10) {
							`echo "No mail" > $ENV{HOME}/.pskmail/TxInputfile`;
						}elsif (-e "$ENV{HOME}/.pskmail/mailheaders") {
							my $hdrlength = length (`cat $ENV{HOME}/.pskmail/mailheaders`);
							`echo "Your mail:" >> $ENV{HOME}/.pskmail/TxInputfile`;
							my $headers = `cat $ENV{HOME}/.pskmail/mailheaders >> $ENV{HOME}/.pskmail/TxInputfile`;
							`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
							unlink "$ENV{HOME}/.pskmail/mailheaders";
						}
					}
				} else {
						`echo "Cannot get your mail!" >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
			}elsif (m/~QTC (\d*)\+/) {
####### get list of headers from nr. #########################

				my $starthdr = $1;
				getuserdata(get_session());

				print "Req. QTC from $starthdr\n";

				if ($Pop_host) {
					eval {
						list_mail() ; # get mail from pop server
					};
					if ($@) {
						`echo "Cannot get the mail headers...$@" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						logprint ("QTC received...\n");
						if ($monitor && $debug_mail) {
							print `cat $ENV{HOME}/.pskmail/mailheaders`;
						}
						if (-s "$ENV{HOME}/.pskmail/mailheaders" < 10) {
							`echo "No mail" > $ENV{HOME}/.pskmail/TxInputfile`;
						}elsif (-e "$ENV{HOME}/.pskmail/mailheaders") {
							my $hdrlines = `cat $ENV{HOME}/.pskmail/mailheaders | wc -l`;

							my $newhdrs = $hdrlines - $starthdr;

							if ($newhdrs > 1) {
								`echo "Your mail:" >> $ENV{HOME}/.pskmail/TxInputfile`;
								my $headers = `tail -n $newhdrs $ENV{HOME}/.pskmail/mailheaders`;
								`echo "$headers" >> $ENV{HOME}/.pskmail/TxInputfile`;
								`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
								unlink "$ENV{HOME}/.pskmail/mailheaders";
							} else {
								my $hdrs = $hdrlines - 2;
								`echo "Sorry, only $hdrs mails" >> $ENV{HOME}/.pskmail/TxInputfile`;
							}
						}
					}
				} else {
						`echo "Don't know how get your mail!" >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~MAIL/) {
####### get ALL mail #################################
				getuserdata(get_session());
				if ($Pop_host) {
					eval {
						read_mail(1 ... $Count) ; # get mail from pop server
					};
					if ($@) {
						`echo "Cannot get your mail: $@ > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						filter($mailuser, 0);
						if ($debug_mail) {`cat $ENV{HOME}/.pskmail/mailfile`;}
						`mv $ENV{HOME}/.pskmail/mailfile $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
					}
				} else {
						`echo "Cannot get your mail!" >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~LISTLOCAL(.*)/) {
####### list local mails #################################
				my $call = get_session();

				if (-e "$ENV{HOME}/.pskmail/localmail/$call") {
					my $folder = Email::Folder->new("$ENV{HOME}/.pskmail/localmail/$call");
					my $mails = "";
					my $count = 0;

					for my $msg ($folder->messages) {
						$count++;
						print $count, " ", $msg->header("From"), " ", $msg->header("Subject"), "\n";
						$mails .= $count . " ". $msg->header("From") . " " . $msg->header("Subject") . "\n";
					}
					`echo "Local mail for $call:\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
					`echo "$mails" >> $ENV{HOME}/.pskmail/TxInputfile`;
					`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
				} else {
					`echo "No local mail for $call:\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				}

			} elsif (m/~DELETELOCAL (.*)/) {
####### delete local mails #################################
				my $call = get_session();
				my $msgnr = 1 unless $1;
				my @numbers = split / /, $1;

				if (-e "$ENV{HOME}/.pskmail/localmail/$call") {
					my $folder = Email::Folder->new("$ENV{HOME}/.pskmail/localmail/$call");
					my $mails = "";
					my $count = 0;
					for my $msg ($folder->messages) {
						$count++;
						my $del = 1;
						for $msgnr (@numbers) {
							if ($count == $msgnr) {
								$del = 0;
							}
						}
						if ($del) {
							$mails .= "From $call " .
							$msg->header("Date") . "\n" .
							$msg->as_string . "\n\n";
						}
					}
					open (MAILS, ">", "$ENV{HOME}/.pskmail/localmail/$call");
					print MAILS $mails;
					close (MAILS);
					`echo "Mails $1 deleted..." >> $ENV{HOME}/.pskmail/TxInputfile`;
				} else {
					`echo "No local mail for $call:\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~READLOCAL (.*)/) {
####### read local mails #################################
				my $call = get_session();
				my $msgnr = 1 unless $1;
				my @numbers = split / /, $1;


				if (-e "$ENV{HOME}/.pskmail/localmail/$call") {
					my $folder = Email::Folder->new("$ENV{HOME}/.pskmail/localmail/$call");
					my $mails = "";
					my $count = 0;

					for my $msg ($folder->messages) {
						$count++;
						for $msgnr (@numbers) {
							if ($count == $msgnr) {
								$mails .= "Message $count for $call:\n" .
								"From: ". $msg->header("From") .
								"\nSubject: " .
								$msg->header("Subject") . "\n" .
								"Date: " .
								$msg->header("Date") . "\n\n" .
								$msg->body . "\n" ;
							}
						}
					}
					`echo "$mails" >> $ENV{HOME}/.pskmail/TxInputfile`;
					`echo "\n-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				} else {
					`echo "No local mail for $call:\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~READPAQ(.*)/ && $Emails_enabled) {
####### get single mails #################################
				getuserdata(get_session());
				my @numbers = split / /, $1;
				eval {
					read_mail(@numbers);
				};
				if ($Emails_enabled == 0) {
					`echo "sorry, no internet email on this server" > $ENV{HOME}/.pskmail/TxInputfile`;
				} elsif ($@) {
					`echo "Cannot get your mail: $@" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {
					filter($mailuser, 0);
					`/usr/local/share/pskmail_server/paq864 $ENV{HOME}/.pskmail/mailfile`;
					$size_file = `cat $ENV{HOME}/.pskmail/mailfile.864`;
					$ident = "~PAQ864RD " . sprintf("%d", length($size_file));
					`echo "$ident" >> $ENV{HOME}/.pskmail/TxInputfile`;
					`cat mailfile.864 >> $ENV{HOME}/.pskmail/TxInputfile`;
					`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
					if ($debug_mail) {`cat $ENV{HOME}/.pskmail/mailfile`;}
					unlink "$ENV{HOME}/.pskmail/mailfile.864";
				}
			} elsif (m/~READ (\d*)/ && $Emails_enabled) {
####### get single mails #################################
				getuserdata(get_session());

				if ($Pop_host) {

					my @numbers = ();
					my $number	= 0;

					if ($1) {
						$number = $1;
					} else {
						$number = 1;
					}
					$numbers[0] = $number;
					eval {
						read_mail(@numbers);
					};
					if ($Emails_enabled == 0) {
						`echo "sorry, no internet email on this server" > $ENV{HOME}/.pskmail/TxInputfile`;
					} elsif ($@) {
						`echo "Cannot get your mail: $@" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						filter($mailuser, 0);
						my $msglen = -s "$ENV{HOME}/.pskmail/mailfile";
						if ($msglen > $email_limit) {
							`head -c $email_limit mailfile`;
							`echo "Your msg: $msglen" >> $ENV{HOME}/.pskmail/TxInputfile`;
							`cat $ENV{HOME}/.pskmail/mailfile >> $ENV{HOME}/.pskmail/TxInputfile`;
							`echo "[mail truncated]" >> $ENV{HOME}/.pskmail/TxInputfile`;

						} else {
							`echo "Your msg: $msglen" >> $ENV{HOME}/.pskmail/TxInputfile`;
							`cat $ENV{HOME}/.pskmail/mailfile >> $ENV{HOME}/.pskmail/TxInputfile`;
						}
							`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;

						if ($debug_mail) {`cat $ENV{HOME}/.pskmail/mailfile`;}
						unlink "$ENV{HOME}/.pskmail/mailfile";
					}
				}
			} elsif (m/~KEEP(.*)/) {
####### send mail to archive #################################
####### changer subject to [Mailarchive]date #################

				getuserdata(get_session());
				if ($Pop_host) {

					my @numbers = split / /, $1;
					eval {
						read_mail(@numbers);
					};
					if ($@) {
						`echo "Cannot get your mail: $@" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						filter($mailuser, 1);

						@message = ();

						my $call = get_session();
						getuserdata($call);

						my $subject = "[Mailarchive] " . `date`;
						my $to = $address;

						push @message, $to;
						push @message, $address;
						push @message, $subject;

						open ($MA, "$ENV{HOME}/.pskmail/mailfile");
						my @body = <$MA>;
						close ($MA);
						foreach my $messageline (@body) {
							chomp $messageline;
						}
						push @message, @body;

						eval {
							send_mail(@message);	# send the mail via smtp
						};
						if ($@) {
							`echo "Error sending mail : $@" >> $ENV{HOME}/.pskmail/TxInputfile`;
						} else {
							`echo "\nArchive $1 sent...\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						}
						@message = ();
						unlink "$ENV{HOME}/.pskmail/mailfile";
					}
				}
			} elsif (m/~GETFILE (.*)/) {
####### get file from ./pskdownload dir #################################
				my $grb = `cat $ENV{HOME}/.pskmail/pskdownload/$1`;
				$grb =~ s/\r\n/\n/g;
				`echo "$grb" > $ENV{HOME}/.pskmail/TxInputfile`;
				`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
			} elsif (m/~GETBIN (.*)/) {
####### get file from ./pskdownload dir #################################
				my $filenm = $1;
#print "$filenm\n";
				my $grb = "";
				if (-e "$ENV{HOME}/.pskmail/pskdownload/$1") {
					if (substr ($filenm, -2) ne "gz" && substr ($filenm, -3) ne "bz2") {
						`gzip -f -9 $ENV{HOME}/.pskmail/pskdownload/$filenm`;
						$filenm .= ".gz";
						$grb = `cat $ENV{HOME}/.pskmail/pskdownload/$filenm`;
						`gunzip $ENV{HOME}/.pskmail/pskdownload/$filenm`;
					} else {
						$grb = `cat $ENV{HOME}/.pskmail/pskdownload/$filenm`;
					}
					$grb = encode_base64 ($grb);
					$lqrb = length ($grb);
					`echo "Your file:$filenm $lqrb" >> $ENV{HOME}/.pskmail/TxInputfile`;
					`echo "$grb" >> $ENV{HOME}/.pskmail/TxInputfile`;
					`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
				} else {
					`echo "File not found!" >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~LISTFILES/) {
####### list files from ./pskdownload dir #################################
				my $filelist = `ls -l $ENV{HOME}/.pskmail/pskdownload`;
				@filelines = split ("\n", $filelist);
				$filelist = "";
				foreach $fileline (@filelines) {
					if ($fileline =~ /.*1(.*) (.*) (.*) (.*) (.*)/) {
						$filelist = $filelist . $5 . " " . $3 . " " .  $4 . " " . $2 . "\n";
					}
				}
				my $grb = `echo "$filelist" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$grb = `echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$filelist = "";
			} elsif (m/~TGET (.*)/ && $Web_enabled) {
####### dump web page with elinks #################################
				my $URL = $1;
				my $webpage = "";

				if ($Web_enabled) {
					if ($monitor) {
						print "Trying $URL\n";
					}
					`echo "Trying $URL\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

					eval {
						$webpage =  `elinks -dump $URL 2>&1` or die "No page";
	#					$webpage =  `lynx -dump -connect_timeout=10 $URL 2>&1` or die "No page";
					};

					if ($webpage =~ /^Alert!/m) { $webpage = "Timeout!\n"; }

					if ($@ && $@ =~ /No page/) {
						if ($monitor) {
							print "404\n";
						}
						`echo "404\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {

						if (length $webpage > $Maxwebpage) {
							$webpage = substr ($webpage, 0, $Maxwebpage);
							$webpage .= "\n-truncated -\n\n";
						}
						my $weblength = length ($webpage);
						`echo "Your wwwpage: $weblength" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "$webpage \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						if ($monitor) {
							print $webpage;
						}
					}

				} else {
						`echo "Sorry, no web access from this server..."  >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~GETPOS\s*(.*)/) {
####### get position from findu.com  #################################
				my $poscall = "";
				my $webpage = "";

				if (-e "$ENV{HOME}/.pskmail/.internet") {
					if (defined $1 && $1) {
						$poscall = $1;
					} else {
						$poscall = get_session();
					}
					my $URL = "http://hermes.esrac.ele.tue.nl/position.php?call=" . $poscall;

					eval {
						local $SIG{ALRM} = sub { die "timeout" };
						alarm 10;
						eval {
							@web =  `lynx -dump $URL` or die "No page";
							$webpage = join "", @web;
						};
					};
					alarm 0;
					if ($@ && $@ =~ /timeout/){
						$webpage = "Cannot connect\n"
					}
					if ($webpage =~ /^Alert!/m) { $webpage = ""; }
				}
				if ($webpage) {
					`echo "$webpage \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
					`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

=head1
					if ($@ && $@ =~ /No page/) {
						if ($monitor) {
							print "404\n";
						}
						`echo "Sorry, page not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						my @positpage = split /\n/, $webpage;
						my $positline = "";
						my @outarray = ();
						foreach $positline (@positpage) {
							if ($positline =~ /Position of/) {push @outarray, $positline; }
							if ($positline =~ /received/) {push @outarray, $positline; }
							if ($positline =~ /PSKAPR/) {push @outarray, $positline; }
						}
						$webpage = join "\n", @outarray;
						`echo "$webpage \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						if ($monitor) {
							print $webpage;
						}
					}
=cut
				} else {
					`echo "Cannot get position\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~GETMSG\s*(\d*)/) {
####### get messages from findu.com  #################################
				my $msgnr = "";

				if (-e ".internet"){
					if ( $1 && $1 > 0) {
						$msgnr = $1;
					} else {
						$msgnr = 10;
					}
					my $call = get_session();
					my $to_me = 0;
					my $webpage = "";

					@msgs = `lynx -dump  http://www.db0anf.de/app/aprs/stations/messages-$call`;
					$printflg = 0;



					my $j = $msgnr;

					foreach $line (@msgs) {
						if ($line =~ /APRS Messages:/) {
							$printflg = 1;
						} elsif ($line =~ /Total number of message records for/) {
							$printflg = 0;
						} elsif ($printflg) {
							if ($line =~ /\[\d+\](\S+)/) {
								if ($1 ne $call) {
									$to_me = 1;
								} else {
									$to_me = 0;
								}
							}
							if ($to_me && $line =~ /\[\d+\](\S+)\s*\[\d+\](\S+)\s+\d\d(\d\d)-(\d\d)-(\d\d\s\d\d:\d\d):\d\d(\s.*)/) {
								my $fr = $1 . "      ";
								$fr = substr ($fr, 0, 10);
					print "CHR|" . substr($6, 1, 2) . "|\n";
								if (substr ($6, 1, 2) ne ":") {
#									print $fr . $3 . $4 . $5 . $6 ."\n";

									$webpage .= $fr . $3 . $4 . $5 . $6 ."\n";
								}

							} else {
								if ($to_me) {
#									print substr ($line, 22);
									$webpage = substr ($webpage, 0, length($webpage) -1);
									if ($line =~ /\s*(.*)/) {
										$line = " " . $1 . "\n";
									}
									$webpage .= $line;
								}
							}
						}

					}
#					print "WEBPAGE:\n" . $webpage;

					my @pg = ();
					my @pg2 = ();
					@pg = split "\n", $webpage;
					@pg2 = reverse @pg;

					$webpage = "";
					my $k = 0;

					for ($k = 0; $k < $msgnr; $k++) {
						my $ln = pop @pg;
						if ($ln) {
							$webpage .= $ln ;
							$webpage .= "\n";
						}
					}
#					print "WEBPAGE2:\n" . $webpage;

					`echo "From       Date  Time  Message " > $ENV{HOME}/.pskmail/TxInputfile `;
					`echo "$webpage" >> $ENV{HOME}/.pskmail/TxInputfile`;


				} else {
					`echo "Sorry, internet not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;

				}
			} elsif (m/~GETNEAR/) {
####### get near stations from findu.com  #################################

				if (-e ".internet"){
					my $URL = "http://www.findu.com/cgi-bin/near.cgi?call=" . get_session();
					my $webpage = "";

					eval {
						local $SIG{ALRM} = sub { die "timeout" };
						alarm 10;
						eval {
							@all =  `lynx -dump $URL 2>&1` or die "No page";
						};
					};
					alarm 0;
					if ($@ && $@ =~ /timeout/){
						$webpage = "Cannot connect\n"
					}
					if ($webpage =~ /^Alert!/m) { $webpage = "Cannot connect\n";}

					if ($@ && $@ =~ /No page/) {
						if ($monitor) {
							print "404\n";
						}
						`echo "Sorry, page not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						`echo "APRS Stations near you \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

						foreach $line (@all) {
							if ($line =~ /Call callbook msg/) {
									$meat = 1;
							} elsif ($line =~ /Overall rate:/) {
									$meat= 0;
							} elsif ($line =~ /.*P(\d+)\.GIF\]\s(.*)/) {
								if ($1 == 90 ||
									$1 == 14 ||
									$1 == 31 ||
									$1 == 48 ||
									$1 == 51 ||
									$1 == 54 ||
									$1 == 58 ||
									$1 == 75 ||
									$1 == 76 ||
									$1 == 86 ||
									$1 == 87 ||
									$1 == 29
								) {
								$validtype = 1;
								$position = $1 . "," . $2 ;
							}


							} elsif ($line =~ /(\d+\.\d+)\s(\d+\.\d+)\s(\d+\.\d+)\s(\w+)\s00:(\d+)(:\d+:\d+)/ && $validtype && $meat) {
								if ($5 < 4) {
									$position = $position . "," . $1 . "," . $2 . "," . $3 . "," . $4 . "," . $5 . $6 ."\n";
									$validtype = 0;
									$webpage .= $position;
								}
							}


						}


						`echo "$webpage \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						if ($monitor) {
							print $webpage;
						}
					}
				} else {
					`echo "Sorry, internet not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;

				}


			} elsif (m/~GETTIDE\s*(\d+)/) {
####### get tideinfo from findu.com  #################################
				my $msgnr = "";

				if (-e ".internet"){
					if ( $1 && $1 > 0) {
						$msgnr = $1;
					} else {
						$msgnr = 1229;
					}

					my $URL = "http://www.findu.com/cgi-bin/tide.cgi?tide=" . $msgnr;
					my $webpage = "";
	 				my @all = ();

					eval {
						local $SIG{ALRM} = sub { die "timeout" };
						alarm 10;
						eval {
							@all =  `lynx -dump $URL 2>&1` or die "No page";
						};
					};
					alarm 0;
					if ($@ && $@ =~ /timeout/){
						$webpage = "Cannot connect\n"
					}
					if ($webpage =~ /^Alert!/m) { $webpage = "Cannot connect\n";}

					if ($@ && $@ =~ /No page/) {
						if ($monitor) {
							print "404\n";
						}
						`echo "Sorry, page not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						my $count = 11;
						my $webpage = "";
						foreach $line (@all) {
							if ($line =~ /Tide at/ || $line =~ /\d+\-\d+\-\d+\s/) {
								$webpage .= $line;
								$count--;
								if ($count == 0) {
									last;
								}
							}
						}


						`echo "$webpage \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						if ($monitor) {
							print $webpage;
						}
					}
				} else {
					`echo "Sorry, internet not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;

				}

			} elsif (m/~GETTIDESTN\s*(\d*)/) {
####### get tidestations from findu.com  #################################

				if (-e ".internet"){
					my $URL = "http://www.findu.com/cgi-bin/tidestation.cgi?call=" . get_session();
					my @all = ();

					eval {
						local $SIG{ALRM} = sub { die "timeout" };
						alarm 10;
						eval {
							@all =  `lynx -dump $URL 2>&1` or die "No page";
						};
					};
					alarm 0;
					if ($@ && $@ =~ /timeout/){
						$webpage = "Cannot connect\n"
					}
					if ($webpage =~ /^Alert!/m) { $webpage = "Cannot connect\n";}

					if ($@ && $@ =~ /No page/) {
						if ($monitor) {
							print "404\n";
						}
						`echo "Sorry, page not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						$webpage = "";
						foreach $line (@all) {
							if ($line =~ /(\d+\.\d+)\s(\d+\s)\[\d+\](.*)\s\d+\.\d+\s\d+\.\d+$/) {
								$webpage .=  $2 . $3 . ", " . $1 . "\n";
							}
						}


						`echo "$webpage \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						if ($monitor) {
							print $webpage;
						}
					}
				} else {
					`echo "Sorry, internet not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;

				}
			} elsif (m/~GETSERVERS\s*(\d*)/) {
####### get server freqs from wiki  #################################

				if (-e ".internet"){
					my @all = ();

					eval {
						local $SIG{ALRM} = sub { die "timeout" };
						alarm 20;
						eval {
							@all =  `elinks -dump http://pskmail.wikispaces.com/PSKmailservers | grep Active` or die "No page";
						};
					};
					alarm 0;
					$webpage = "";
					if ($@ && $@ =~ /timeout/){
						$webpage = "Cannot connect\n"
					}
					if ($webpage =~ /^Alert!/m) { $webpage = "Cannot connect\n";}

					if ($@ && $@ =~ /No page/) {
						if ($monitor) {
							print "404\n";
						}
						`echo "Sorry, page not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						$webpage = "";
						foreach $line (@all) {
							if ($line =~ /\[\d+\](\S+)\s(.*)\s*\[\d+\](\w+)small.*/) {
								$webpage .= " " . $1 . "     " . $2 . " " . $3 . "\n";
							} elsif ($line =~ /(.*)\s*\[\d+\](\w+)small.*/) {
								$webpage .= $1 . " " . $2 . "\n";
							} elsif ($line =~ /(\S+)\s+(\d+\.*\d*)\s*(\d+\.*\d*)\s*(\d+\.*\d*)\s*(\d+\.*\d*)\s*(\d+\.*\d*)/) {
								$webpage .= $1 . " " . $2  . " " . $3 . " " . $4 . " " . $5 . " " . $6 . "\n";
							}
						}


						`echo "$webpage \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						if ($monitor) {
							print $webpage;
						}
					}
				} else {
					`echo "Sorry, internet not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;

				}
			} elsif (m/~GETNEWS\s*(\d*)/) {
####### get server freqs from wiki  #################################

				if (-e ".internet"){
					my @all = ();

					eval {
						local $SIG{ALRM} = sub { die "timeout" };
						alarm 20;
						eval {
							@all =  `elinks -dump http://pskmail.wikispaces.com/NEWS` or die "No page";
						};
					};
					alarm 0;
					if ($@ && $@ =~ /timeout/){
						$webpage = "Cannot connect\n"
					}
					if ($webpage =~ /^Alert!/m) { $webpage = "Cannot connect\n";}

					if ($@ && $@ =~ /No page/) {
						if ($monitor) {
							print "404\n";
						}
						`echo "Sorry, page not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						$webpage = "Pskmail News \n\n";
						foreach $line (@all) {
							if ($line =~ /notify me/) {
								$valid = 1;
							} elsif ($line =~ /--------/) {
								$valid = 0;
							} elsif ($valid && $line !~ /Edit This Page/) {
								$webpage .= $line;
							}
						}
						$webpage .= "\n-end-\n";

						`echo "$webpage \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						if ($monitor) {
							print $webpage;
						}
					}
				} else {
					`echo "Sorry, internet not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;

				}
			} elsif (m/~GETWWV/) {
####### get WWV data from NOAA  #################################

			my $wwvout;

				`echo "QRX..., getting your info! \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

				`wget -O $ENV{HOME}/.pskmail/pskdownload/wwv.txt http://www.swpc.noaa.gov/ftpdir/latest/wwv.txt`;

				my @wwv = `cat $ENV{HOME}/.pskmail/pskdownload/wwv.txt`;

				for ($i = 7; $i < 9; $i++) {
					$wwvout .= $wwv[$i];
				}
				`echo "$wwvout \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

			} elsif (m/~GETCAMP (\d+\.\d+)\s*(\d+\.\d+)/) {
####### get camp sites from findu.com  #################################

				if (-e ".internet"){
					my @all = ();

					`echo "QRX..., getting your info! \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

					eval {
						local $SIG{ALRM} = sub { die "timeout" };
						alarm 30;
						eval {
							@all =  `lynx -dump "http://www.darc.de/echolink-bin/ba.pl?sel=latlondec\&latdegdec=$1\&londegdec=$2" 2>&1` or die "No page";
						};
					};
					alarm 0;
					if ($@ && $@ =~ /timeout/){
						$webpage = "Cannot connect\n"
					}
					if ($webpage =~ /^Alert!/m) { $webpage = "Cannot connect\n";}

					if ($@ && $@ =~ /No page/) {
						if ($monitor) {
							print "404\n";
						}
						`echo "Sorry, page not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						$webpage = "";
						foreach $line (@all) {
							if ($line =~ /^Refer/) {
								last;
						} elsif ($line =~ /Google/) {
								last;
						} elsif ($line =~ /\[1\](.*)/) {
								$line = "\n" . $1 . "\n";
							} elsif ($line =~ /^\s*\[\d+\](\d+).(\d+).(\d+).\s(\w)\s(\d+).(\d+).(\d+).\s(\w)(.*)/) {
								$latval = $1 + ($2 + $3/100)/60;
								$lonval = $5 + ($6 + $7/100)/60;
								if ($4 ne "N") {
									$latval *= -1;
								}
								if ($8 ne "O") {
									$lonval *= -1;
								}
								$latprint = sprintf("\n%f, ",$latval);
								$lonprint = sprintf("%f", $lonval);
								$line = $latprint . $lonprint . $9 . "\n";
							} elsif ($line =~ /womo-sp/) {
								$line = "";
							} elsif ($line =~ /Campingplatz/) {
								$line = "";
							}
							$webpage .= $line;
							$count++;
							if ($count > 100) {
								$count = 0;
								last;
							}

						}


						`echo "$webpage \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						if ($monitor) {
							print $webpage;
						}
					}
				} else {
					`echo "Sorry, internet not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;

				}

			} elsif (m/~GETRELAYS (\d+\.\d+)\s*(\d+\.\d+)/) {
####### get VHF/UHF Relays from DARC.de  #################################

				`echo "QRX..., getting your info! \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

				`wget -O relais http://www.darc.de/echolink-bin/relais.pl?sel=latlondec\\&latdegdec=$1\\&londegdec=$2\\&printas=psk` ;

				`cat relais >> $ENV{HOME}/.pskmail/TxInputfile`;
				`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

			} elsif (m/~MSG (\S+)\s+(\S*\@\S*)\s(.*)/) {
####### send aprs mail #################################
					my $call = $1;
					getuserdata($call);
					$to = $2;
					my $aprsmsg = $3;
					if ($to =~ /(.*)\@$ServerCall/) { # local mbox
						print "Local mbox $1\n";
						if (-e "$ENV{HOME}/.pskmail/localmail/$1") {
							print "Local mbox $1 exists\n";
						} else {
							`touch "$ENV{HOME}/.pskmail/localmail/$1"`;
							print "Local mbox $1 made\n";
						}
						my $mydate = `date`;
						my $msg =
							"From $call $mydate" .
							"To: $to" . "\n" .
							"From: $address" . "\n" .
							"Subject: " . "PSKaprs message from $call" . "\n" .
							"Date: $mydate" . "\n" .
							$aprsmsg . "\n\n";

						my @boxes = ("$ENV{HOME}/.pskmail/localmail/$1");
						my @delivered_to = Email::LocalDelivery->deliver($msg, @boxes);

						@message = ();

					} else {
						$subject = "PSKaprs message from $call";
						@message = ();
						if ($to =~ /tweet\@ttt/) {
							$to = "tweet\@messagedance.com";
						}
						if ($to =~ /\S+\@\S+\.\S+/) {
							push @message, $to;
							push @message, $address;
							push @message, $subject;
							push @message, $aprsmsg;
							logprint ("sending mail message to $to\n");
							eval {
								send_mail(@message);	# send the mail via smtp
							};

							if ($@) {
								logprint ("Could not send aprs email message:$@\n");
							 }
					 	}
					}
			} elsif (m/~GETUPDATE/) {
####### get twitter updates #################################

				`echo "QRX..., getting your info! \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

				@myxml = `curl -u "pskmail:pskmail" http://twitter.com/statuses/friends_timeline.xml`;

				my $i = 0;
				foreach $line (@myxml) {
					if ($line =~ /\<screen_name\>(.*)\<\/screen_name\>/) {
						`echo "$1\n" >> TxInputfile`;
						$i++;
						if ($i > 5) {
							last;
						}
					} elsif ($line =~ /\<text\>(.*)\<\/text\>/) {
						print $1, "\n";
						`echo "$1" >> TxInputfile`;

					}

				}
			} elsif (m/~POSITION (\d+\.\d+) (\d+\.\d+)/) {
####### send position to findu.com #################################

				if (-e "$ENV{HOME}/.pskmail/.internet") {

					my $call = get_session();
					getuserdata($call);
					if ($findupasswd) {

						my $pos_string = "call=$call&passwd=$findupasswd&lat=$1&lon=$2\n";

						$to = "posit\@findu.com";
						$subject = "none";
						@message = ();
						push @message, $to;
						push @message, $address;
						push @message, $subject;
						push @message, $pos_string;

						eval {
							send_mail(@message);	# send the mail via smtp
						};
						if ($@) {
							`echo "Cannot send position: $@" >> $ENV{HOME}/.pskmail/TxInputfile`;
						 } else {
							`echo "\nPosition sent...\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
							@message = ();
						}
					} else {
						`echo "Sorry, no password on file!\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
					}
				} else {
						`echo "Sorry, internet not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				}

			} elsif (m/~QUIT/) {
####### Quit connect  #################################
				logprint ("Disconnect received\n");

#				disconnect();

				$ConnectStatus = "Disconnected";

				open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
				print SESSIONDATA $nosession;
				close SESSIONDATA;


			} elsif (m/~DELETE(.*)/) {
####### delete message(s) from POP #################################
				my @numbers = split / /, $1;
				eval {
					delete_mail(@numbers);
				};
				if ($@) {
					`echo "Cannot delete mail: $@" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {
					filter($mailuser, 0);
					`echo "Mail $1 deleted...\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~TELNET (\S+)\s(\d+)/) {
####### telnet agent #################################
print "Starting telnet agent\n";


				telnetagent($1, $2, 0);


			} elsif (m/~NODE/) {
####### telnet agent #################################
print "Entering packet node\n\n";

				my $telnetmode = 1;

				telnetagent("gate", 23, $telnetmode);

			} elsif (m/~RECx(.*)$/) {
####### Update user database for CALL #################################

				my $call = get_session();

				eval {
					my $rec = decode_base64($1);
					my @values = split (/,/ , $rec);

					$Pop_host = shift @values;
					my $record = $Pop_host . ",";
					$Pop_user = shift @values;
					$record .= ($Pop_user . ",");
					$Pop_pass = shift @values;
					$record .= ($Pop_pass . ",");
					$record .= ("smtp_host,");
					$address = shift @values;
					$record .= ($address . ",");
#					$mailuser = ".mailuser";
					$record .= ($mailuser . ",");
					$findupasswd = shift @values;
					chomp $findupasswd;
					$record .= ($findupasswd . ",");

					tie (%db, "DB_File", $dbfile)
						or die "Cannot open user database\n";

					$db{$call} = $record;

					untie %db;
				};
				if ($@) {
					print "Error in database: $@\n";
				} else {
					print "Updating database for $call\n";
					`echo "Updated database for $call" >> $ENV{HOME}/.pskmail/TxInputfile`;
				}

			} elsif (m/~SEND/) {
####### send message file #################################
				my @message = ();	# $to, $from, $subject, @body

				my $mailstatus = "receive";

				logprint ("Receiving mail \n");

				@body = ();

				while ($mailstatus eq "receive") {


					my $readline = <$reader>;


					if ($readline) {

						reset_idle();
						chomp($readline);
						if ($monitor) {
							print $readline, "\n";
						}
						$_ = $readline;
						if (m/^\.$/) {
							$mailstatus = "end_of_mail";
							@message = ();

							my $call = get_session();
							getuserdata($call);

							push @message, $to;
							push @message, $address;
							push @message, $subject;
							if ($to !~ /saildocs/){
								push @message, "PSKmail message from $address";
							}
							push @message, @body;
#							$dummy = pop @body;

							if ($to =~ /(.*)\@$ServerCall/) { # local mbox
								print "Local mbox $1\n";
								if (-e "$ENV{HOME}/.pskmail/localmail/$1") {
									print "Local mbox $1 exists\n";
								} else {
									`touch "$ENV{HOME}/.pskmail/localmail/$1"`;
									print "Local mbox $1 made\n";
								}
								my $mydate = `date`;
								my $msg =
									"From $call $mydate" .
									"To: $to" . "\n" .
									"From: $address" . "\n" .
									"Subject: " . $subject . "\n" .
									"Date: $mydate" . "\n" .
									join ("\n", @body) . "\n\n";

								my @boxes = ("$ENV{HOME}/.pskmail/localmail/$1");
								my @delivered_to = Email::LocalDelivery->deliver($msg, @boxes);

								@message = ();
								`echo "\nMessage sent...\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

							} else {						# internet mbox

								eval {
									if (length($attachment) == 0) {
										send_mail(@message);	# send the mail via smtp
									} else {
										push @message, $attachment;
										send_MIME(@message);    # send MIME message via smtp
										$attachment = "";
									}
								};

								if ($@) {
									`echo "Error sending mail : $@" >> $ENV{HOME}/.pskmail/TxInputfile`;
								} else {
									`echo "\nMessage sent...\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
								}
								@message = ();
							}
						}
						elsif (m/~ABORTSEND/) {	# in case we are stuck in a loop
							# stop.....
							$to = "";
							$subject  = "";
							$attachment = "";
							@body = ();
							last;
						}
						elsif (m/Subject: /) {
							# add subject
							my $subjectline = substr($readline, 9);
							$subject = $subjectline ;
						}
						elsif (m/To: /) {

							# write  To:

							$to = substr($readline, 4);

						} elsif (m/Your attachment: filename=/) {

							# write  To:

							$attachment = substr($readline, 26);
							print $attachment, "\n";
							push @body, $readline;

						} else {
							push @body, $readline;
						}
					}
				}	# end mail receive

			}
			$readline = "";
			select undef, undef, undef, 0.1;
		} else {
####### scheduler #################################

			if ($scheduler) {		# try the scheduler...
				$thishour = (gmtime)[2];
				if ($thishour != $lasthour) {

					if ($schedule[$thishour]) {
						if (get_session() eq $nosession) {
							my $cmd = '';
							my $outfreq = 0;
							my $rigm = '';

							# Set mode if defined
							if (@rigmod){
								$rigm = $rigmod[$mins];		##PATCH IS0GRB 11092008
							}
							if ($scheduler[0] eq 'C' ) {
								$cmd = 'H'; # set channel
							} elsif ($scheduler[0] eq 'M' ) {
								$cmd = 'E'; # set memory
							} else {
								$cmd = 'F';
								$outfreq = $freqs[$mins] + $freq_corrections[$mins];
							}
							eval {
								# Set a mode if none is entered
								if (defined $rigm && $rigm ne ''){
									##PATCH IS0GRB 11092008
									$error = `rigctl -m $rigtype -r $rigdevice -s $rigrate $cmd $outfreq M $rigm 0`
									or die "cannot use hamlib? $@\n";
								} else {
									# No mode switch here
									$error = `rigctl -m $rigtype -r $rigdevice -s $rigrate $cmd $outfreq`
									or die "cannot use hamlib? $@\n";
								}
							};
							if ($@) {
												print "Freq set error?\n";
							}

						}
					}
					$lasthour = $thishour;
				}
			}
####### scanner #################################



			if ($scanner) {
				scanner();
			}

####### beacon  ################################
			my $minu = (time / 60 ) % $period;		# What minute is this ? /
			if ($minu>=0 && $minu <5){
				serverbeacons();
			}

####### send aprs beacon #################################

			if ($Aprs_connect == 1) {
				if (time() - $systime >= 60 * $posit_time) {			## 10 mins aprs beacon...
					$systime = time();
					# now do what we want....

					my ($month, $hour, $min) = (gmtime) [3, 2, 1];
					$month = substr (("0" . $month), -2, 2);
					$hour = substr (("0" . $hour), -2, 2);
					$min = substr (("0" . $min), -2, 2);

					my $mytime = $month . $hour . $min . "z";
					if (-e "$ENV{HOME}/.pskmail/aprs_wx.txt") {

						my $WX = `cat "$ENV{HOME}/.pskmail/aprs_wx.txt"`;

						if (index ($WX, "_") == 0) { $WX = substr($WX, 1);}

						$MSG = "$ServerCall" . ">PSKAPR:" . "@" . $mytime . $Aprs_beacon . $WX ."\n";
					} else {
						$MSG = "$ServerCall" . ">PSKAPR:" . "@" . $mytime . $Aprs_beacon ."\n";
					}

					aprs_send ($MSG);

					# end
				}
			}
		}	# end if 'read'
	}	# end while loop

	close $reader;
	waitpid($pid, 0);
} else {
	die "Cannot fork: $!" unless defined $pid;
	close $reader;

	pskserver($ServerCall, "$Inputfile", "$output");

	close $writer;

	$error = `killall rflinkserver.pl`; # kill all children still running.....

	exit;
}
$error = `killall rflinkserver.pl`; # kill all children still running.....

exit (1);



########################################################
sub pskserver {	#		main, server
########################################################

my ($ServerCall, $Inputfile, $output) = @_;
$outputfile = substr ($output, 1);

my $STAT = "";
my $teststatus = "Listening";
my $startflag = 0;
my $mailcount = 0;

`cp zb "$Inputfile"`;
`cp zb "$outputfile"`;

		$ConnectStatus = "Listening";

while (1) {
	logprint ("Listening to the radio\n");
	$mailcount = 0;

	if ($startflag) {
		if (get_session() eq $nosession) {
			set_txstatus("TXDisconnect");
			send_frame();
		}
	} else {
		$startflag = 1;
	}


	reset_arq();

		$ConnectStatus = "Listening";

	if ($ConnectStatus eq "Listening") {

		until (get_rxstatus() eq "Connect_req") {

			if (get_session() eq $nosession) {
				if (get_rxqueue()) {
					my $mystring = get_rxqueue();
					printf $writer ("%s", $mystring);
					reset_rxqueue();
				}
			}
			eval {
				local $SIG{ALRM} = sub { die "alarm!!" };
				alarm 10;
				eval {
					listening($select);
				};
				alarm 0;
			};
			alarm 0;
			die if $@ && $@ !~ /alarm!!/;

			inc_idle();
			if (get_rxstatus() eq "Poll_rx") {
				set_txstatus("TXAbortreq");
				send_frame();
			}
		}

		if (get_rxstatus() eq "Connect_req") {

			$ConnectStatus = "Connected";

			# send connect_ack
			set_txstatus("TXConnect_ack");
			send_frame();

			reset_arq();

			if (-e "$ENV{HOME}/.pskmail/motd") {
				my $motd = `cat $ENV{HOME}/.pskmail/motd`;
				`echo "$motd" > $ENV{HOME}/.pskmail/TxInputfile`;
			}

			my $Reconnect_possible = 0;

			if (-e "$ENV{HOME}/.pskmail/telnetactive") {	#/
				unlink "$ENV{HOME}/.pskmail/telnetactive";
			}

## we are now connected

					print $writer "~ABORTSEND\n";

					$call=get_call();

					if (index ($Usercalls, $call) < 0) {
						if (getuserdata ($call) ne "Unknown") {
							$Usercalls .= $call;	# add it to the list of users
							$Usercalls .= " ";
						}elsif ($opensystem) {
							$Usercalls .= $call;	# add it to the list of users anyway
							$Usercalls .= " ";
							$Pop_host = "";
							print "added call to list of known calls\n";
							$usrmessage = "pse update your record!\n";
						}
					}

					$_ = $Usercalls;

					my @callfrags = split ("\/", $call);
					foreach my $frag(@callfrags) {
						if (m/$frag/) { 	# from Usercalls list
							if (length($frag) > 3) {
								$call = $frag;
								logprint ("Call $call o.k.\n");
								open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
								print SESSIONDATA $call;  # put it away safely
								close SESSIONDATA;
								sleep(2);

								my $mailcall = get_session();
								$Pop_host = "";
								$Pop_user = "";
								$Pop_pass = "";
								$address = "";
#								$mailuser = "";
								$findupasswd = "";

								getuserdata($mailcall);
								logprint("Connected to $mailcall");

								$sstate = "";
=head1
								@ping = ();

								if (-e "$ENV{HOME}/.pskmail/.internet") { unlink "$ENV{HOME}/.pskmail/.internet";}

								eval {								# check if internet present
									@ping = `ping -c 1 google.com 2>/dev/null`;
								};
								if($ping[1] && $ping[1] =~ /(time=\d*\.*\d* ms)/) {
									$sstate = "I";
									`touch $ENV{HOME}/.pskmail/.internet`;
								}
=cut
								$sstate = "I";
								`touch $ENV{HOME}/.pskmail/.internet`;

								if (-e "$ENV{HOME}/.pskmail/localmail/$mailcall") {	# see if there is local mail
									my $folder = Email::Folder->new("$ENV{HOME}/.pskmail/localmail/$call");
									my $mails = "";
									my $count = 0;

									for my $msg ($folder->messages) {
										$count++;
									}
									if ($count) {
										$sstate .= "L$count";
									}
								}

								if ($Pop_host && -e "$ENV{HOME}/.pskmail/.internet") {	# check if interner mail
									my $count = 0;
									$usrmessage = "";
									eval {
										local $SIG{ALRM} = sub { die "alarm"};
										alarm 20;
										eval {
											$mailcount = count_mail() ; # get mail from pop server
			print "Mail count =", $mailcount, "\n";
										};
										alarm 0;
									};

									alarm 0;
									if ($@ && $@ =~ /alarm/) {
										$usrmessage = " Timeout reading the mail...\n";
									} else {
											if ($mailcount > 1) {
												$usrmessage = $mailcount . " mails.\n";
											} elsif ($mailcount ==1) {
												$usrmessage = $mailcount . " email.\n";
											} elsif ($count < 0) {
												$usrmessage = "Could not list mail.\n";
											} else {
												$usrmessage = "No mail.\n";
											}
											if (-e "$ENV{HOME}/.pskmail/mailheaders") {unlink "$ENV{HOME}/.pskmail/mailheaders";}

									}
								}
								if ($mailcount > 0) {$sstate .= "M" . $mailcount};

								if (-e "$ENV{HOME}/.pskmail/motd") {
									my $motd = `cat $ENV{HOME}/.pskmail/motd`;
									`echo "$motd" >> $ENV{HOME}/.pskmail/TxInputfile`;
								}

											if ($usrmessage !~ /Could not list mail/) {
												`echo "$ServerCall $Version-$sstate>\n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
												$sstate = "";
											} else {
												`echo "$ServerCall $Version\nHi $call, pse update your record\n" > $ENV{HOME}/.pskmail/TxInputfile`;
											}
											$STAT = getuserdata($mailcall);
											$STAT = "" unless $Pop_host;
											last;
										}
									} else {
										`echo "Sorry, $call not registered...\n" > $ENV{HOME}/.pskmail/TxInputfile`;
										sleep 10;
									}
								}

								if ($Pop_host eq "" || $mailcount < 0) {
									`echo "\nHi $call, pse update your record\n" > $ENV{HOME}/.pskmail/TxInputfile`;
								}

					$RxReady = 1;

					$Retries = 0;

			while ($ConnectStatus eq "Connected") {	# Connected loop...

				until (get_idle() > $Maxidle) {
#					sleep(1);
					inc_idle();	# update idle counter

					reset_rxstatus();

					eval {
						local $SIG{ALRM} = sub { die "Alarm!!" };
						alarm 10;
						eval {
							listening($select);
						};
						alarm 0;
					};
					alarm 0;
					die if $@ && $@ !~ /Alarm!!/;

					$RxStatus = get_rxstatus();


					if (check_lastblock()) {
						last;
					}
				}
				$RxStatus = get_rxstatus();

				$txqlen = gettxtqueue();

				if ($RxStatus eq "Abort") {
					$ConnectStatus = "Listening";
					last;
				} elsif ($RxStatus eq "Status_rx"){
					if ($txqlen) {
						$RxReady = 1;
						$Retries = 0;
					} else {
						$RxReady = 1;
						$Retries++;

					}
				} elsif ($RxStatus eq "Poll_rx"){
						$RxReady = 0;
						$Retries++;
						set_txstatus ("TXStat");
						send_frame();
				} elsif ($RxStatus eq "Connect_req"){
					$Reconnect_possible++;
					$Retries = 0;
					if ($Reconnect_possible >= 1) {
						$Reconnect_possible = 0;
						print "Reconnect received!!\n";

						$ConnectStatus = "Listening";
						last;
					}
				} elsif ($RxStatus eq "Disconnect_req"){
					disconnect();
					set_txstatus("TXDisconnect");
					send_frame();
logprint("Disconnected from $call");
					reset_rxstatus;
					$ConnectStatus = "Listening";
					$Retries = $Max_retries;
				}

				my $mystring = get_rxqueue();
				printf $writer ("%s", $mystring);
				if ($mystring) {
					$Retries = 0;
				}
				reset_rxqueue();

				$Maxidle = get_maxidle();
# send next frame

#				if (-e "$ENV{HOME}/.pskmail/.tx.lck") {
#					reset_idle();
#				} else {
					inc_idle();
#					sleep (1);
#				}

				my $session_status = get_session();
				if ($session_status eq "none" || $session_status eq "beacon") {
					last;	# outta here...
				}

				if (get_idle() > $Maxidle) {	# send poll frame
					set_txstatus("TXPoll");
					send_frame();
					$Retries++;
					$RxReady = 0;
				}
				if ($RxReady == 1 ) {
					$RxReady = 0;
					if ($txqlen) {
						$Retries = 0;
					}
					$outputstring = gettxinput();
					set_txstatus("TXTraffic");
					send_frame($outputstring);
					if ($outputstring) {
						$Retries = 0;
					}
					$outputstring = "";
				}
					if ($Retries >= $Max_retries) {
						if (-e "$ENV{HOME}/.pskmail/telnetactive" && $Retries < 60) {
							# do nothing
						} else {
							set_txstatus("TXDisconnect");
							send_frame();
							$ConnectStatus = "Listening";
							reset_rxstatus;
							$ConnectStatus = "Listening";
							$Retries = 0;
						}
					}


				if ($ConnectStatus ne "Connected") {
					print "Disconnected\n";
					open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
					print SESSIONDATA $nosession;
					close SESSIONDATA;
					$ConnectStatus = "Listening";
					last;
				}
			}
## connect ended

		}

	}
}
}

########################################################
sub get_serverstatus {
########################################################

		return $ConnectStatus;
}

########################################################
sub count_mail {
#######################################
my $validconnect = 0;
my $pop;
$stationname = get_session();
print "\nStationname =", $stationname, "\n";
getuserdata($stationname);

return -1 unless $Pop_host;

if ($Pop_host =~ /gmail/i) {
	$Use_ssl = 1;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
								USESSL		=> $Use_ssl,
								);
} else {
	$Use_ssl = 0;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
	      						AUTH_MODE => 'PASS',
								USESSL		=> $Use_ssl,
								);
}


$count = $pop->Count();

if ($count > -1) {
	$pop->Reset;
}
return $count;
}

########################################################
sub list_mail {
#######################################
my $validconnect = 0;
my $hdr = "";
my $pop = "";

$stationname = get_session();

getuserdata($stationname);

return unless $Pop_host;

if ($debug_mail) {print "$Pop_host|\n";}

if ($Pop_host =~ /gmail/i) {
	$Use_ssl = 1;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
								USESSL		=> $Use_ssl,
								);
} else {
	$Use_ssl = 0;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
	      						AUTH_MODE => 'PASS',
								USESSL		=> $Use_ssl,
								);
}


	if ($pop->Count() > -1) {
		logprint ("Pop defined\n");
		$validconnect = 1;
	}

	if ($debug_mail) {print "$Pop_user,$Pop_pass|\n";}


	if ($validconnect) {logprint ("Authenticated\n");}

	$Count = $pop->Count;
	print "Count=$Count\n";


	if ($Count) {
		logprint ("Got message list\n");
		`echo '' > $mailuser`;
	};

	open (HFH, ">$ENV{HOME}/.pskmail/mailheaders");
	open (MFH, "$mailuser");

	print HFH "\n";
	print MFH "\n";

	for ($i =1; $i <= $Count; $i++) {
		my (@msg, $subject, $sender, $from);
		my $headerlength = 0;

		@msg = $pop->Head($i);

		$subject = $sender = '';

		foreach $hdr (@msg) {

			$headerlength += length ($hdr);

			if ($hdr =~ /^Subject:\s+/i) 	{
				$subject = substr($hdr, 8);
				$subject =~ s/['"]//g;
			}
#'"
			if ($hdr =~ /^From:\s+/i)	{
				$sender = substr($hdr, 6);
					($from = $sender) =~ s{<.*>}{};
					if ($from =~ m{\(.*\)}) {$from = $hdr; }
					$from ||= $sender;
					$from =~ s/['"]//g;
			}
		}

		my $messagecontent = $pop->Head($i, 999);

		my $mesglength = length ($messagecontent) - ($headerlength + 100);

		my $headerline = sprintf ("%2.0d %-30.30s %-60.60s %d\n", $i, $from, $subject, $mesglength);
		print HFH $headerline;
		if ($monitor && $debug_mail) {
			print $headerline;
		}
		my $fromline = sprintf ("%-20.20s %2.0d %-55.55s ", $from, $i, $subject);

			print MFH "From $fromline $mesglength Bytes\n";
			print MFH @$messagecontent;


		$mymailnumber = $i;

	}

	print HFH "\n";
	print MFH "\n";

	close (HFH);
	close (MFH);

	$pop->Close; # keep the mail for the moment




} # end list_mail

########################################################
sub read_mail {
#######################################

my @numbers = @_;

my $fault = 0;
my $messagecontent;
my $pop;

getuserdata(get_session());

return unless $Pop_host;

if ($Pop_host =~ /gmail/i) {
	$Use_ssl = 1;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
								USESSL		=> $Use_ssl,
								);
} else {
	$Use_ssl = 0;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
	      						AUTH_MODE => 'PASS',
								USESSL		=> $Use_ssl,
								);
}

if (-e "$mailuser") {`rm "$mailuser"`} ;

open (MFH, ">>$mailuser");

foreach my $msgid (@numbers) {
	my (@msg, $subject, $sender, $from, $dte);

	if ($msgid !~ /\d+/) { next; }

		if ($Pop_user) {
			$messagecontent = $pop->Head($msgid, 999);
		} else {
			$messagecontent = $pop->HeadAndBody($msgid);
		}
		if ($messagecontent) {
			print MFH "\n\nFrom \n";
			print MFH $messagecontent;
		} else {
			if ($monitor) {
				print "failed \n";
			}
			`echo "Message not available\n"`;
		}

	$mymailnumber = $msgid;
}

close (MFH);

$pop->Reset; # keep the mail for the moment




} # end read_mail

#####################################################
sub delete_mail {
#####################################################

my @delmessages = @_;

getuserdata(get_session());

return unless $Pop_host;

if ($Pop_host =~ /gmail/i) {
	$Use_ssl = 1;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
								USESSL		=> $Use_ssl,
								);
} else {
	$Use_ssl = 0;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
	      						AUTH_MODE => 'PASS',
								USESSL		=> $Use_ssl,
								);
}



foreach $msgid(@delmessages) {
	if ($msgid) {
		$pop->Delete($msgid) ;
	}
}
$pop->Close;

} # end delete_mail

#####################################################
sub getuserdata {
#####################################################
	tie (%db, "DB_File", $dbfile)
		or die "Cannot open user database\n";

	tie (%visitors, "DB_File", ".visitors")
		or die "Cannot open visitors database\n";

	my $key = "";
	my $Value = "";

	$key = shift @_;
	return ("") unless $key;
	if (exists $db{$key}) {
		 $Value = $db{$key};
	} else {
		$Pop_host = "";
		$Pop_user = "";
		$Pop_pass = "";
		$address = "";
#		$mailuser = "";
		$findupasswd = "";

		return "";
	}

=header1
		my $record = "";
		eval {
			local $SIG{ALRM} = sub { die "timeout" };
			alarm 10;
			eval {
				$record = `elinks -dump -dump-width 120 $dbaddress$ServerCall:$dbpass:$key 2>/dev/null` or die;
			};
		};

		alarm 0;
		if ($@ && $@ =~ /timeout/){
			return "Unknown";
		}
		if (length $record < 13 ) {
			print "Could not find record\n";
			$Pop_host = "";
			return ("Unknown");
		} else {
			$record =~ m/(\w.*)END/;
			$record = $1;
			my @dbvalues= split ", ", $record;

			if ($dbvalues[2] =~ /(.*)\@/) {
				$dbvalues[2] = $1;
			}

			$Value = $dbvalues[1] . ","	# host
				. $dbvalues[2] . ","	# user
				. $dbvalues[3] . ","	# pass
				. $relay . ","			# smtp
				. $dbvalues[5] . ","	# from
				. $mailuser . ","		# mailuser file
				. $dbvalues[4];
			$db{$key} = $Value;			# set record in db
#print $Value, "\n";
			$visitors{$key} = time;

		}
	}
=cut


		my @values = split (/,/ , $Value);

	$Pop_host = shift @values;
	$Pop_user = shift @values;
	$Pop_pass = shift @values;
	my $dump = shift @values;	# dump smtp server, fixed
	$address = shift @values;
	$dummy = shift @values;
	$findupasswd = shift @values;

	foreach $key (keys %visitors) {
		if (time - $visitors{$key} > 130000) {
			delete $visitors{$key};	# give them 36 hours...
			delete $db{$key};
		}
	}

	untie %db;
	untie %visitors;
	return @values;
}
#########################################################
sub getusercalls {
#########################################################

	tie (%db, "DB_File", $dbfile)
		or die "Cannot open user database\n";

	$Usercalls = "";
	foreach my $callkey (keys %db) {
		$Usercalls .= $callkey;
		$Usercalls .= " ";
	}
	untie %db;

}
#end
################################
sub filter {
################################
# in = $mailuser, out = mailfile
################################

my $inputfile = shift @_;
my $html = shift @_;

my @fromarray;
my @subjectarray;
my $datearray;
my @headerarray;
my $counter = -1;
my $line;
my @testarray = ();
@msgarray = ();
my @craps = ();

init_crap_filter ();	# fills craps array with text scraps

open (INP, $inputfile);
my @mailtest = <INP>;
close (INP);

my $quiet = 1;
my $subjdone = 0;
my $datedone = 0;
my $fromdone = 0;

my $mesgstart = 0;
foreach $line (@mailtest) {

$line =~ s/\015//g;

$_ = $line;

		if (m/^From /) {
			$quiet = 1;
			$counter++;
			$subjdone = 0;
			$datedone = 0;
			$fromdone = 0;
			$mesgstart = 1;
#			$msgarray[$counter] .= $line;
			if ($monitor) {
				print $line;	##debug
			}


		}

		next unless $mesgstart;

		if ($line eq "\n" || $line eq "\r\n") {
			if ($counter >= 0) { $msgarray[$counter] .= $line; }
			$quiet = 0;

			if ($monitor) {
				print $line;	##debug
			}
		}
		if (m/^From: / && $fromdone == 0) {
			if ($counter >= 0) { $msgarray[$counter] .= $line; }
			$fromdone = 1;
			if ($monitor) {
				print $line;	##debug
			}

		}
		elsif (m/^Subject:/ && $subjdone == 0) {
			if ($counter >= 0) { $msgarray[$counter] .= $line; }
			if ($counter >= 0) { $subjectarray[$counter] .= $line; }
			$subjdone = 1;
			if ($monitor) {
				print $line;	##debug
			}
		}
		elsif (m/^Date:/ && $datedone == 0) {
			if ($counter >= 0) { $msgarray[$counter] .= $line; }
			$datedone = 1;
			if ($monitor) {
				print $line;	##debug
			}
		} else {
			if ($quiet == 0) {
				if ($counter >= 0) {
					$line =~ s/\n\n/\n/;
					$line =~ s/\r\n/\n/;
					$msgarray[$counter] .= $line;
				}
			if ($monitor) {
				print $line;	##debug
			}
			}
		}
}

open (OUTP, ">$ENV{HOME}/.pskmail/mailin");
print OUTP @msgarray;
close (OUTP);

open (MAILF, "$ENV{HOME}/.pskmail/mailin");		# html filter
open (MAILOUT, ">$ENV{HOME}/.pskmail/mailfile");

while (<MAILF>) {
	if ($html == 0 && m#<HTML#i ... m#</HTML#i) {
		if ($monitor) {
			print $_;
		}
	 } else {
	 	my $in = crapfilter ($_);
	 	print MAILOUT $in;
	 }
}
close (MAILOUT);
close (MAILF);

`cp $ENV{HOME}/.pskmail/mailfile $ENV{HOME}/.pskmail/mailtest`;

} #end filter

################################################
sub crapfilter {
################################################
	my 	$line = shift @_;


	if ($craps[0]) {

		$line =~ s/=\?\?Q\?//;
		$line =~ s/\?Q\?//;
		$line =~ s/\?iso-8859-1\?q\?//;
		$line =~ s/=\?iso-8859-1\?Q\?//;
		$line =~ s/=09//;
		$line =~ s/=20//;
		$line =~ s/=22//;
		$line =~ tr/\r//;
		$line =~ s/^\s+\n/\n/;

		if ($line =~ /^>/ ) {
			return ("");
		}

		foreach $scrap (@craps) {
			chomp $scrap;
			if ($line =~ /$scrap/) {
				return ("");
			}
		}

	}

	return $line;
}
#################################################################
sub init_crap_filter {	# fills array with text scraps
#################################################################
	if (-e "$ENV{HOME}/.pskmail/crapmail.txt") {
		open ($nfh, "$ENV{HOME}/.pskmail/crapmail.txt");
		@craps = <$nfh>;
		close ($nfh);
	} else {
		print "Can not find file: crapmail.txt\n";
	}
}
#################################################################


############################################################
# Used to transfer emails through smtpserver.
# No internal eval block, pse use one when calling this method.
# SMTP AUTH TLS added 2008-01-10 /SM0RWO
# Rewritten to allow none, plain or TLS AUTH. 2008-10-19 /SM0RWO
# Added mandatory date header. 2008-10-19 / SM0RWO
sub send_mail {
############################################################
    my($to, $from, $subject, @body)= @_;
    my $smtp;

	if ($smtpauthlevel != 2)  # Will not use TLS Auth
	{
	        $smtp = Net::SMTP->new($relay, Debug => 1);
    	        die "Could not open connection: $!" unless $smtp;
		if ($smtpauthlevel == 1 && $smtptlsuser ne '')	# Plain AUTH here
		{
			if ( !$smtp->auth($smtptlsuser, $smtptlspass) ) {
				logprint ("SMTP authentication failed or is not needed.\n");
			}
		}
	}
	else # Will use TLS Auth
	{
		logprint ("SMTP TLS requested, no debug info available..\n");
    		$smtp = new Net::SMTP::TLS($relay,
        	Port     =>      $smtptlsport,
        	User     =>      $smtptlsuser,
        	Password =>      $smtptlspass,
		Timeout => 10,
		Debug    =>      1);
   	        die "Could not open TLS connection: $!" if (! defined $smtp);
	}
##DEBUG:
logprint ("Connection open...\n");
##DEBUG END

	# Create a date for the mandatory Date: header
   	my @dayofweek = (qw(Sun Mon Tue Wed Thur Fri Sat));
   	my @monthnames = (qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec));
   	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday) = localtime();
   	$year += 1900;
   	my $mydate = sprintf("%s, %02d %3s %04d %02d:%02d:%02d CST",$dayofweek[$wday],$mday,$monthnames[$mon],$year,$hour,$min,$sec);

	### Begin transaction ###
	$smtp->mail( $from );
	$smtp->to( $to );

	# Send the data
    	$smtp->data;
    	$smtp->datasend("To: $to\n");
    	$smtp->datasend("From: $from\n");
	$smtp->datasend("Date: $mydate\n");
	$smtp->datasend("Reply-To: $from\n"); # Necessary for gmail as it rewrites the from-field.
    	$smtp->datasend("Subject: $subject\n");
	$smtp->datasend("Comments: Via PskMail.\n");
	$smtp->datasend("\r\n");
    	foreach (@body) {
        	 $smtp->datasend("$_\n");
    	}
    	$smtp->dataend;
    	$smtp->quit;
}
###########################################
sub send_MIME {
###########################################
	my($to, $from, $subject, @body)= @_;
	my $attachment = pop @body;
	my @attachment_array = ();
	my @text_array = ();
	my @content_array = ();
	my $fileflg = 0;
	my $msg = "";

	foreach $line (@body) {
		if ($line =~ /Your attachment/) {
			$fileflg = 1;
		} elsif ($fileflg == 0) {
			push @text_array, $line;
		} else {
			push @content_array, $line;
		}
#		printf ("%d LINE:%s\n", $fileflg, $line);
	}
	my $filecontent = join "\n", @content_array;
	my $textcontent = join "\n", @text_array;

	my $afile = substr($attachment, 1, length($attachment) -2);

	my $att = decode_base64($filecontent);


	open (ATT, ">$ENV{HOME}/.pskmail/uploads/$afile");
	print ATT $att;
	close (ATT);


#	print "\nTEXT:\n", $textcontent , "\n---------\n" ;
#	print	"FILE=" , $afile , "\n";

# now try to send it via smtp...

### Create the multipart container
$msg = MIME::Lite->new (
  From => $from,
  To => $to,
  Subject => $subject,
  Type =>'multipart/mixed'
) or die "Error creating multipart container: $!\n";

### Add the text message part
$msg->attach (
  Type => 'TEXT',
  Data => $textcontent
) or die "Error adding the text message part: $!\n";

### Add the file
my $tpe = substr($afile, length($afile) - 3);
print $tpe, "\n";

$msg->attach (
   Type => "image/$tpe",
   Path => "$ENV{HOME}/.pskmail/uploads/$afile",
   Filename => $afile,
   Disposition => 'attachment'
) or die "Error adding $afile: $!\n";

### Try to send the message

### Send the Message
MIME::Lite->send('smtp', $relay, Timeout=>60);
$msg->send;
print "done", "\n";

`rm "$ENV{HOME}/.pskmail/uploads/$afile"`;

}

###########################################
sub get_session {
###########################################
	open SESSIONDATA, "$ENV{HOME}/.pskmail/PSKmailsession";
	my $session = <SESSIONDATA>;
	chomp $session;
	close SESSIONDATA;

	return $session;
}
##############################################
sub scanner {
##############################################

	$hour = (time / 3600) % 24;	#/
	if ($hour != $oldhour) {

		if (-e "$ENV{HOME}/.pskmail/qrg/freqs.txt") {
			open ($fh, $qrgfile) or die "Cannot open the scanner file!";
			@freqhrs = <$fh>;
			close ($fh);

			$cat = $freqhrs[0];
			print $cat;
			chomp $cat;

			@freqs = split (",", $cat);
			my $pskmodes = $freqhrs[1];
			print $pskmodes, "\n";
			@modes = split (",",$pskmodes);
		}
		$oldhour = $hour;
	}

	my $secs = time % 60;

#	if ($secs = 55 && $traffic_qrg && get_session() eq $nosession) {
#		$dummy = `rigctl -m $rigtype -r $rigdevice -s $rigrate "F" $traffic_qrg`;
#	}


	$mins = ((time / 60) + $freq_offset) % 5 ;	#/
	if ($mins != $oldmins) {
		if (get_session() eq $nosession) {
			my $cmd = '';
			my $outfreq = 0;
			my $rigm = '';

			if (@rigmod) {
				$rigm = $rigmod[$mins];		##PATCH IS0GRB 11092008
			}
			if ($scanner eq 'C' ) {
				$cmd = 'H'; # set channel
			} elsif ($scanner eq 'M' ) {
				$cmd = 'E'; # set memory
				$outfreq = $freqs[$mins];
				$modem = $modes[$mins];
			} elsif ($scanner eq 'S' ) {
				$modem = $modes[$mins];
				setmode($modem); # set fldigi mode
				$oldmins = $mins;
				return;
			} else {
				$cmd = 'F';
				$outfreq = $freqs[$mins] + $freq_corrections[$mins];
				$modem = $modes[$mins];
			}
			setmode($modem); # set fldigi mode
			my $error = "";
			eval {
				# Set a mode if none is entered
				if (defined $rigm && $rigm ne ''){
					$error = `rigctl -m $rigtype -r $rigdevice -s $rigrate $cmd $outfreq M $rigm 0`   ##PATCH IS0GRB 11092008
					or die "cannot use hamlib? $@\n";
				} else {
					$error = `rigctl -m $rigtype -r $rigdevice -s $rigrate $cmd $outfreq`   # No mode switch here
					or die "cannot use hamlib? $@\n";
				}
			};
			if ($@ !~ /cannot use/) {
				print "Freq set error?\n$@\n$error\n";
			}
#			set_autotune(1); # Remember that a fq change has taken place
		}
		}
		$oldmins = $mins;
#	}
}


##############################################
# Check the beacons array and send a beacon if the current minute is 0-4 (array size)
# and that array segment is 1. Update the status array to reflect beacon sent status.
# 2007-01-19, SM0RWO/Pär Crusefalk
sub serverbeacons {
##############################################
	my $trigger = 0;				# set to 1 if its time for a beacon
	my $n = 0;					# Used to loop the arrays
	my $realmin = (time / 60 ) % $period;		# What minute is this ?	#/

	# Loop the array
	foreach (@Beaconarray)
	{
		if ($realmin == $n)
		{
			# If the beacon has not been sent and it should then enter here
			if ($Beacons_sent[$n] == 0 && $_ == 1)
			{
				$trigger=1;			# Time for a beacon
				$Beacons_sent[$n]=1;		# Set the current minute as done
			}
			$Beacons_sent[$n-1]=0;			# Set the minute before (or wrap around) as unsent
		}
		$n++;
	}

	# Send the beacon if triggered and no session
	if ($trigger == 1 && (get_session() eq $nosession))
	{
		sleep 1+int(rand(25)); # settle the tx and allow multiple servers on qrg
		# send beacon
		logprint ("Sending beacon\n");
		set_txstatus("TXBeacon");
		send_frame();
	}
}

################################################
sub setmode {
################################################

my $pskmode =shift @_;

sendmode($pskmode);

open (CMDOUT, ">$ENV{HOME}/.pskmail/.modem");
print CMDOUT $pskmode;
close (CMDOUT);
}
################################################
sub getbeacons {
################################################

my $ClientCall = shift @_;
`cat $ENV{HOME}/.pskmail/server.log | grep "$ClientCall" > $ENV{HOME}/.pskmail/.mylines`;

my $now = time();
my $then = time();
$then -= 60 * 60 * 12;
my %beaconhours = ();

my $j = int ($then/(3600) % 24);
my $k = int ($now/(3600) % 24);
my $m = 0;

if ($j > $k) {
	$k += 24;
	$m = 1;
}

for (my $i = $j; $i <= $k; $i += 1) {
	$beaconhours{$i} = 0;
}

open (LOG, "$ENV{HOME}/.pskmail/.mylines");
while (my $input = <LOG>) {
	if ($input =~ /((\d\d):(\d\d)\s\w*\s(\w\w\w)-(\d*)-(\d\d\d\d): <SOH>..u\S*:26 .\d\d\d\d\.\d\d\w)/ ||
		$input =~ /((\d\d):(\d\d)\s\w*\s(\w\w\w)-(\d*)-(\d\d\d\d): <SOH>..u\S*:26 &&)/) {
#print $input;
		my $month = monthnumber($4);
		my $hour = $2;
		my $mins = $3;
		my $epoch = timegm (0, $mins, $hour, $5, $month,  $6);
		if ($epoch > $then) {
			my $beaconhour = int ($epoch/(3600) % 24);
			if ($m && $beaconhour < 13) { $beaconhour += 24;}
			$beaconhours{$beaconhour}++;
		}
	}
}
close (LOG);

my $index;
my $b_outstring = "";

foreach my $beacon(sort {$a <=> $b} keys %beaconhours) {
	if ($beacon > 23) {
		$index = $beacon - 24;
	} else {
		$index = $beacon;
	}
	$b_outstring .= $beaconhours{$beacon};
}
	$b_outstring .= "|";
	$b_outstring .= $index;
	$b_outstring .= " UTC\n";

	return $b_outstring;
} # end

###############################################
sub monthnumber {
###############################################
	my $mon = shift @_;
	my $count = 0;
	my @months = qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
	foreach $month (@months) {
		if ($month =~ /$mon/i) {
			last;
		}
		$count++;
	}
	if ($count > 11) {
		$count = 0;
	}
	return $count;
} # end

#############################################################
sub catcher  {		# catch ctrl-c
#############################################################
	$SIG{INT} = \&catcher;
	sendmodemcommand ("normal");
	sleep 2;
	print "Exiting...\n";
	`killall rflinkserver.pl`;
	wait;
	exit;
}

=head1
###############################################
sub bigearconnect {
###############################################
	if ($BigEarserverport) {
		eval {
			$BigEar = IO::Socket::INET->new(Proto     => "tcp",
										PeerAddr  => 'pskmail.org',
										PeerPort  => $BigEarserverport)
			   or die "can't connect to PSKBigEar server: $!";
		};
		$BigEar->autoflush(1);

		if ($@) {
			print "$@\n";
			return 0;
		}

		if (defined $BigEar) {print $BigEar "/nick $ServerCall\n";}

		return $BigEar;
	} else {
		return 0;
	}
}
=cut
################################################
sub telnetagent {
################################################
my $answer = "";
my $alarmtime = 120;
my $last_time = time();
my $rv;
my $telnetpid;
my $s = 0;
my ($host, $port, $isnode) = @_;
my $userid = "";
my $pass = "";

$Max_retries = 30;

	 $s = IO::Socket::INET->new(PeerAddr => $host,
							PeerPort => $port,
							Proto => "tcp",
							Type => SOCK_STREAM)
	or die "Could not connect to Remote Host: $@\n";

$s->blocking(0);

if ($s && $isnode) {
	`echo "\n -- Connecting packet radio node --\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
} else {
	`echo "\n -- PSKmail Telnet agent --\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
}

`touch $ENV{HOME}/.pskmail/telnetactive`;

my @telnetarray = `cat $ENV{HOME}/telnetusers`;
my @telnetcommands = ();
my @commands = ();
my $call = get_session();

if ($isnode) {
	foreach $commandline (@telnetarray) {
		if ($commandline =~ /^$call/) {
			print $commandline, "\n";
			push @telnetcommands, $commandline;
		}
	}

	foreach $telnetaddress (@telnetcommands) {
		if ($telnetaddress =~ /$host\,$port\,(.*)\n/) {
			@commands = split "," , $1;
		}
	}

	foreach my $output (@commands) {
		sleep 1;
		chomp $output;
		print $s $output, "\n";
	}
}

if ($telnetpid = fork) {
## parent code
	while ($s) {

		eval {
			local $SIG{ALRM} = sub {die "timeout"};
			alarm 30;
			eval {
				#########################
				$command = <$reader>;

				if ($command && $command =~ /~QUIT/ ) {
					print "::" . $command . "::\n";
					close $s;
					last;
				} elsif ($command) {
					chomp $command;
					print "|", $command, "|\n";

					if ($command) {
						if ($isnode) { #needs CR
							print $s "$command\r\n"
						} else {
							print $s "$command\n";
						}
					}
					$command = "";
				}
				#########################
			};
			alarm 0;
		};
		alarm 0;
		last if $@ && $@ =~ /timeout/;

		if (get_session() eq $nosession) {
			close $s;
			last;
		}


	}

	if (-e "$ENV{HOME}/.pskmail/telnetactive") {	#/
		unlink "$ENV{HOME}/.pskmail/telnetactive";
	}

} else {
## child code

	$s->blocking(0);

	open (OUT, ">>", "$ENV{HOME}/.pskmail/TxInputfile"); #/

	while ($s) {

		$rv = read ($s, $c, 1);

		while ($rv) {

			$answer .= $c;
			if ($answer =~ /(.*\n)/) {
				open (OUT, ">>", "$ENV{HOME}/.pskmail/TxInputfile"); #/
				print OUT $answer;
				close OUT;
				print $answer;
				$answer = "";
			} elsif ($answer =~ /Login: /) {
				open (OUT, ">>", "$ENV{HOME}/.pskmail/TxInputfile"); #/
				print OUT $answer, "\n";
				close OUT;
				print $answer;
				$answer = "";
			} elsif ($answer =~ /Password: /) {
				open (OUT, ">>", "$ENV{HOME}/.pskmail/TxInputfile"); #/
				print OUT $answer, "\n";
				close OUT;
				print $answer;
				$answer = "";
			} elsif ($answer =~ /^=>/) {
				open (OUT, ">>", "$ENV{HOME}/.pskmail/TxInputfile"); #/
				print OUT "=>\n";
				close OUT;
				print $answer, "\n";
				$answer = "";
			}

			$rv = read ($s, $c, 1);
		}

		if ($answer) {
			open (OUT, ">>", "$ENV{HOME}/.pskmail/TxInputfile"); #/
			print OUT $answer;
			close OUT;
			$answer = "";
		}

		sleep 1;

		if (get_session() eq $nosession) {
			close $s;
			last;
		}

	}

	`echo "\nTelnet closed.\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

#	close (OUT);
	exit;

}

if ($isnode) {
	`echo "\nDisconnecting from packet node.\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
} else {
	`echo "\nClosing telnet sesion.\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
}

$Max_retries = 8;

	return;
}

